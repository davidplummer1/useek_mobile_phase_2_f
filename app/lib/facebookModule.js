/**
 * This is a simple wrapper for the facebook module
 */

/** handler for the facebook login event */
function handleFacebookLogin(e) {
	if (e.error) {
		alert(e.error);
	} else if(e.success){
		loginThroughFacebook();
	}
}

var facebookModule = require("facebook");
facebookModule.appid = Alloy.CFG.facebookAppId;
facebookModule.permissions = ["email", "public_profile"];
facebookModule.forceDialogAuth = true;
facebookModule.addEventListener("login", handleFacebookLogin);

/** log into the app using the facebook credentials */
function loginThroughFacebook(){
	
	if(!Alloy.Globals.isOnline()){	return;	}
	
	if(!facebookModule.loggedIn){
		facebookModule.authorize();
		return;
	}

	Alloy.Globals.activityIndicator.show("Loading...");
	Alloy.Models.instance("users").login({
		provider : "facebook",
		token : facebookModule.accessToken
		
	}, {
		success : function(e) {
			Alloy.Globals.activityIndicator.hide();
			//
			Alloy.Globals.mainWindow.openWindow({
				name : 'main',
				animate : true
			});
			//Alloy.createController('main').getView().open();
		},
		error : function(e) {
			Alloy.Globals.activityIndicator.hide();
			alert("email or password is not match");
		}
	});
}

/** logout from facebook */
exports.softLogout = function(){
	if(facebookModule.loggedIn){
		facebookModule.logout();
	}
};

/** logout from facebook & clear the local cookies 
 * */
exports.hardLogout = function(){
	
	if(facebookModule.loggedIn){
		facebookModule.logout();
	}
	
	/*
	var client = Titanium.Network.createHTTPClient();
	client.clearCookies("https://login.facebook.com");
	client.clearCookies("http://login.facebook.com");
	client.clearCookies("https://m.facebook.com");
	client.clearCookies("http://m.facebook.com");
	*/
};
exports.permissions = facebookModule.permissions;
exports.accessToken = facebookModule.accessToken;
exports.handleFacebookLogin = handleFacebookLogin;
exports.loginThroughFacebook = loginThroughFacebook;