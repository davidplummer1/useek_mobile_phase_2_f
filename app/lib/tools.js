/**
 * @fileOverview	helpful methods to use
 * @name			tools.js
 */

/** checks whether the current device is a tablet
 * @returns {boolean}	true: tablet; false; non-tablet device
 * */
exports.isTablet = function() {
	var osname = Ti.Platform.osname;
	switch(osname) {
		case 'ipad':
			return true;
			break;
		case 'iphone':
			return false;
			break;
		case 'android':
			var screenWidthInInches = Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi;
			var screenHeightInInches = Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi;
			var maxInches = (screenWidthInInches >= screenHeightInInches) ? screenWidthInInches : screenHeightInInches;
			return (maxInches >= 7) ? true : false;
			break;
		default:
			return false;
			break;
	}
};

/** creates and displays an alert dialog box
 *
 * @param {Object} properties the properties to set for the alert dialog
 * @param {Object} properties.message	The message to show the user
 * @param {Object} properties.ok		The label for the okay button
 * @param {Object} okCallback the callback method to be executed when okay button is pressed
 * @param {Object} cancelCallback the callback method to be executed when the cancel button is pressed
 */
exports.showAlertDialog = function(properties, okCallback, cancelCallback) {
	var properties = properties || {};
	properties.message = properties.message || "Are you sure?";
	properties.ok = properties.ok || "Yes";
	var alertDialog = Ti.UI.createAlertDialog(properties);
	alertDialog.addEventListener("click", function(e) {
		if (e.index !== e.source.cancel) {
			okCallback && okCallback();
		} else {
			cancelCallback && cancelCallback();
		}
	});
	alertDialog.show();
};

/**
 * This function will attempt to round a decimal value to two place
 * @param {Number} amount
 * @param {Number} sigFigs	the amount of significant figures to retain to the right of the decimal place
 * If this isn't specified, two will be used
 * @return {Number}	the formatted monetary value
 **/
exports.roundDecimal = function(amount, sigFigs) {
	Ti.API.info("in " + amount);
	try {
		amount = amount == null ? 0 : (typeof (amount) == "string" ? parseFloat(amount) : amount);
		//if this number has a decimal value, round it
		if (amount % 1 > 0) {
			Ti.API.info("out " + amount.toFixed(sigFigs || 2));
			return amount.toFixed(sigFigs || 2);
		}
		//trim the decimal value and return the integer value
		else {
			Ti.API.info("out " + parseInt(amount));
			return parseInt(amount);
		}
	} catch(ex) {
		return 0;
	}
};

/** 
 * Returns an image url. If a URL cannot be found in the received JSON, the default useek placeholder will be used 
 * @param {Object} urlJSON
 * @param {Object} urlJSON.large
 * @param {Object} urlJSON.medium
 * @param {Object} urlJSON.thumb
 * @return {String} the local/remote path for an image URL
 * 
 * */
exports.getImageURL = function getImageURL(urlJSON){
	return (urlJSON && (urlJSON.large || urlJSON.medium || urlJSON.thumb)) || "/images/useek_default-500.png";
};

/** returns the received date object as DD/MM/YYYY
 * 
 * @param {Object} dateObj a JavaScript date object
 * @return {String} a date object string of the form DD/MM/YYYY
 */
exports.formatDateString = function(dateObj){
	var month = dateObj.getMonth()+1;
	var date = dateObj.getDate();
	var year = dateObj.getFullYear();
	month = month < 10 ? "0" + month : month;
	date = date < 10 ? "0" + date : date;
    return month + "/" + date + "/" + year;
};
