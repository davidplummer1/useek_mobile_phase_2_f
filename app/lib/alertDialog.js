/** creates an alert dialog box
 *
 * @param {Object} properties the properties to set
 * @param {Object} okCallback the callback method to be executed when okay is pressed
 * @param {Object} cancelCallback the callback method to be executed when cancel is pressed
 */
exports.show = function(properties, okCallback, cancelCallback) {
	var properties = properties || {};
	properties.message = properties.message || "Are you sure?";
	properties.ok = properties.ok || "Yes";
	var alertDialog = Ti.UI.createAlertDialog(properties);
	alertDialog.addEventListener("click", function(e) {
		if (e.index !== e.source.cancel) {
			okCallback && okCallback();
		} else {
			cancelCallback && cancelCallback();
		}
	});
	alertDialog.show();
};