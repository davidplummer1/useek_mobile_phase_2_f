/**
 * @fileOverview : REST API Sync Adapter
 * @dependencies /lib/activityIndicator.js
 */

/**
 * @property {Number} a constant representing one minutes worth of milliseconds
 * This is the timeout period for the http client requests
 */
var ONE_MINUTE = 60000;

/**
 * @property {Object} options
 * Some of the properties used by the sync adapter to know when to reference the cache,
 * when to clean the cache
 * 
 * @property {Boolean} options.useCache		This property determines whether the cache will be used or not. This applies to all models & collections.	
 * @property {Boolean} options.cacheSeconds	the number of seconds to wait	
 * @property {Boolean} options.pruneSeconds	specifies how old a record must be in seconds before it's deleted from the cache
 */
var options = {
	useCache : false,
    cacheSeconds : 600,
    pruneSeconds : 720
};

/**
 * @event app.purge.cache
 * Triggered by the app in order to clear the cache
 * @param {Object} _options the optional properties used when pruning the cache
 * @param {Boolean} _options.showAlert if true, an alert will be shown after the purge method is executed
 */
Ti.App.addEventListener('app.purge.cache', function(_options) {
    _prune_cache(0, _options.showAlert);
});

/** this method is called by beforeModelCreate whenever a model is created 
 *
 * @param {Object} [config] an optional configuration object (currently not used)
 *  */
function InitAdapter(config) {
	
	//connect to a local data base
    db = Titanium.Database.open('http_client_cache');

	//make a requests table if it doesn't exist
    db.execute('CREATE TABLE IF NOT EXISTS REQUESTS (URL_HASH STRING, RESPONSE TEXT, UPDATED_AT INTEGER)');

    db.close();

    return {};
}

/** 
 * Remove expired records from the cache
 * 
 * @param {Number/null} seconds a number of seconds representing how old records can in the cache. 
 * @param {Boolean} _showAlert show an alert after pruning is finished = true
 */
function _prune_cache(seconds, _showAlert) {
    var count, row, origCount;

    if (seconds == null) {
        seconds = options.pruneSeconds;
    }
    
    db = Titanium.Database.open('http_client_cache');
    
    //count the expired records which should be purged
    var row = db.execute("SELECT COUNT(*) FROM REQUESTS WHERE UPDATED_AT < DATETIME('now','-" + seconds + " seconds')");
    origCount = ((row && row.rowCount !== 0) ? row.field(0) : 0);
    Ti.API.debug(' num to purge ' + origCount);

	//delete the expired records
    db.execute("DELETE FROM REQUESTS WHERE UPDATED_AT < DATETIME('now','-" + seconds + " seconds')");

	//count the remaining records
    row = db.execute("SELECT COUNT(*) FROM REQUESTS WHERE UPDATED_AT < DATETIME('now','-" + seconds + " seconds')");
    Ti.API.debug('remaining after purge ' + ((row && row.rowCount !== 0) ? row.field(0) : 0));

	//show an alert (optional)
    if (_showAlert) {
        alert("Purged " + origCount + ' records from cache');
    }

    return db.close();

};

/**
 * Build a hash url to be inputted into the cache
 * @param {Object} _options
 * 
 * @param {} _options.type
 * @param {} _options.url
 * @param {} _options.date
 * 
 */
function _compute_url_hash(_options) {
    return url_hash = Ti.Utils.md5HexDigest(_options.type + _options.url + _options.data);
};

/**
 * Check if exists 
 * @param {Number} seconds the age limit for the cache data in seconds
 * @param {String} _url_hash the url hash to search for
 *  
 */
function _get_cached_response(seconds, _url_hash) {

    var cachedAt, responseText, row;
    db = Titanium.Database.open('http_client_cache');
    if (seconds == null) {
        seconds = options.cacheSeconds;
    }
    
    //select the cache data
    row = db.execute("SELECT RESPONSE, UPDATED_AT FROM REQUESTS WHERE URL_HASH=? AND UPDATED_AT > DATETIME('now','-" + seconds + " seconds')", _url_hash);
    responseText = ((row && row.rowCount !== 0) ? row.field(0) : null);
    cachedAt = ((row && row.rowCount !== 0) ? row.field(1) : null);
    row.close();
    db.close();
    
    //return the cached data if it exists
    if (responseText != null) {
        Ti.API.info(' cache hits ' + _url_hash);
        return {
            responseText : responseText,
            cached : true,
            cached_at : cachedAt,
            status : 200
        };
    }
};

/**
 * checks if a data cache can be found for the corresponding url hash
 * @param {String} _url_hash	the url hash to search for
 * @return {Object} 
 */
function _exists_in_cache(_url_hash) {

    var count, row, _ref;
    row = db.execute("SELECT COUNT(*) FROM REQUESTS WHERE URL_HASH=?", _url_hash);

    count = ((row && row.rowCount !== 0) ? row.field(0) : 0);
    row.close();
    return ( _ref = count > 0) != null ? _ref : {
        "true" : false
    };
};

/** 
 *	save the http response data in the cache
 * @param {Object} _response	the http client object
 * @param {Object} _options		the options property tied to the http client object
 *  
 */
function _save_to_cache(_response, _options) {

    var urlHash;
    if (_response.status >= 400 || _response.cached) {
        return;
    }
    db = Titanium.Database.open('http_client_cache');
    urlHash = _compute_url_hash(_options);
    
    //update the data in the cache if it already exists
    if (_exists_in_cache(urlHash)) {
        Ti.API.info('updated cache ' + urlHash);
        db.execute("UPDATE REQUESTS SET RESPONSE=?, UPDATED_AT=CURRENT_TIMESTAMP WHERE URL_HASH=?", _response.responseText, urlHash);
    } else {
    	
    	//save the data to the cache if it's new
        Ti.API.info('cached ' + urlHash);
        db.execute("INSERT INTO REQUESTS (RESPONSE, URL_HASH, UPDATED_AT) VALUES (?,?,CURRENT_TIMESTAMP)", _response.responseText, urlHash);
    }
    return db.close();
};

/**
 *	The method whereupon all of the model/collection sync methods converge (fetch, save, and destroy). 
 *  This me
 * 
 *  @param {Object}		_options
 *  @param {Function} 	_callback
 *  @param {Object}		model
 *  @param {String} 	method
 *  
 */
function apiCall(_options, _callback,model,method) {

	//clean the cache
   	_prune_cache();
  	
    var urlHash = _compute_url_hash(_options);
    

	   // Cache only if useCache is explicitly set in the model
	    if ( _options.useCache && (response = _get_cached_response(1800, urlHash)) ) {
	        try {
	            _callback({
	                success : true,
	                text : response.responseText || null,
	                data : JSON.parse(response.responseText) || null
	            });
	            return;
	        } catch (EE) {
	            Ti.API.error('bad cache entry ' + _options.url);
	            // trying to make the call again
	        }
	
	        if (_options.preventActivityIndicator !== true) {
	            
	        }
	
	    }
    

    var xhr = Ti.Network.createHTTPClient({
        timeout : ONE_MINUTE
    });

    // save them for later!!
    xhr.options = _options;

    //Prepare the request
    xhr.open(_options.type, _options.url);

   

    xhr.onload = function() {
        

        var data = null;
	
        try {
        	//if the cache is enabled, save the response to it
			if(_options.useCache || options.useCache){
            	_save_to_cache(xhr, xhr.options);
           	}
						
            data = JSON.parse(xhr.payloadString || xhr.responseText);
          
            Ti.API.debug("apiCall response Text::" + (xhr.payloadString || xhr.responseText));
            _callback({
                success : true,
                text : xhr.responseText || null,
                data : data || null
            });

        } catch (EE) {
            Ti.API.error('Error parsing response text: ' + EE);
            Ti.API.error('Error parsing response text: ' + (xhr.payloadString || xhr.responseText));
            _callback({
                success : false,
                text : xhr.payloadString || xhr.responseText || null,
                data : data || null
            },model,method);
        }

    };

    /** event handler for the http errors returned 
     * @param {Object} err	the error object returned via HTTP
     *  */
    xhr.onerror = function(err) {

        // Ti.API.error(JSON.stringify(err));
        _callback({
            'success' : false,
            'text' : xhr.responseText
        });
        Ti.API.error("Error Text::" + xhr.responseText);

    };
    
    /** event handler for data packets coming in from the http client */
    xhr.ondatastream = function(e){
    	xhr.payload = xhr.payload || "";
    	try{
    		xhr.payload += e.source.responseText;
    	}catch(ex){}
    };
   
   	//add the headers to the http client request
    for (var header in _options.headers) {
        xhr.setRequestHeader(header, _options.headers[header]);
    }

    if (_options.beforeSend) {
        _options.beforeSend(xhr);
    }

    //Show Loading dialog
    

    Ti.API.debug('_options.url: ' + _options.url);
    Ti.API.debug('_options.type: ' + _options.type);
    Ti.API.debug('_options.data: ' + JSON.stringify(_options.data));
    Ti.API.debug('_options.headers: ' + JSON.stringify(_options.headers));
    Ti.API.debug('_options.contentType: ' + JSON.stringify(_options.contentType));

    try {
        xhr.send(_options.data || {});
    } catch(EE) {
        // looking for exceptions here
        
    }
}

/** the sync method which is executed when fetch, save, or destroy are executed by the model or collection
 * @param {String} method	the type of action being performed by the model/collection
 * @param {Object} model 	a reference to the model/collection being synced
 * @param {String} opts		the options passed through the model/collection
 * @param {Function} opts.success	the success callback
 * @param {Function} opts.error		the error callback
 */
function Sync(method, model, opts) {

// Check for a flag indicating whether or not to use the cache (defaults to no).
	// This property can be set on the entire model (model.config.useCache),
	// or passed on a per-API call basis (opts.useCache)
	opts.useCache = opts.useCache || model.config.useCache || options.useCache || false;

    /// Allowing developers to prevent activity indicator to be shown.
    // var preventActivityIndicator = opts.preventActivityIndicator;
    // opts.preventActivityIndicator && delete opts.preventActivityIndicator;

    var methodMap = {
        'create' : 'POST',
        'read' : 'GET',
        'update' : 'PUT',
        'delete' : 'DELETE'
    };

    var type = methodMap[method];
    var params = _.extend({}, opts);

    // allow the user to pass in the method type
    params.type = params.type || type;

    //set default headers (headers passed as an option take higher precedence)
    try{
    	params.headers = _.extend(((model.headers && model.headers()) || {}), (params.headers  || {}));
	}catch(ex){
		Ti.API.error(ex);
	}
	
	
    // We need to ensure that we have a base url.
    if (!params.url) {
    	
    	//look for a url function in the model
        params.url = (model && model.url && model.url()) || false;
        
        //check if the user has passed a specific url (this takes higher precedence)
        params.url = opts.url || params.url;
        
        if (!params.url) { 
            Ti.API.debug("fetch ERROR: NO BASE URL");
            return;
        }
    }

    // For older servers, emulate JSON by encoding the request into an HTML-form.
    if (Alloy.Backbone.emulateJSON) {
        params.contentType = 'application/x-www-form-urlencoded';
        params.processData = true;
        params.data = params.data ? {
            model : params.data
        } : {};
    }

    // For older servers, emulate HTTP by mimicking the HTTP method with
    // `_method`
    // And an `X-HTTP-Method-Override` header.
    if (Alloy.Backbone.emulateHTTP) {
        if (type === 'PUT' || type === 'DELETE') {
            if (Alloy.Backbone.emulateJSON)
                params.data._method = type;
            params.type = 'POST';
            params.beforeSend = function(xhr) {
                params.headers['X-HTTP-Method-Override'] = type;
            };
        }
    }

    //json data transfers ONLY SET IF iPhone
    !Ti.Android && (params.headers['Content-Type'] = "application/json; charset=utf-8");

    /**
     *	the callback method for the onload and onerror methods of the http client in apiCall
     *	@param {Object} _resp	the http response from the httpclient in the apiCall method scope
     *  @param {Object} model	the applicable model/collection making this sync call
     *  @param {String} method 	the method string
     **/
    var callbackOptions = function(_resp,model,method) {
    	//if the response was successful, handle the payload
        if (_resp.success) {
        	
        	try{
        	//if a read has been performed by a collection, try to pull the data array out of the response and put it into the collection 
        	//TODO expand this to include every collection instead of just recommendations
        	if(method == "read" && model.config.autoParse){
        		params.success(_resp.data[model.config.adapter.collection_name],_resp.data);
        	}
        	else {
            	params.success(_resp.data || _resp.text, _resp.data);
            }
           }catch(ex){
           		params.success(_resp.data, _resp.data);
           }
            
        } else {
            
            //
            try {
                params.error({ message : "An unexpected error has occurred. Please try again."}, _resp.text);
            } catch (EE) {
                params.error(_resp.text, _resp.text);
            }
            Ti.API.debug("ERROR" + _resp.text);
            model.trigger("error");
        }
    };

    if (opts.JSON) {
        params.data = model.toJSON();
    } else if (!opts.data && model && (method == 'create' || method == 'update')) {
        // comment, this might have to be configurable
        params.data = JSON.stringify(model.toJSON());
	} else if (opts.data) {
		
        /* add any of the extras as parameters on the URL,
         this content should be JSON objects converted
        to strings*/
        var query = "";
        for (var i in opts.data) {
            query += i + "=" + opts.data[i] + "&";
        }

        // add the params, remove trailing "&"
        params.url += "?" + query.substring(0, query.length - 1);

        Ti.API.debug('THE URL ' + params.url);
        
        // no data, it is all on URL
        params.data = null;
    }

    switch (method) {
    	
    	//executed by model.destroy
        case "delete":
        
        //executed by model.save when a valid id exists in the model
        case "update":

            apiCall(params, function(_r) {
                callbackOptions(_r,model,method);
                _r ? model.trigger("fetch change sync") : model.trigger("error");
            },model,method);
            break;
            
        //executed by model.save when the id doesn't exist for the model
        case "create":
            apiCall(params, function(_r) {
                callbackOptions(_r,model,method);
                _r ? model.trigger("fetch sync") : model.trigger("error");
            },model,method);
            break;
            
        //executed by model.fetch and collection.fetch    
        case "read":
            apiCall(params, function(_r) {
                callbackOptions(_r,model,method);
                _r ? model.trigger("fetch sync") : model.trigger("error");
            },model,method);
            break;
    }

};

//we need underscore
var _ = require("alloy/underscore")._;

module.exports.sync = Sync;

/**
 * this method is called before each model tied to this sync adapter is created
 * @param {Object} config
 */
module.exports.beforeModelCreate = function(config) {
    config = config || {};
    InitAdapter(config);
    return config;
};

/**
 * this method is called after each model tied to this sync adapter is created
 * @param {Object} Model	a reference to the model being created
 */
module.exports.afterModelCreate = function(Model) {
    Model = Model || {};
    Model.prototype.config.Model = Model;
    return Model;
};
