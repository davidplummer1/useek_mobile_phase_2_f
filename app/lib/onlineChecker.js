/** @fileOverview: checks whether or not the user is online.
 *  @name			onlineChecker.js
 */

/**
 * check if the user is online or not
 * @returns	{boolean} true: the user is online; false: the user isn't online
 */
exports.isOnline = function(){

	//if the user is not online, hide the activity indicator if it's being shown and display an error message
	if(!Ti.Network.getOnline()){
		try{Alloy.Globals.activityIndicator.hide();}catch(ex){Ti.API.info("Alloy.Globals.activityIndicator doesn't exist. Oh well.");}
		alert("You must have an Internet Connection to use this feature.");
		return false;
	}
	return true;
};
