//var app = require('/common/globals');
var Cloud = require('ti.cloud');
//var CloudPush = require("ti.cloudpush");


function receivePush(e) {

	//if the user is in the app, update the badge number
	if(!e.inBackground && e.badge){
		
		if(OS_IOS){
			Ti.UI.iPhone.appBadge = e.badge;
		}
	}
	if(e.inBackground && OS_IOS){	
		
	}
	

}

var androidPushModule = null;

var pushDeviceToken = null, pushNotificationsEnabled = null;
function enablePushNotifications() {

	pushNotificationsEnabled = true;
	Ti.App.Properties.setBool('PushNotifications-Enabled', true);
	checkPushNotifications();
}

function disablePushNotifications() {
	
	pushNotificationsEnabled = false;
	Ti.App.Properties.setBool('PushNotifications-Enabled', false);
	checkPushNotifications();
}

function getAndroidPushModule() {
	
	try {
		return require('ti.cloudpush');
	} catch (err) {
		//alert('Unable to require the ti.cloudpush module for Android!');
		pushNotificationsEnabled = false;
		Ti.App.Properties.setBool('PushNotifications-Enabled', false);
		return null;
	}
}

function checkPushNotifications() {
	if (pushNotificationsEnabled === null) {
		pushNotificationsEnabled = Ti.App.Properties.getBool('PushNotifications-Enabled', false);
	}
	if (Ti.Platform.name === 'iPhone OS') {
		if (pushNotificationsEnabled) {
			if (Titanium.Platform.model == 'Simulator') {
				//alert('The simulator does not support push!');
				disablePushNotifications();
				return;
			}
			

			//alert("registering for push notifications");
			Ti.Network.registerForPushNotifications({
				types : [Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND],
				success : deviceTokenSuccess,
				error : deviceTokenError,
				callback : receivePush
			});
		} else {

			Ti.Network.unregisterForPushNotifications();
			pushDeviceToken = null;
		}
	} else if (Ti.Platform.name === 'android') {
		if (androidPushModule === null) {
			androidPushModule = getAndroidPushModule();
			if (androidPushModule === null) {
				return;
			}
		}
		if (pushNotificationsEnabled) {
			
			// Need to retrieve the device token before enabling push
			androidPushModule.retrieveDeviceToken({
				success : deviceTokenSuccess,
				error : deviceTokenError
			});
		} else {
			
			androidPushModule.enabled = false;
			androidPushModule.removeEventListener('callback', receivePush);
			pushDeviceToken = null;
		}
	}
}

function deviceTokenSuccess(e) {
	pushDeviceToken = e.deviceToken;
	Ti.App.Properties.setString("deviceToken",pushDeviceToken);
	var cf = Alloy.Models.instance("user").get("custom_fields");
	cf.deviceTokens = cf.deviceTokens || {};
	cf.deviceTokens[e.deviceToken] = Ti.Platform.osname;
	Alloy.Models.instance("user").save({ custom_fields : cf});
	
	if (OS_ANDROID) {
		androidPushModule.enabled = true;
		//if app should open on push
	//	androidPushModule.focusAppOnPush = true;
		androidPushModule.showAppOnTrayClick = true;
		androidPushModule.addEventListener('callback', receivePush);

	}
	subscribeToChannel(Alloy.CFG.pushNotificationChannel);
}

function deviceTokenError(e) {
	//alert(JSON.stringify(e));
	disablePushNotifications();
}

function subscribeToChannel(_channel) {
	
	//attempt to unscubscribe the user
	/*Cloud.PushNotifications.unsubscribe({
		channel : _channel,
		device_token : pushDeviceToken
	},function(e){*/
	
		//attempt to subscribe the user
		Cloud.PushNotifications.subscribe({
		channel : _channel,
		device_token : pushDeviceToken, //Ti.App.Properties.getString('deviceToken'),
		type : Ti.Platform.name === 'iPhone OS' ? 'ios' : Ti.Platform.name
	}, function(ee) {
		if (ee.success) {
			channel = _channel;

			//alert("registered");
		} else{
			//alert("An error occurred while trying to subscribe for push notifications.");
		}

	});
		
	//});

}


function turnOnAndroidTrayListeners(){
	androidPushModule.addEventListener("trayClickFocusedApp",handleAppFocus);
}

function turnOffAndroidTrayListeners(){
	androidPushModule.removeEventListener("trayClickFocusedApp",handleAppFocus);
}

function handleAppFocus(e){

}


exports.disablePushNotifications = disablePushNotifications;
exports.enablePushNotifications = enablePushNotifications;
exports.subscribeToChannel = subscribeToChannel;
