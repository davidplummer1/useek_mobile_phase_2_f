/**
 * CommonJS module used to display an activity window on Android and iOS
 * compatible with Titanium 3.x
 * 
 * TODO: 
 *      - Migrate to Alloy Widget
 *  - Allow the iOS view to expand for longer text messages.
 * 
 */

/// Used as a reference to the timeout counter.
var timeout;
var maxTime = 60000;

/// We can specify a callback to be executed when the activity indicator times out
var timeoutCallback;

var shown = false;
var isAndroid = (Titanium.Platform.osname === 'android');
var message = "Loading...";

var actInd;

/// The activity windows are different for Android and iOS, so we have to handle the
/// creation separately 
if (!isAndroid) {       
        actInd = Titanium.UI.createActivityIndicator({
                bottom : 50,
                height : 55,
                width : '180',
                height : 'auto',
                width : 'auto',
                color : "black",
        });
        
        actInd.style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;

        var window = Ti.UI.createWindow({
                modal : false,
                opacity : 1,
                width : '100%',
                height : '100%',
                navBarHidden : true,
        });

        var toolActIndView = Ti.UI.createView({
                height : Ti.UI.FILL,
                width : Ti.UI.FILL,
                backgroundColor : 'transparent',
                opacity : 0.3
        });

        window.add(toolActIndView);

        var indView = Ti.UI.createView({
                height : '150dp',
                width : '150dp',
                backgroundColor : 'black',
                borderRadius : 10,
                opacity : 0.8
        });

        var ai = Ti.UI.createActivityIndicator({
                top : '50dp',
                left : "55dp",
                message : "",
                style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
                height : 'auto',
                width : 'auto',
                color : "black",
                
        });

        indView.add(ai);

        indView.add(actInd);
        window.add(indView);

        var activity_message = Ti.UI.createLabel({
                text : message || 'Loading...',
                color : 'white',
                width : 'auto',
                height : 'auto',
                font : {
                        fontSize : '16dp',
                        fontWeight : 'bold'
                },
                bottom : (parseInt(Ti.Platform.displayCaps.platformHeight) / 2 - 70) + "dp",
                //bottom : '185dp'
        });
        window.add(activity_message);

/// New way to create the activity indicator on Ti 3.X
} else {
        actInd = Ti.UI.Android.createProgressIndicator({
          message: message,
          location: Ti.UI.Android.PROGRESS_INDICATOR_DIALOG,
          type: Ti.UI.Android.PROGRESS_INDICATOR_INDETERMINANT,
        });
}

/**
 * Sets the defult time out time.
 * @params {int} Time in milliseconds 
 */
function setMaxTime(time){
        maxTime = time;
}

/// Setter for the timeout callback
function setTimeoutCallback(fn){
        timeoutCallback = fn;
}

/// Changes indicator's text
function setText(text){
        message = text;
}

/**
 * Shows the activity indicator
 * @param {msg} optional - Message to be displayed
 */
function show(msg){
        if (shown) {
                if (isAndroid){
                        actInd.message = msg;
                        actInd.show();
                } else
                        activity_message.text = msg;
                return;
        }

        if (msg)
                if (isAndroid)
                        actInd.message = msg;
                else
                        activity_message.text = msg;
        if (!isAndroid) {
                ai.show();
                window.open();
        } else {
                actInd.show();
        }
        shown = true;

        // Setting a timeout in case it is taking too long to complete the task
        if(maxTime!==false){
                timeout = setTimeout(function() {
                        if (shown) {
                                hide();
                                /// Calling the timeoutCallback if specified. 
                                timeoutCallback && timeoutCallback();
                                /// Clearing the timeoutCallback
                                timeoutCallback = null; 
                        }
                }, maxTime);
        }
}

/// Hides the indicator
function hide(){
        timeout && clearTimeout(timeout);
        
        /// If hidden already, there is nothing to do here
        if (!shown)
                return;
                
        if (!isAndroid) {
                /// Closing the modal window
                window.close();
                shown = false;
        } else{
                actInd.hide();
        }       
        shown = false;
}

/// Toggled the activity window state
function toggle(){
        if (actInd.visible)
                actInd.hide();
        else
                actInd.show();
}

module.exports.show = show;
module.exports.hide = hide;
module.exports.toggle = toggle;
module.exports.setText = setText;
module.exports.setTimeoutCallback = setTimeoutCallback;
module.exports.setMaxTime = setMaxTime;
module.exports.shown = shown;
///CONSTANTS
module.exports.TIME_DEFAULT = 20000;
module.exports.TIME_SHORT = 5000;
module.exports.TIME_LONG = 30000;
module.exports.TIME_INFINITE = false;