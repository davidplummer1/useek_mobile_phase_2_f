var args = arguments[0] || {};
$.navBar.parentController = $;
$.navBar.setTitle('Video');
$.navBar.toggleBackButton(true);
$.useAndroidBack = true;
$.data = [];

//make a game level model
var level = Alloy.createModel("level", args.level);
level.resetLevel({
	gameId : args.id,
	parent : $
});

/** deconstructor for this window */
$.close = function(e) {
	$.video.stop();
	Alloy.Globals.mainWindow.closeWindow({
		name : 'video',
		animate : true
	});
	Alloy.Globals.activityIndicator.hide();
};

function hideActivityIndicator(e) {
	Alloy.Globals.activityIndicator.hide();
}

/*
 * when video play dealay, activity indicator will cover.
 */
$.video.addEventListener('load', hideActivityIndicator);

/*
 * when video play end, app will send user's total score to server.
 */
$.video.addEventListener('complete', function(e) {
	//send total score to server here
	level.finishLevel();
});

/*
 * this function makes to know user's clicking coordination is correct or not
 */
$.video.addEventListener('click', function(e) {
	Ti.API.info(e);
	level.evaluateClick(e);

});

//this is for checking listview. we will add this feature after coordination function working properly.


var offers = level.get("video").offers;
for (var i in offers) {
	$.data.push({
		template : 'template',
		properties : {
			height : Ti.UI.SIZE
		},
		pic : {
			image : level.get("video").sponsors[i].logo_url
		},
		point : {
			text : parseInt(level.get("video").offers[i].worth) + ' points'
		},
		title : {
			text : level.get("video").offers[i].description
		}
	});

}
$.section.setItems($.data);

$.headerTitle.text = level.get("video").earnable_points + ' POINTS';
$.footerTitle.text = 'REWARDS $' + level.get("video").earnable_rewards;

$.listView.addEventListener('itemclick', function(e) {
	var item = e.section.getItemAt(e.itemIndex);
	//mark finding logo will be here.
});


/** starts the game */
function playLevel() {
	Alloy.Globals.activityIndicator.show("Loading...");
	level.playLevel();
}

//event listener to start the game when this page opens
$.getView().addEventListener("open", playLevel);

		
/*
 * this function makes landscape view so user can see fullscreen size of video instead of video full screen proerty.
 * because if video set fullscreen, app can't handle eventListener.
 * if parameter is true, it will change backgroundColor, listview height, canvas width and height
 */

function getLandscape(value) {

	if (value == true) {
		Ti.UI.setBackgroundColor('black');
		$.container.layout = null;
		$.listView.height = 0;
		$.viewCanvas.width = Alloy.Globals.pH;
		$.viewCanvas.height = Alloy.Globals.videoDimension.width;
	} else {
		Ti.UI.setBackgroundColor(Alloy.Globals.colors.themeColor);
		$.viewCanvas.width = Alloy.Globals.videoDimension.width;
		$.viewCanvas.height = Alloy.Globals.videoDimension.height;
		$.container.layout = "vertical";
		$.listView.height = null;
	}

}

//$.win.orientationModes = [Ti.UI.LANDSCAPE_LEFT];
//getLandscape(true);



/*
 * it makes landscape view of video using rotation.
 * if orientation is left or right, it will rotate 90 or -90 degree
 * but it is portrait, it will make normal view.
 */


Ti.Gesture.addEventListener('orientationchange', function(e) {

	if (e.orientation == 3) {

		getLandscape(true);
		$.viewCanvas.transform = Ti.UI.create2DMatrix().rotate(90);

	} else if (e.orientation == 4) {

		getLandscape(true);
		$.viewCanvas.transform = Ti.UI.create2DMatrix().rotate(-90);

	} else if (e.orientation == 1 || e.orientation == 2) {
		getLandscape(false);
		$.viewCanvas.transform = Ti.UI.create2DMatrix().rotate(0);

	}

});



/*
 * when video play is completed, it will make normal view.
 */

$.video.addEventListener('complete', function(e){
	getLandscape(false);
	$.viewCanvas.transform = Ti.UI.create2DMatrix().rotate(0);
});

