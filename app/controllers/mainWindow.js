/**
 * 
 * The controller serves as the main window manager for the application. Additional controllers
 * are loaded into this window managers $.contentView section as if they were pages.
 * Windows can still be opened outside of this window manager using Alloy.createController.
 * 
 * 
 * The following functions will be called by this window manager if you define then in your windows (views)
 * 
 * @param {Function} $.init
 * This is executed when the controller is created but before it's added to the window manager
 * 
 * @param {Function} $.focused
 * This is executed on a view when it moves to the top of the window stack due to all views on top of it being closed.
 * 
 * @param {Function} $.opened
 * This function is executed when a window (view) becomes visible.
 * 
 * @param {Function} $.closed
 * This function is executed as a window (view) is being deleted from the stack.
 */



Alloy.Globals.mainWindow = $;
$.leftMenu.parent = $;
$.windows = [];

/** the coordinates for the beginning and ending position of the window during the menu revelation */
$.sliderPositions = {
	closed : {
		left : 0,
		right : 0
	},
	opened : {
		left : "80%",
		right : "-80%"
	}
};

/** slide the left menu right and left */
function toggleLeftMenu() {
	
	//if the left menu is visible, hide it
	if ($.leftMenu.on) {
		$.hideEventMask();
		$.contentView.animate({
			duration : 300,
			left : $.sliderPositions.closed.left,
			right : $.sliderPositions.closed.right
		});

	} else {
		//show the menu
		$.showEventMask();
		$.contentView.addEventListener("swipe", closeMenu);
		$.contentView.animate({
			duration : 300,
			left : $.sliderPositions.opened.left,
			right : $.sliderPositions.opened.right
		});

	}

	$.leftMenu.on = !$.leftMenu.on;
};

/** close the menu when the content view is swiped from right the left
 * @param {object} e the swipe event being fired by the content view
 **/
function closeMenu(e) {
	if (e.direction === "left") {
		$.contentView.removeEventListener("swipe", closeMenu);
		$.leftMenu.on = true;
		toggleLeftMenu();
	}
}

exports.toggleLeftMenu = toggleLeftMenu;

/**
 * @event	androidback
 * Triggered when the user clicks on the android back button
 * if the current window has $.useAndroidBack set to true, then the window will be closed
 * when the android back button is pressed
 */
$.mainWindow.addEventListener('androidback', function() {

	//if the current window should not be closed when android back is fired, then stop
	if (!Alloy.Globals.getCurrentWindow().useAndroidBack) {
		return;
	}
	closeWindow({
		animate : true
	});
});


/** open a new view as a window 
 * @param {Object} opts
 * configuration for the controller being opened
 * @param {Object} opts.name the name of the controller to open (Alloy.createController(opts.name))
 * @param {Object} opts.data the JSON to be passed to the controller (Alloy.createController(opts.name,opts.data))
 * @param {Object} opts.animate	true: animate the view as it's being opened; false: don't animate the view
 * */
var openWindow = function(opts) {
	if (!opts.name) {
		return;
	}
	var controller = Alloy.createController(opts.name, opts.data || {});
	controller.init && controller.init();
	var view = controller.getView();
	if (opts.animate) {
		view.right = '-100%';
		view.left = '100%';
		$.contentView.add(view);

		//if the user specifies an animation callback, execute it
		if ( typeof (controller.opened) === "function") {
			var animation = Ti.UI.createAnimation({
				duration : '300',
				left : 0,
				right : 0
			});
			animation.addEventListener("complete", controller.opened);
			view.animate(animation);
			if(OS_IOS){
				view.applyProperties({
					opacity : 0.0
				});
				require("alloy/animation").fadeIn(view,300);
			}
		} else {
			view.animate({
				duration : '300',
				left : 0,
				right : 0
			});
			if(OS_IOS){
				view.applyProperties({
					opacity : 0.0
				});
				require("alloy/animation").fadeIn(view,300);
			}
		}
	} else {

		$.contentView.add(view);
		if(typeof(controller.opened) == "function"){
			controller.opened();
		}
		
	}

	windowManager.push(controller);

};
exports.openWindow = openWindow;

/** closes the current view
 * @param {Object} opts
 * The closure options for this window
 * @param {Boolean} opts.animate	if true, the window will be animated as it's closed
 * @param {Boolean} opts.force		
 *
 * */
var closeWindow = function(opts) {

	windowManager.pop((opts && opts.animate ) || false, (opts && opts.force) || true);
	try{
					//attempt to call the $.focused method for the top
					if($.windows.length > 0 && typeof($.windows[$.windows.length-1].focused) == "function"){
						$.windows[$.windows.length-1].focused();
					}
				}catch(ex){
					Ti.API.error(ex);
				}
};
exports.closeWindow = closeWindow;

// Closes all open windows
var closeAllWindows = function() {

	windowManager.killAll();
};
exports.closeAllWindows = closeAllWindows;

/** opens a new window and closes all windows beneath it
 * @param {Object} opts
 * configuration for the controller being opened
 * @param {Object} opts.name the name of the controller to open (Alloy.createController(opts.name))
 * @param {Object} opts.data the JSON to be passed to the controller (Alloy.createController(opts.name,opts.data))
 * @param {Object} opts.animate	true: animate the view as it's being opened; false: don't animate the view
 * */
var openAndCloseAllWindows = function(opts) {
	if (!opts.name) {
		return;
	}
	var controller = Alloy.createController(opts.name, opts.data || {});

	controller.init && controller.init();
	$.contentView.add(controller.getView());
	if(typeof(controller.opened) == "function"){
			controller.opened();
		}
	windowManager.killAll();
	windowManager.push(controller);
};
exports.openAndCloseAllWindows = openAndCloseAllWindows;

/** Handles the creation and destruction of new windows.
 *  Since we are only working with view, not heavyweight windows,
 *  we need this to handle android back buttons, and do some cleanup
 **/
var WindowManager = function() {
	var windows = $.windows = [];
	this.push = function(wd) {

		windows.push(wd);
		$.windows = windows;
		
		
		//if the underlying window has a blurred function, execute it
		if($.windows.length > 1 && typeof($.windows[$.windows.length-2].blurred) == "function"){
			$.windows[$.windows.length-2].blurred();
		}
	};
	
	/** remove the top window from the window stack 
	 * @param {Boolean} animate 	true : animate the window as it's being destroyed
	 * @param {Boolean} force		true : remove the window from the window stack even if there's nothing beneath it
	 * */
	this.pop = function(animate, force) {
		if ((windows.length > 1) || (force && windows.length > 0)) {
			var _wd = windows.pop();
			var view = _wd.getView();

			
			if (animate) {
				if(OS_IOS){
				
				require("alloy/animation").fadeOut(view,300);
			}
				view.animate({
					duration : 300,
					right : '-100%',
					left : '100%'
				}, function() {

					// Calling the destroy method only if the controller supports it
					if (_wd.close) {
						if (_wd.closeMethod === 'async') {
							/// If the window closes asynchronoysly, then call close and remove it on the callback
							_wd.close(function() {
								$.contentView.remove(view);
							});
						} else {
							/// Closing normally
							_wd.close();
							/// Removing from the view
							$.contentView.remove(view);
						}
					} else {
						/// This controller doesn't hava a cloe method, so just remove it
						$.contentView.remove(view);
					}
					_wd = null;
					view = null;
					
					try{
						//attempt to call the $.focused method for the top
						if(windows.length > 0){
							windows[windows.length-1].focused && windows[windows.length-1].focused();
						}
					}catch(ex){
						alert(ex);
					}
				});
			} else {

				// Calling the destroy method only if the controller supports it
				if (_wd.close) {
					if (_wd.closeMethod === 'async') {
						/// If the window closes asynchronoysly, then call close and remove it on the callback
						_wd.close(function() {
							$.contentView.remove(view);
						});
					} else {
						/// Closing normally
						_wd.close();
						/// Removing from the view
						$.contentView.remove(view);
					}
				} else {
					/// This controller doesn't hava a cloe method, so just remove it
					$.contentView.remove(view);
				}
				_wd = null;
				view = null;
				
				
			}

		}
		
		
	};
	this.killAll = function() {
		_.each(windows, function(_wd) {

			//call the closed method for this
			if(typeof(_wd.closed) == "function"){
				 _wd.closed();
				}
			$.contentView.remove(_wd.getView());
			_wd = null;
		});
		windows = [];
	};
	this.getCurrentWindow = function() {
		return (windows && (windows.length > 0) && windows[windows.length - 1]) || [];
	};

};



/** display a an invisible event mask that prevents underlying events from firing */
$.showEventMask = function() {
	$.eventMask.addEventListener("swipe", hideMenuViaEventMask);

	//set the properties to show the event mask
	$.eventMask.applyProperties({
		top : Alloy.Globals.navigationBarHeight,
		left : $.sliderPositions.opened.left,
		right : $.sliderPositions.opened.right
	});
};

/** hide an invisible event mask that prevents underlying events from firing */
$.hideEventMask = function() {
	$.eventMask.removeEventListener("swipe", hideMenuViaEventMask);

	//set the properties to hide the event mask
	$.eventMask.applyProperties({
		top : Ti.Platform.displayCaps.platformHeight,
		left : $.sliderPositions.closed.left,
		right : $.sliderPositions.closed.right
	});
};

/** event handler to hide the left menu if the user swipes right-to-left on the event mask
 *
 * @param {Object} event the swipe event fired by the event mask
 */
function hideMenuViaEventMask(event) {
	if (event.direction === "left") {
		toggleLeftMenu();
	}
}

//lock portrait orientation for android handhelds
if (OS_ANDROID && !require("tools").isTablet()) {
	$.getView().orientationModes = [Ti.UI.PORTRAIT];
}

// Opening and closing the view and activity indicator within the timeout parameters
$.fadeOutSplash = function() {

	//if the splash window has already been removed, stop
	if ($.splashHidden) {
		return;
	} else {
		require("alloy/animation").fadeOut($.splash, 400, function() {
			$.removeSplash();
			$.splashHidden = true;
		});
	}
};

/** get rid of the splash screen view */
$.removeSplash = function() {
	$.getView().remove($.splash);
};

/** toggle the landscape orientation of the main window
 * @params {boolean} [landscapeOn] true: turn landscape on; false: turn landscape off
 * */
$.toggleLandscapeOrientation = function(landscapeOn) {
	if (landscapeOn) {
		$.getView().applyProperties({
			orientationModes : [Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT]
		});

	} else {
		$.getView().applyProperties({
			orientationModes : [Ti.UI.PORTRAIT]
		});
	}
};

Ti.Gesture.addEventListener("orientationchange", function(e) {
	Ti.API.info(JSON.stringify(e));
});

/** reveal the left menu (this is normally called during the start-up process after certain views have been loaded) */
$.revealLeftMenu = function() {
	if ($.leftMenu.getView().visible == false) {
		$.leftMenu.getView().show();
	}
};

function removeSplashVideo(){
	$.splashVideo.removeEventListener("complete",removeSplashVideo);
	require("alloy/animation").fadeOut($.splashVideo,300,function(){
	$.getView().remove($.splashVideo);
	checkUserSession();
	});
}
$.showSpecialSplashScreen = function() {
	$.splashVideo.addEventListener("complete", removeSplashVideo);
	$.splashVideo.applyProperties({
		autoplay : true,
		url : "/vids/useekSplash.mp4",
		visible : true
	});
};

/** run an animation to reveal the splash screen image */
$.showSplashImage = function() {
	
	if(OS_IOS){
		require("alloy/animation").fadeIn($.splashImage, 300,checkUserSession);
	}else{
		
		//load the image on a 1-second delay in order to give the screen enough time to adjust to the forced orientation
		setTimeout(function(){
			$.splashImage.setImage("/images/default2.png");
			require("alloy/animation").fadeIn($.splashImage, 300,checkUserSession);
		},1000);

	}
};

//if the app has been started for the first time, show a special splash screen
if (!Ti.App.Properties.getBool("appStartedBefore", false)) {
	Ti.App.Properties.setBool("appStartedBefore", true);
	$.getView().addEventListener("open", $.showSpecialSplashScreen);
}

// else, show the standard splash screen
else {
	$.getView().addEventListener("open", $.showSplashImage);
}

/** check the user's session */
function checkUserSession() {
	// openWindow({
		// name : 'login'
	// });
	// setTimeout(function() {
		// $.fadeOutSplash();
	// }, 2000);
	// return;
	
	
	var user = Alloy.Models.instance('users');
	Alloy.Globals.activityIndicator.show("Loading...");
	user.refreshSession({
		success : function(e) {

			user.toggleSessionRefresher(true);

			$.leftMenu.setMode('user');

			Alloy.Globals.activityIndicator.hide();

			//go to home window
			openWindow({
				name : 'main'
			});
			setTimeout(function() {
				$.fadeOutSplash();
			}, 2000);

		},
		error : function() {

			Alloy.Globals.activityIndicator.hide();

			$.leftMenu.setMode('user');
			//session expired : show login window
			openWindow({
				name : 'login'
			});
			setTimeout(function() {
				$.fadeOutSplash();
			}, 2000);
		}
	});
	
}

var windowManager = $.windowManager = new WindowManager();