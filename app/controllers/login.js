/**
 * @fileOverview	It makes to login user and guest.
 * @name			login.js
 */

//get push notification module
var push = require("basicPush");

var user = Alloy.Models.instance('users');
//Ti.API.info(Ti.App.Properties.getString('login_id') + ", " + Ti.App.Properties.getString('login_password'))

//If remember me swich is on, it will remember id and password.
if (Ti.App.Properties.getString('login_id') && Ti.App.Properties.getString('login_password')) {
	$.email.value = Ti.App.Properties.getString('login_id');
	$.password.value = Ti.App.Properties.getString('login_password');
	$.switchRemember.applyProperties({
		value : true
	});
}

//When click login button with email and password value, it will allow to login.
$.buttonLogin.addEventListener('click', login);

function login(e) {
	$.email.blur();
	$.password.blur();
	if ($.email.value.trim().length == 0 || $.password.value.trim().length == 0) {
		alert("The email and password fields cannot be blank.");
		return;
	}
	//check if you're online
	if (!Alloy.Globals.isOnline()) {
		return;
	}

	Alloy.Globals.activityIndicator.show('Logging in...');

	//when user checked remember me box.
	if ($.switchRemember.value == true) {
		Ti.App.Properties.setString('login_id', $.email.value);
		Ti.App.Properties.setString('login_password', $.password.value);
	} else {
		Ti.App.Properties.removeProperty('login_id');
		Ti.App.Properties.removeProperty('login_password');

	}

	//attempts to login a user and store his/her session in Ti.App.Properties.getString("sessionToken");
	user.login({
		auth_key : $.email.value, //"ci_test@clearlyinnovative.com", // "heedoo@clearlyinnovative.com", //"ci_test@clearlyinnovative.com",
		password : $.password.value, //"password",
		url : Alloy.CFG.useekWebService + "/sessions"
	}, {
		success : function(e) {
			Alloy.Globals.activityIndicator.hide();

			Alloy.Globals.mainWindow.openWindow({
				name : 'main',
				animate : true
			});
			//Alloy.createController('main').getView().open();
		},
		error : function(e) {
			Alloy.Globals.activityIndicator.hide();
			alert("Sorry, email or password is incorrect");
		}
	});
}

/** attempt to login as a guest */
function guestLogin() {
	hideKeyboard();

	if (!Alloy.Globals.isOnline()) {
		return;
	}

	Alloy.Globals.activityIndicator.show('Logging in...');

	user.login({
	}, {
		success : function(e) {
			Alloy.Globals.activityIndicator.hide();

			Ti.App.Properties.setBool("guestLogin", true);

			Alloy.Globals.mainWindow.openWindow({
				name : 'main',
				animate : true
			});
			//Alloy.createController('main').getView().open();
		},
		error : function(e) {

			Alloy.Globals.activityIndicator.hide();
			alert("email or password is not match");
		}
	});

}

/** event listener to open the registration screen */
$.buttonCreate.addEventListener('click', function(e) {

	hideKeyboard();
	//Alloy.createController('registration').getView().open();
	Alloy.Globals.mainWindow.openWindow({
		name : 'registration',
		animate : true
	});
});

$.labelForgot.addEventListener('click', function(e) {
	hideKeyboard();
	Alloy.Globals.mainWindow.openWindow({
		name : 'forgotPassword',
		animate : true
	});
});

/**	event listener to handle logging in as a guest */
$.buttonGuestLogin.addEventListener("click", guestLogin);

/**
 * @event change
 * Triggered when the state of the remember me switch is toggled on/off
 */
$.switchRemember.addEventListener("change", function() {
	Ti.App.Properties.setBool("rememberSession", $.switchRemember.value);
});

//set the value of the remember me switch
$.switchRemember.applyProperties({
	value : Ti.App.Properties.getBool("rememberSession", false)
});
//click event listener to display the facebook login modal
$.viewFacebookLogin.addEventListener("click", require("facebookModule").loginThroughFacebook);

require("alloy/animation").fadeIn($.getView(), 300, function() {
	Alloy.Globals.mainWindow.revealLeftMenu();
});

/** method called by the window manager after this controller is made visible */
$.opened = function() {

	//add the keyboard toolbar functionality

		if (OS_IOS) {
			$.password.keyboardToolbar = $.email.keyboardToolbar = $.keyboardToolbar;
			$.cancel.addEventListener("click", hideKeyboard);
		}
	
	//focus the password when the email field is skipped
	$.email.addEventListener("return", focusPassword);

	//login when the password field is returned
	$.password.addEventListener("return", login);

	//warn the user about network connectivity
	Alloy.Globals.warnTheUser();
};

/** de-constructor */
$.closed = function() {

	//remove the keyboard toolbar functionality
	if (OS_IOS) {
		$.cancel.removeEventListener("click", hideKeyboard);
	}
	//focus the password when the email field is skipped
	$.email.removeEventListener("return", focusPassword);

	//login when the password field is returned
	$.password.removeEventListener("return", login);

};

/** hide the software keyboard by blurring the login fields */
function hideKeyboard() {
	$.email.blur();
	$.password.blur();
}

/** give focus to the password text field */
function focusPassword(e) {
	$.password.focus();
}

