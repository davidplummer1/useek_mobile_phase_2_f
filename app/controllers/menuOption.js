var args = arguments[0] || {};

$.option.text = args.label;

if($.option.text == 'Menu'){
	$.option.applyProperties({
		color : Alloy.Globals.colors.trueBlue
	});
	$.arrow.applyProperties({
		visible : false
	});
}

//store the menu option data inside
$.getView().menuOptionData = args;
$.getView().menuOptionLabel = $.option;

$.menuOption.addEventListener('click', function() {
	//if the user has tapped the current menu option, do nothing
	/*if($.getView().menuOptionData.label ===  Alloy.Globals.mainWindow.leftMenu.getCurrentMenuOptionLabel()){
		
		if(["menu","logout"].indexOf($.getView().menuOptionData.label.toLowerCase()) === -1){
			return;
		}
	}*/
	Alloy.Globals.mainWindow.leftMenu.changeMenuOption($.getView().menuOptionData.label);
	
	if (args.type === 'page') {
		if (args.method === 'openOnly') {

			Alloy.Globals.mainWindow.openWindow({
				name : args.action,
				data : args.data,
				animate : args.animate
			});
		} 
		else {
			Alloy.Globals.mainWindow.openAndCloseAllWindows({
				data : args.data ,
				name : args.action
			});
		}
	}else if (args.type === 'function') {
		args.action();
		// working on registration back action
	} 

	Alloy.Globals.mainWindow.toggleLeftMenu();
});
