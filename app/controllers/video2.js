var videoJSON = {
	"Finding Nemo" : "http://d3jr0h7offc3h1.cloudfront.net/USeek_Nemo/USeek_Nemo.m3u8",
	"Wizard of Oz" : "http://d3jr0h7offc3h1.cloudfront.net/USeek_WizardOfOz/USeek_WizardOfOz.m3u8",
	"Justin Bieber Part 1" : "http://d3jr0h7offc3h1.cloudfront.net/USeek_Boyfriend/USeek_Boyfriend_Part1.m3u8",
	"Justin Bieber Part 2" : "http://d3jr0h7offc3h1.cloudfront.net/USeek_Boyfriend/USeek_Boyfriend_Part2.m3u8",
	"Justin Bieber Part 3" : "http://d3jr0h7offc3h1.cloudfront.net/USeek_Boyfriend/USeek_Boyfriend_Part3.m3u8",
	"Lonely Island Boys: Captain Jack Sparrow" : "http://d3jr0h7offc3h1.cloudfront.net/USeek_JackSparrow/USeek_JackSparrow.m3u8",
	"Twilight New Moon" : "http://d3jr0h7offc3h1.cloudfront.net/USeek_Twilight/USeek_Twilight.m3u8"
};

$.switchVideo = function(){
Ti.App.Properties.setInt("index",(Ti.App.Properties.getInt("index",0)+1) % 7);
$.getView().orientationModes = [Ti.UI.PORTRAIT, Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT];
$.videoPlayer.stop();
$.videoPlayer.url = videoJSON[Object.keys(videoJSON)[Ti.App.Properties.getInt("index",0)]];
$.videoPlayer.play();
}
$.switchVideoButton.addEventListener("click",$.switchVideo);


$.videoPlayer.addEventListener("click", function(e) {
	$.x.text = "x = " + e.x;
	$.y.text = "y = " + e.y;
	$.height.text = "height = " + Alloy.Globals.gameDimensions.video.height;
	$.width.text = "width = " + Alloy.Globals.gameDimensions.video.width;
	
	game.processClick(e);
});
$.videoPlayer.addEventListener("dblclick", function(e) {
	$.x.text = "x = " + e.x;
	$.y.text = "y = " + e.y;
	$.height.text = "height = " + Alloy.Globals.gameDimensions.video.height;
	$.width.text = "width = " + Alloy.Globals.gameDimensions.video.width;
	
	game.processClick(e);
});

//sample calls for the local game api
var Game = function(){};
Game.prototype.init = function(gameData){
	this.gameData = gameData;
	
	//load the user's game data (did (s)he play this game before?)
};


Game.prototype.start = function(){
	
	//verify the video exists
	
	//verify the game data exists
	
	//
	this.clicks = [];
	this.successes = {};
};

Game.prototype.processClick = function(e){
	this.moveTheDot(e);
	Ti.API.info(JSON.stringify(this.convertCoordinatesToPercentages(e)));
};

Game.prototype.moveTheDot = function(e){
	
	if(OS_IOS){
		$.dot.top = (e.y - 5) +( OS_IOS ? "dp" : "px");
		$.dot.left = (e.x - 5) + (OS_IOS ? "dp" : "px");
		Ti.API.info(JSON.stringify({ x : $.dot.left, y : $.dot.top }));
	
	}else{
		$.dot.top = (e.y - require("alloy/measurement").dpToPX(parseInt($.dot.getHeight()))) + "px";
		$.dot.left = (e.x - require("alloy/measurement").dpToPX(parseInt($.dot.getWidth()))) + "px";
		Ti.API.info(JSON.stringify({ x : $.dot.left, y : $.dot.top }));
	}
};

Game.prototype.finish = function(){
	//calculate how many points the user has earned
	this.calculateUserPointsLocally();
	
	//send the results to the server for validate
	this.sendUserClicksToTheServer();
};


Game.prototype.convertCoordinatesToPercentages = function(coordinates){
	
	//if the coordinates don't exist, return an empty JSON object
	if(!coordinates || !coordinates.x || !coordinates.y ){
		return {};
	}
	
	var coordinatePercentages = {
		x : coordinates.x / parseInt(Alloy.Globals.gameDimensions.video.width),
		y : coordinates.y / parseInt(Alloy.Globals.gameDimensions.video.height) 
	};
	return coordinatePercentages;
};


var game = new Game();


