/**
 * displays a history of charitable donations made to the selected charity
 * 
 */

var args = arguments[0] || {};

$.navBar.setTitle('Give Back History');
$.useAndroidBack = true;
$.navBar.setSearchIcon(false);
$.navBar.toggleBackButton(true);

var givebacks = Alloy.Collections.instance("givebacks");

/** fetch the charity history when this window is opened */
$.opened = function() {
	Alloy.Globals.activityIndicator.show("Loading...");
	fetchData();
};

/** fetch the charity history */
function fetchData() {
	givebacks.fetch({
		success : function(e) {

			var data = [];

			//parse the history data into list view elements
			givebacks.each(function(item) {
				data.push({
					template : 'template',
					properties : {
						height : Ti.UI.SIZE
					},
					logo : {
						image : item.get("charity").image_urls.medium
					},
					name : {
						text : item.get("charity").name
					},
					amount : {
						text : "$" + require("tools").roundDecimal(item.get("amount"))
					}

				});
			});
			$.section.setItems(data);

			Alloy.Globals.activityIndicator.hide();
		},
		error : function(e, raw) {
			Alloy.Globals.activityIndicator.hide();
		}
	});
}