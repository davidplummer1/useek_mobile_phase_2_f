//$.index.open();

/* Set the various game collections to automatically update the level collection when 
the sync event is fired by the sync adapter. */
Alloy.Collections.instance("games").on("sync",Alloy.Collections.instance("games").updateLevelCollection);
Alloy.Collections.instance("featured_games").on("sync",Alloy.Collections.instance("featured_games").updateLevelCollection);
Alloy.Collections.instance("active_games").on("sync",Alloy.Collections.instance("active_games").updateLevelCollection);
Alloy.Collections.instance("recommendation_games").on("sync",Alloy.Collections.instance("recommendation_games").updateLevelCollection);


/** function used to test for an internet connection
 * @parameter {String} optionalMessage
 * @return {Boolean}	true: the internet connection is valid; false: no internet connection could be found
 * */
Alloy.Globals.isOnline = function(optionalMessage) {

	if (!Ti.Network.getOnline()) {
		alert(optionalMessage || "You must have an Internet Connection in order to use this feature.");
		Alloy.Globals.activityIndicator.hide();
		return false;
	} else {
		return true;
	}
};

/** the function that initializes the app */
function startUp() {

	//if the user was previously logged in as a guest, clear the old session data
	if (Ti.App.Properties.getBool("guestLogin", false)) {

		Ti.App.Properties.removeProperty("guestLogin");
		Ti.App.Properties.removeProperty("sessionToken");
		Ti.App.Properties.removeProperty("userId");
	}
	Alloy.Globals.index = $;

	if (!Ti.App.Properties.getBool("rememberSession", false)) {
		require("facebookModule").hardLogout();
		Ti.App.Properties.removeProperty("sessionToken");
		Ti.App.Properties.removeProperty("userId");
		Ti.App.Properties.removeProperty('login_id');
		Ti.App.Properties.removeProperty('login_password');
	}

	var mainWindow = Alloy.createController('mainWindow', {
		parent : $
	});
	mainWindow.getView().open();

}

//if the user isn't connected to the internet, stop the app
if (Alloy.Globals.isOnline("You need an internet connection in order to use this app. Please check your network connection and restart the app.")) {
	
	startUp();
}

/** warn the user about their network connectivity */
Alloy.Globals.warnTheUser = function() {
	setTimeout(function() {
		if (Alloy.Globals.warned) {
			return;
		}
		//if the user isn't on a wifi or lan connection, warn them
		if ([Ti.Network.NETWORK_WIFI, Ti.Network.NETWORK_LAN].indexOf(Ti.Network.networkType) == -1) {
			alert("Because of this app's data streaming capabilities, it's recommended that you use a WiFi connection to avoid excessive costs.");
			Alloy.Globals.warned = true;
		}
	}, 3000);
};

//set global event listeners to monitor the foreground & background state of the app
Alloy.Globals.appPaused = false;
function pauseApp(){
	Alloy.Globals.appPaused = true;
}
function resumeApp(){
	Alloy.Globals.appPaused = false;
}
Ti.App.addEventListener("resume",resumeApp);
Ti.App.addEventListener("resumed",resumeApp);
Ti.App.addEventListener("pause",pauseApp);
Ti.App.addEventListener("paused",pauseApp);
