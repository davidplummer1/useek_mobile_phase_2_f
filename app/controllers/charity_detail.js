/** 
 * 
 * displays some of the details about this charity
 * 
 * 
 * 
 */

var args = arguments[0] || {};

$.navBar.setTitle('Give Back Detail');
$.useAndroidBack = true;
$.navBar.setSearchIcon(false);
$.navBar.toggleBackButton(true);

//grab the charity details from the charities collection

var charityJSON = Alloy.Collections.instance("charities").where({ id : args.charityId })[0].toJSON();

//populate this controller with the charity details
$.labelName.text = charityJSON.name;
$.labelDescription.text = charityJSON.description;
$.labelURL.text = charityJSON.url;
$.labelContactEmail.text = charityJSON.contact_email;
$.labelContactAddress.text = charityJSON.contact_address;
$.labelContactCity.text = charityJSON.contact_city;
$.labelContactState.text = charityJSON.contact_state;
$.labelContactCountry.text = charityJSON.contact_country;
$.labelContactZipcode.text = charityJSON.contact_postal_code;
$.labelContactPhone.text = charityJSON.contact_phone;

