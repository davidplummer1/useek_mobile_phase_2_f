var args = arguments[0] || {};
debugger;
$.args = args;
$.parent = $.args.parent;
$.parent.child = $;
var level;
var lastPosition = 0;
var ctrPosition = 0;

$.activities = {};
//holds boolean state for several asynchronous processes for this controller

$.sponsorsArr = args.sponsors;

//Top sponsor viewBox width will be device height minus right side width which is 40.
var screen = {
	width : Math.max(Alloy.Globals.pH, Alloy.Globals.pW),
	height : Math.min(Alloy.Globals.pH, Alloy.Globals.pW)
};

var barUnits = Alloy.isTablet ? 120 : 60;

barUnits = OS_ANDROID ? require("alloy/measurement").pxToDP(barUnits) : barUnits;

/** make 16:9 ratio for video canvas */
function adjustVideoSize() {
	var dimension = {
		width : screen.width - barUnits,
		height : screen.height - barUnits
	};
	//if the screen is too short, set the height and let the extra space hang on the sides
	if ((dimension.width / 16) > (dimension.height / 9)) {

		$.viewCanvas.applyProperties({
			height : dimension.height + Alloy.Globals.deviceUnitOfMeasure,
			width : (dimension.height / 9 * 16) + Alloy.Globals.deviceUnitOfMeasure
		});
		$.canvasOverlay.applyProperties({
			height : dimension.height + Alloy.Globals.deviceUnitOfMeasure,
			width : (dimension.height / 9 * 16) + Alloy.Globals.deviceUnitOfMeasure
		});
		$.video.applyProperties({
			height : dimension.height + Alloy.Globals.deviceUnitOfMeasure,
			width : (dimension.height / 9 * 16) + Alloy.Globals.deviceUnitOfMeasure
		});
	}
	//if the space is too narrow, set the width and let the extra space hang on the top and bottom
	if ((dimension.width / 16) < (dimension.height / 9)) {
		$.viewCanvas.applyProperties({
			width : dimension.width + Alloy.Globals.deviceUnitOfMeasure,
			height : (dimension.width / 16 * 9) + Alloy.Globals.deviceUnitOfMeasure
		});
		$.canvasOverlay.applyProperties({
			width : dimension.width + Alloy.Globals.deviceUnitOfMeasure,
			height : (dimension.width / 16 * 9) + Alloy.Globals.deviceUnitOfMeasure
		});
		$.video.applyProperties({
			width : dimension.width + Alloy.Globals.deviceUnitOfMeasure,
			height : (dimension.width / 16 * 9) + Alloy.Globals.deviceUnitOfMeasure
		});
	} else {
		//if this is a perfect 16:9 ratio, set the height and width as 100% of the container
		dimension = {
			width : dimension.width + Alloy.Globals.deviceUnitOfMeasure,
			height : dimension.height + Alloy.Globals.deviceUnitOfMeasure
		};
		$.viewCanvas.applyProperties(dimension);
		$.canvasOverlay.applyProperties(dimension);
		$.video.applyProperties(dimension);

	}
	Ti.API.debug('Screen dimensions: ' + JSON.stringify(dimension) + ' Units: ' + Alloy.Globals.deviceUnitOfMeasure);
	//$.viewCanvas.width = Alloy.Globals.pH - (Alloy.isTablet ? 80 : 40) + Alloy.Globals.deviceUnitOfMeasure;
	//$.viewCanvas.height = parseInt(parseInt($.viewCanvas.width) / 16 * 9) + Alloy.Globals.deviceUnitOfMeasure;
}

/** make the level active by hiding the activity indicator and setting the level's property to playing = true */
function activateLevel(e) {
	Ti.API.debug('In videoWin activateLevel...');

	level.levelIsPlaying = true;
	Alloy.Globals.activityIndicator.hide();
}

/** complete this level */
$.completeLevel = function(e) {
	Ti.API.info("completeLevel");
	if(Alloy.Globals.appPaused){ return; }else{ Ti.API.info("app isn't paused"); }
	
	Ti.API.debug('In video_win completeLevel...');
	$.video.removeEventListener('complete', $.completeLevel);
	if ($.levelComplete) {
		Ti.API.debug('In video_win completeLevel... already done, exiting!');
		return;
	}
	$.levelComplete = true;
	Ti.App.removeEventListener("paused", $.cleanup);

	//send total score to server here
	level.finishLevel({
		success : function(e) {
			var moreLikes = Alloy.Collections.instance("moreLikes");

			moreLikes.reset(e.recommendations);
			Alloy.Globals.activityIndicator.hide();

			//Open completed level page and pass level, parent, earned point and rewards data.
			$.videoCompletedController = Alloy.createController('video_completed', {
				data : level,
				parent : $, //<-- this parent meaning is video_win.js
				pointsEarned : level.pointsEarned,
				rewardsEarned : level.rewardsEarned,
				messages : e.messages || {}
			});
			$.videoCompletedController.getView().open();

		},
		error : function() {
			Alloy.Globals.activityIndicator.hide();
			$.getView().close();
		}
	});
};

/** starts the game */
$.playLevel = function() {
	
	//turn on the interruption event listeners
	enableInterruptionListener();
	
	Ti.API.debug('In videoWin playLevel...');

	Ti.App.addEventListener("paused", $.cleanup);

	if (OS_ANDROID) {
		$.setupLevel();

		Alloy.Globals.activityIndicator.show("Loading...");
		level.playLevel();
	} else {
		require("alloy/animation").popIn($.getView(), function() {
			Ti.API.debug('In videoWin playLevel IOS...');
			$.setupLevel();

			Alloy.Globals.activityIndicator.show("Loading...");
			level.playLevel();
		});
	}
};

//this function calling from child. it make to close window when user completed game and click close(X) button.
$.closeWin = function() {
	Ti.API.info("closeWin");
	if(Alloy.Globals.appPaused){ return; }else{ Ti.API.info("app isn't paused"); }
	args && args.parent && args.parent.getView() && args.parent.getView().close();
};

/*
 * Below code is phase two(with portrait and landscapde modes).
 * Phase one will support only landscape mode.
 */

$.setupLevel = function() {
	Ti.API.debug('In videoWin setupLevel...');

	$.data = [];
	level = Alloy.createModel("level", args.level);
	$.levelComplete = false;
	adjustVideoSize();
	$.data = [];
	$.scrollSponsors.removeAllChildren();
	//Loop through all sponsors and display video sponsor controller on scroll view.
	for (var i in $.sponsorsArr) {

		$.sponsors = Alloy.createController('components/video_sponsors', {
			image : $.sponsorsArr[i].logo_url,
			id : $.sponsorsArr[i].id
		}).getView();
		$.scrollSponsors.add($.sponsors);
	}

	//make a game level model

	level.resetLevel({
		gameId : args.id,
		controller : $
	});

	//display point dynamically.
	$.labelPoints.setText("--");
	//display reward dynamically.
	$.labelRewards.setText('$ --');

};

/** replay the current level */
$.replayLevel = function() {
	Ti.API.debug('In videoWin replayLevel...');

	$.child.getView().close();

	//if this is android, load the level on a split second delay
	if (OS_ANDROID) {
		setTimeout(function() {
			$.setupLevel();
			$.playLevel();
		}, 500);
	} else {
		$.setupLevel();
		$.playLevel();
	}
};

/** the function called when this window closes */
$.cleanup = function() {
	Ti.API.info("cleanup");
	if(Alloy.Globals.appPaused){ return; }else{ Ti.API.info("app isn't paused"); }
	Ti.API.debug('In videoWin cleanup...');

	Ti.App.removeEventListener("paused", $.cleanup);

	if (OS_ANDROID) {
		level.toggleVideoMonitor(false);
		$.levelComplete = true;
	}
	//the event listener to replay the current level
	Ti.App.removeEventListener("replayLevel", $.replayLevel);

	/**
	 * @event load
	 *
	 * Triggered when the video loads.
	 * Start playing the level
	 */
	$.video.removeEventListener('load', activateLevel);

	/*
	* when video play end, app will send user's total score to server.
	*/
	//  $.video.removeEventListener('complete', $.completeLevel); moved to $.completeLevel

	//event listener used to check if the user successfully clicked on a sponsor
	if (!OS_ANDROID) {
		$.video.removeEventListener('click', evaluateClick);
	} else {
		$.canvasOverlay.removeEventListener("click", evaluateClick);
	}

	if (OS_ANDROID) {
		clearInterval($.monitorVideo);
	}
	level.cancelLevel(true);
	//event listener to start the game when this page opens
	$.getView().removeEventListener("open", $.playLevel);
	$.getView().removeEventListener("close", $.cleanup);
	$.destroy();

};

/** show/hide the loading image container */
$.toggleLoadingImage = function(on) {
	if (!$.levelComplete) {
		if (on) {
			if (!Alloy.Globals.activityIndicator.shown) {
				Alloy.Globals.activityIndicator.shown = true;
				Alloy.Globals.activityIndicator.show("Loading...");

			}
			//require("alloy/animation").fadeIn($.loadingImageContainer,100);
		} else {
			if (Alloy.Globals.activityIndicator.shown) {
				Alloy.Globals.activityIndicator.shown = false;
				Alloy.Globals.activityIndicator.hide();
			}
			//require("alloy/animation").fadeOut($.loadingImageContainer,100);
		}
	}
};
/**
 * close the window with a fade out animation
 * @param {Function} callback the optional callback method to be fired when the window closes
 * */
$.fadeOutWindow = function(params) {
	Ti.API.info("fadeOutWindow");
	if(Alloy.Globals.appPaused){ return; }else{ Ti.API.info("app isn't paused"); }
	
	//turn off the interruption event listeners
	disableInterruptionListener();
	
	
	$.levelComplete = true;
	
	require("alloy/animation").fadeOut($.getView(), 300, function() {
		$.getView().close();

		if (OS_IOS) {
			params && params.callback && params.callback();
		}
	});
};
/**
 * check if the user successfully clicked on a sponsor
 * @param {Object} event the click event for the canvas overlay/video player
 */
function evaluateClick(event) {
	level.evaluateClick(event);
}

//the event listener to replay the current level
Ti.App.addEventListener("replayLevel", $.replayLevel);

/**
 * when video play dealay, activity indicator will cover.
 */
$.video.addEventListener('load', activateLevel);

/*
 * when video play end, app will send user's total score to server.
 */
$.video.addEventListener('complete', $.completeLevel);

if (OS_ANDROID) {
	// Some Android devices do not bubble the completed event with the stream ends, this attempts to catch it manually
	$.monitorVideo = setInterval(function() {
		Ti.API.debug("Current time: " + level.videoPlayer.currentPlaybackTime + " of " + level.videoPlayer.duration + " Last position: " + lastPosition + " - " + ctrPosition);
		if ((level.videoPlayer.currentPlaybackTime == 0 && lastPosition > 10000) || (level.videoPlayer.currentPlaybackTime == lastPosition && ctrPosition > 4)) {
			Ti.API.debug('Manually firing completeLevel');
			$.completeLevel();
		} else {
			if (lastPosition > 0 && lastPosition == level.videoPlayer.currentPlaybackTime) {
				ctrPosition += 1;
			} else {
				ctrPosition = 0;
			}
			lastPosition = level.videoPlayer.currentPlaybackTime;
		}
	}, 1000);
}

//event listener used to check if the user successfully clicked on a sponsor
if (!OS_ANDROID) {
	$.video.addEventListener('click', evaluateClick);
} else {
	$.canvasOverlay.addEventListener("click", evaluateClick);
}

/** tell the parent controller to close this window and display an error message
 *
 * @param {Boolean} stoppedOnPurpose	true: this level stopped peacefully; false: a bad internet connection has halted this level
 */
$.closeForAndroid = function(stoppedOnPurpose) {
	//Alloy.Globals.activityIndicator.hide();
	level.resetLevel();
	$.parent.stopLevelOnAndroid(stoppedOnPurpose);
};

//event listener to start the game when this page opens
$.getView().addEventListener("open", $.playLevel);
$.getView().addEventListener("close", $.cleanup);




/** pause the video & show the play button */
function pauseVideo(event) {
	togglePlayButtonOverlay(true);
	$.video.pause();
}

/** pause the video only */
function softPause(){
	$.video.setVisible(true);
	$.video.pause();
}
/** resume the video */
function resumeVideo() {
	togglePlayButtonOverlay(false);
	$.video.setVisible(true);
}

function togglePlayButtonOverlay(show) {

	//if the overlay is animating, wait
	if ($.activities.animating) {
		return;
	} else {
		$.activities.animating = true;
	}

	if (show) {

		//show the play button overlay
		$.playButtonDescription.animate(animations.playButtonDescription.show);
		require("alloy/animation").popIn($.playButton);
		require("alloy/animation").fadeIn($.playButtonOverlay, 300, function() {
			$.playButtonOverlay.applyProperties({
				touchEnabled : true
			});

			$.activities.animating = false;
		});
	} else {

		//hide the play button overlay
		$.playButtonDescription.animate(animations.playButtonDescription.hide);
		require("alloy/animation").fadeOut($.playButtonOverlay, 300, function() {
			$.playButtonOverlay.applyProperties({
				touchEnabled : false
			});
			$.playButton.applyProperties({ opacity : 0.0 });
			$.activities.animating = false;
			Ti.API.info("current position " + $.video.currentPlaybackTime);
			$.video.play();
			Ti.API.info($.video.playing + ", " + $.video.url);
		});
	}

}

/** various animation definitions */
var animations = {
	playButtonDescription : {
		show : Ti.UI.createAnimation({
			bottom : "30%",
			duration : 500,
			opacity : 1.0
		}),
		hide : Ti.UI.createAnimation({
			bottom : "10%",
			duration : 500,
			opacity : 0
		})
	}
};

/** turn on the event listeners to monitor the app's closure or interruption, pausing gameplay if need be */

function enableInterruptionListener(e){
	if(!Alloy.Globals.interruptionListenerSet){
		Ti.App.addEventListener("pause",pauseVideo);
		Ti.App.addEventListener("paused",pauseVideo);
		Ti.App.addEventListener("resume",softPause);
		Ti.App.addEventListener("resumed",softPause);
		Alloy.Globals.interruptionListenerSet = true;
	}
}

/** turn of the event listeners which were monitoring the app's closure or interruption */
function disableInterruptionListener(e){
	if(Alloy.Globals.interruptionListenerSet){
		Ti.App.removeEventListener("pause",pauseVideo);
		Ti.App.removeEventListener("paused",pauseVideo);
		Ti.App.addEventListener("resumed",softPause);
		Ti.App.addEventListener("resume",softPause);
		Alloy.Globals.interruptionListenerSet = false;
	}
}
