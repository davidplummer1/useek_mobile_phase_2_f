/**
 * @fileOverview	Register user's informaiton
 * @name			registration.js
 */
//get from scanned info
var args = arguments[0];
var moment = require('moment');
var user = Alloy.Models.instance('users');
Ti.API.info("users: " + JSON.stringify(user));

//include back button on title bar
$.useAndroidBack = true;
$.navBar.parentController = $;
$.navBar.setTitle("Profile");
$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());

//display avatar image.
if (user.get('user') == undefined || user.get('user').avatar.thumb == "/images/thumb/missing.png") {
	$.imgAvatar.image = '/images/user.png';
} else if(user.get('user').avatar.thumb !== "/images/thumb/missing.png") {
	$.imgAvatar.image = user.get('user').avatar.thumb;
}


/*
$.imgAvatar.addEventListener('click', function(e){

	//display option dialog.
	var dialog = Ti.UI.createOptionDialog({
		options : ['From my Gallery', 'Take a Photo', 'Cancel']
	});
	dialog.addEventListener('click', function(e) {
		if (e.index == 0) {
			//select photo : open photo album in device and display in photo box
			Titanium.Media.openPhotoGallery({
				success : function(e) {
					var image = e.media;

					Ti.API.debug('Our type was: ' + e.mediaType);

					if (e.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
						//Alloy.Globals.currentPhoto = e.media;
						$.imgAvatar.image = e.media;
						//save to server here!

					} else {
						alert("got the wrong type file =" + e.mediaType);
					}
				},
				cancel : function(e) {},
				error : function(e) {
					alert('Please try again');
				},
				mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO]
			});
			//end photogallery
		} else if (e.index == 1) {
			//Take photo and save in device and display in photo box

			Titanium.Media.showCamera({

				success : function(event) {
					var image = event.media;

					Ti.API.debug('Our type was: ' + event.mediaType);
					if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
						$.imgAvatar.image = event.media;
						//Alloy.Globals.currentPhoto = event.media;

					} else {
						alert("got the wrong type file =" + event.mediaType);
					}
				},
				cancel : function() {},
				error : function(error) {
					// create alert
					var a = Titanium.UI.createAlertDialog({
						title : 'Camera'
					});
					// set message
					if (error.code == Titanium.Media.NO_CAMERA) {
						a.setMessage('Please run on device');
					} else {
						a.setMessage('Unexpected error: ' + error.code);
					}
					// show alert
					a.show();
				},
				saveToPhotoGallery : true,
				allowEditing : true,
				mediaTypes : [Ti.Media.MEDIA_TYPE_VIDEO, Ti.Media.MEDIA_TYPE_PHOTO],
			});
			//end taking photo
		} else {}
	});
	dialog.show();
	
});
*/

$.labelName.text = (user.get('first_name')!== null && user.get('last_name') !== null) ? user.get('first_name') + " " + user.get('last_name') : "";
$.fName.value = user.get('first_name');
$.lName.value = user.get('last_name');
$.birth.text = user.get("birth_date") == null ? "" : require("tools").formatDateString(new Date(user.get('birth_date')));
$.email.value = user.get('email');

function blurTextFields() {
	$.fName.blur();
	$.lName.blur();
	$.email.blur();
	$.password.blur();
	$.confirm.blur();
}

//picker will show up on screen.
$.birth.addEventListener('click', focusBirthdatePicker);

/*
 * Done button set change birth value in label and color
 */
$.buttonDone.addEventListener('click', function(e) {
	//Ti.API.info($.picker.value)
	$.birth.text = moment($.picker.value).format('L');
	$.birth.color = 'black';
	
	require("alloy/animation").fadeOut($.viewPicker,200,function(){
		$.email.focus();
	});
	
});

//set max and min date picker
$.picker.minDate = new Date(1900, 0, 1);

//Update user's profile
$.buttonUpdate.addEventListener('click', function(e) {

	updateUser(JSON.stringify({
		"user" : {
			"first_name" : $.fName.value,
			"last_name" : $.lName.value,
			"birth_date" : $.birth.text, //birth_date value should be 01/01/1900 or 01.01.1900 format otherwise it will deny.
			"email" : $.email.value
			//"avatar" : $.imgAvatar.image
		}
	}));

});

/** attempt to update an account
 * @param {object} e the click event from the update button
 * */
function updateUser(updateInfo) {

	//check if you're online
	if (!require("onlineChecker").isOnline()) {
		return;
	}

	Alloy.Globals.activityIndicator.show('Loading..');
	
	$.fName.blur();
	$.lName.blur();
	$.email.blur();
	$.password.blur();
	$.confirm.blur();

	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (reg.test($.email.value) === false) {
		alert('Please Enter a valid email address.');
		return;
	}

	var client = Ti.Network.createHTTPClient();
	client.open("PUT", Alloy.CFG.useekWebService+"/users/" + user.get('id'));
	client.setRequestHeader('Content-Type', "application/json");
	client.setRequestHeader("Authorization", 'Token token="' + Ti.App.Properties.getString("sessionToken") + '"');
	client.onload = function(e) {
		
		//var json = JSON.parse(this.responseText);
		//Ti.API.info("json: " + JSON.stringify(json));
		try{
		var response = JSON.parse(client.responseText);

		//if there are any errors returned, stop
		if (response.messages.error.length > 0) {
			Alloy.Globals.activityIndicator.hide();
			alert(response.messages.error[0]);
			return;
		}
		//parse the user object and open the main window
		var user = Alloy.Models.instance("users");
		user.clear();
		user.set(response.user);
				
		Alloy.Globals.activityIndicator.hide();
		alert('Your profile updated successfully.');
		}catch(ex){
			Alloy.Globals.activityIndicator.hide();
		}
	};
	client.onerror = function(e) {
		//Ti.API.debug(e.error);
		Alloy.Globals.activityIndicator.hide();
		alert('Profile updating fails.');
	};
	client.timeout = 5000;

	client.send(updateInfo);

};

$.buttonChangePassword.addEventListener('click', function(e) {
	$.viewPassword.visible = true;
});
$.buttonPWCancel.addEventListener('click', function(e) {
	$.password.blur();
	$.confirm.blur();
	$.viewPassword.visible = false;
});

$.buttonSave.addEventListener('click', savePassword);

/*
 * change user's password.
 * send new password to server.
 */
function savePassword() {

	if ($.password.value != $.confirm.value) {
		alert('Passwords do not match.');
		$.confirm.focus();
		return;
	}

	var searchResult = $.password.value.search(' ');
	if (searchResult !== -1) {
		alert('Password cannot contain spaces.');
		return;
	}

	if ((0 < $.password.value.length) && ($.confirm.value.length < 6)) {
		alert('Password must be at least 7 characters.');
		$.password.focus();
		return;
	}

	var dialog = Ti.UI.createAlertDialog({
		buttonNames : ['Yes', 'No'],
		message : 'Would you like to save your password?',
		title : 'CHANGE PASSWORD'
	});
	dialog.addEventListener('click', function(e) {
		if (e.index === 0) {
			$.password.blur();
			$.confirm.blur();
			Alloy.Globals.activityIndicator.show("Processing...");
			$.viewPassword.visible = false;

			//do change password code
			updateUser(JSON.stringify({
				"user" : {
					"password" : $.password.value
				}
			}));

			Alloy.Globals.activityIndicator.hide();

		} else {
			$.password.blur();
			$.confirm.blur();
			$.viewPassword.visible = false;
		}

	});
	dialog.show();

}

/** hide the software keyboard by blurring the login fields */
function hideKeyboard() {
	$.fName.blur();
	$.lName.blur();
	$.email.blur();
	$.password.blur();
	$.confirm.blur();
}

/** this method is called by the window manager when the view becomes visible */
$.opened = function(){
	try{
	//setup the keyboard toolbar & text field indexing
	if(OS_IOS){
		$.fName.keyboardToolbar = $.lName.keyboardToolbar = $.email.keyboardToolbar = $.password.keyboardToolbarColor = $.confirm.keyboardToolbar = $.keyboardToolbar;
		$.cancel.addEventListener("click",hideKeyboard);
	}	
	}catch(ex){}
		//add focus to the applicable text field based on the key board next button and the field current focused
		$.fName.addEventListener("return", focusLastNameField);
		$.lName.addEventListener("return", focusBirthdatePicker);
		$.email.addEventListener("return", focusPasswordField);
		$.password.addEventListener("return", focusPasswordConfirmationField);
	
};

/** focus the last name field */
function focusLastNameField(){ $.lName.focus(); }


/** focus the password field */
function focusBirthdatePicker(){
	hideKeyboard();
	require("alloy/animation").popIn($.viewPicker);
}  

/** focus the password field */
function focusPasswordField(){ $.password.focus(); }

/** focus the password confirmation field */
function focusPasswordConfirmationField(){ $.confirm.focus(); }


/** de-constructor */
$.closed = function(){
	
	try{
	//remove the keyboard toolbar
	if(OS_IOS){
		$.cancel.removeEventListener("click",hideKeyboard);
	}	
	}catch(ex){}
		//add focus to the applicable text field based on the key board next button and the field current focused
		$.fName.removeEventListener("return", focusLastNameField);
		$.lName.removeEventListener("return", focusBirthdatePicker);
		$.email.removeEventListener("return", focusPasswordField);
		$.password.removeEventListener("return", focusPasswordConfirmationField);
	
};
