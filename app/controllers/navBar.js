/**
 * A custom navigation bar to be used with the window manager
 * 
 */

//make global reference for the height of the nav bar
Alloy.Globals.navigationBarHeight = Alloy.Globals.navigationBarHeight || $.getView().height;

/** set the left nav bar button 
 * @param {Object} button the button to be set
 * */
$.setLeftNavButton = function(button){
	if($.leftNav){
		$.leftNavButton.remove($.leftNav);
	}
	$.leftNavButton.add(button);
	$.leftNav = button;
};

/** set the right nav bar button 
 * @param {Object} button the button to be set
 * */
$.setRightNavButton = function(button){
	if($.rightNav){
		$.rightNavButton.remove($.rightNav);
	}
	
	$.rightNavButton.add(button);
	$.rightNav = button;
};

/** sets the title image of the nav bar
 * @param {string} image the path to the title image
 */
$.setTitleImage = function(image){
	$.titleImage.applyProperties({
			visible : true,
			image : image
		});
	$.titleLabel.applyProperties({
			visible : false
		});
};

/**sets the title text of the nav bar 
 * @param {string} title the new title of the nav bar
 * */
$.setTitle = function(title){
    $.titleLabel.applyProperties({
			visible : true,
			text : title || ""
		});
    $.titleImage.applyProperties({
			visible : false
		});
};

$.setSearchIcon = function(searchIcon) {
	if (searchIcon === true) {
		$.searchIcon.applyProperties({
			visible : true
		});
	} else {
		$.searchIcon.applyProperties({
			visible : false
		});
	}

}; 

/**sets the title text of the nav bar 
 * @param {string} title the new title of the nav bar
 * */

$.setSearch = function(search) {
	if (search === true) {
		$.titleImage.visible = false;
		$.search.applyProperties({
			visible : true,
			//text : title || ""
		});
	} else {
		$.search.applyProperties({
			visible : false
		});
	}

}; 


/** show/hide the navgation back button
 * @param {boolean} on true: show the back button; false: don't show the back button (if this is an android device, this parameter is ignored)
 * @param {boolean} right true: the back button will be added to the right; false: the back button will be added to the left (default)
 * */
$.toggleBackButton = function(on,right){
	if(on && !OS_ANDROID){
		$.backButton.applyProperties({
			visible : true
		});
		
		if(right){
			$.rightNavButton.add($.backButton);
		}else{
			$.leftNavButton.add($.backButton);
		}
	} else {
		$.backButton.applyProperties({
			visible : false
		});
		if(right){
			$.rightNavButton.remove($.backButton);
		}else{
			$.leftNavButton.remove($.backButton);
		}
	}
};

$.setHistory = function(on){
	if (on === true) {
		$.buttonHistory.applyProperties({
			visible : true
		});
	} else {
		$.buttonHistory.applyProperties({
			visible : false
		});
	}
};

$.buttonHistory.addEventListener('click', function(e){
	Alloy.Globals.mainWindow.openWindow({
		name : 'charity_history',
		animate : true
	});
});

//EVENT REGISTRATION
$.backButton.addEventListener('click', function(){
	if($.parentController && $.parentController.isWindow){
		
		$.parentController.getView().close($.parentController.closeAnimation);
	}else{
		//$.parentController.getView().close(); // <-- if we don't use windowManger
		Alloy.Globals.mainWindow.closeWindow({animate : true, force:true});
	}
});

$.searchIcon.addEventListener('click', function(e){
	Alloy.Globals.mainWindow.openWindow({
		name : 'search'
	});
});

$.navBar.addEventListener('click', function(e){
	Ti.API.info('navbar clicked: ' + JSON.stringify(e.source));
});

/** get the height of the sponsor bar
 * @return {number} the height of sponsor bar expressed in dp or px depending on the device
 */
$.getHeight = function(){

	return (OS_ANDROID ? parseInt(require("alloy/measurement").dpToPX(parseInt($.navBar.getHeight()))) : parseInt($.navBar.getHeight()));
};
