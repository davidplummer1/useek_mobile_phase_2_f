$.on = false;
var currentUser = Alloy.Models.instance("users");
/*
 $.leftMenu.addEventListener('click', function(e) {
 $.parent.openAndCloseAllWindows({
 name : e.source.id,
 data : {
 parent : $.parent
 }
 })

 $.parent.toggleLeftMenu();
 })
 */
$.menuOptions = [];
$.selectedMenuOptionIndex = 0;

$.setMode = function(mode) {
	if (mode === 'guest') {
		var menuOpts = [{
			label : 'Register',
			action : 'registration',
			type : 'page',
			method : 'openOnly',
			animate : true
		}, {
			label : 'Login',
			action : close,
			type : 'function'
		}];
	} else if (mode === 'user') {
		var menuOpts = [{
			label : 'Home',
			action : 'main',
			type : 'page'
		}, {
			label : 'Redeem',
			action : openWallet,
			type : 'function'
		}, {
			label : 'Give Back',
			action : 'giveBack',
			type : 'page'
		}, {
			label : 'Account',
			action : 'profile',
			type : 'page'
		}, {
			label : 'Prizes',
			action : 'prizes',
			type : 'page'
		}, /*{
		 label : 'How to Play',
		 action : 'how_to_play',
		 type : 'page'
		 },*/
		{
			label : 'Terms & Conditions',
			action : 'terms',
			data : {
				title : "Terms & Conditions",
				type : "tos"
			},
			type : 'page'
		}, {
			label : 'Privacy Policy',
			action : 'terms',
			data : {
				title : "Privacy Policy",
				type : "privacy"
			},
			type : 'page'
		}, {
			label : 'About Us',
			action : 'terms',
			data : {
				title : "About Us",
				type : "about"
			},
			type : 'page'
		}, {
			label : 'Contact Us',
			action : 'terms',
			data : {
				title : "Contact Us",
				type : "contact"
			},
			type : 'page'
		}, {
			label : 'Sign Out',
			action : logoutUser,
			type : 'function'
		}];

	}

	$.scrollView.removeAllChildren();
	_.each(menuOpts, function(option) {
		$.menuOptions.push(Alloy.createController('menuOption', option).getView());
		$.scrollView.add($.menuOptions[$.menuOptions.length - 1]);
	});
};
function close() {
	Alloy.Globals.mainWindow.openAndCloseAllWindows({
		animate : true,
		name : 'login'
	});
};

function openWallet(e) {
	if (Alloy.Models.instance("users").get("anonymous")) {
		require("tools").showAlertDialog({
			message : "Would you like to register in order to redeem your rewards?",
			buttonNames : ["Yes", "No"],
			cancel : 1
		}, function() {
			Alloy.Globals.mainWindow.openWindow({
				name : "registration",
				animate : true,
				data : {
					guestRegistration : true
				}
			});
		});
	} else {
		Alloy.Globals.mainWindow.openWindow({
			name : "wallet",
			animate : true
		});
	}
}

function logoutUser(e) {

	var alert = Ti.UI.createAlertDialog({
		message : 'Are you sure you want to logout?',
		buttonNames : ['Yes', 'No']
	});

	alert.addEventListener('click', function(e) {
		if (e.index === 0) {
			//logout fb as well.
			var facebook = require("facebook");
			facebook.addEventListener("logout", function(e) {
			});
			facebook.logout();

			currentUser.logout();
			Alloy.Globals.mainWindow.openAndCloseAllWindows({
				name : 'login',
				animate : true
			});

		}
	});

	alert.show();

}

$.changeMenuOption = function(menuLabel) {

	//deselect all menu options except for the one selected
	$.deselectOtherMenuOptions(menuLabel);

	//select this menu option
	for (var x in $.menuOptions) {
		if ($.menuOptions[x].menuOptionData.label === menuLabel) {
			$.selectedMenuOptionIndex = x;
			$.menuOptions[x].applyProperties({
				backgroundColor : Alloy.Globals.colors.selectedMenuOptionBG
			});
			//$.menuOptions[x].menuOptionLabel.setColor(Alloy.Globals.colors.selectedMenuOptionText);
		}
	}
};
/**
 * changes the styling of all other menu options to to be unselected
 * @param {Object} selectedMenuOptionLabel the label of the menu option selected
 */
$.deselectOtherMenuOptions = function(selectedMenuOptionLabel) {

	//loop through every available menu option
	for (var x in $.menuOptions) {

		//if the menu label doesn't exist, deslect all menu options
		if (!selectedMenuOptionLabel) {
			$.menuOptions[x].applyProperties({
				backgroundColor : Alloy.Globals.colors.unselectedMenuOptionBG
			});
			//$.menuOptions[x].menuOptionLabel.setColor(Alloy.Globals.colors.unselectedMenuOptionText);
		} else if ($.menuOptions[x].menuOptionData.label !== selectedMenuOptionLabel) {

			//deselect this option in the loop if a menu option label was specified for this function call
			$.menuOptions[x].applyProperties({
				backgroundColor : Alloy.Globals.colors.unselectedMenuOptionBG
			});
			//$.menuOptions[x].menuOptionLabel.setColor(Alloy.Globals.colors.unselectedMenuOptionText);
		}
	}
};

/** returns the label text for the currently selected menu option
 * @returns {String} the label text for the currently selected menu option; "" is returned if no menu option is selected
 */
$.getCurrentMenuOptionLabel = function() {
	return $.selectedMenuOptionIndex == 0 ? "" : $.menuOptions[$.selectedMenuOptionIndex].menuOptionData.label;
};

