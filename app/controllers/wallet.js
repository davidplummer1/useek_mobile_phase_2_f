/**
 * Displays a summary of the user's accumulated benefits.
 * 
 * 
 */

var args = arguments[0] || {};
$.navBar.setTitle('Wallet');
$.useAndroidBack = true;
$.navBar.parentController = $;
$.navBar.setSearchIcon(true);
//it will not allow click event, when user is on wallet page.
$.leaderboard.allowOpenWindow = false;

//try below code tomorrow.
$.leaderboard.viewUnredeemed.addEventListener('click', function(e){
	$.scrollable.scrollToView(0);
	$.moveHighlighter(0);
	
});
$.leaderboard.viewRedeemed.addEventListener('click', function(e){
	$.scrollable.scrollToView(1);
	$.moveHighlighter(1);
});
$.leaderboard.viewPoints.addEventListener('click', function(e){
	$.scrollable.scrollToView(2);
	$.moveHighlighter(2);
});
$.leaderboard.viewPrizes.addEventListener('click', function(e){
	$.scrollable.scrollToView(3);
	$.moveHighlighter(3);
});


$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());

var user = Alloy.Models.instance('users');
//Ti.API.info(JSON.stringify(user));
var data = [];

//Create each views and put it into scrollableview
var wallet_active = Alloy.createController('components/wallet_active').getView();
var wallet_redeemed = Alloy.createController('components/wallet_redeemed').getView();
var wallet_points = Alloy.createController('components/wallet_points').getView();
var wallet_giveback = Alloy.createController('components/wallet_giveback');
$.scrollable.views = [wallet_active, wallet_redeemed, wallet_points, wallet_giveback.getView()];

if (args === "unredeemed") {
	$.scrollable.currentPage = 0;
	
} else if (args === "redeemed") {
	$.scrollable.currentPage = 1;
	
} else if (args === "points") {
	$.scrollable.currentPage = 2;
	
} else if (args === "giveback") {
	$.scrollable.currentPage = 3;
	wallet_giveback.displayCharities();
}

var animate = Ti.UI.createAnimation({
	duration : 300
});

//when user click wallet page, leaderboard mover view height is going to be 5dp. So user can see mover.
$.leaderboard.viewMover.height = '5dp';

$.scrollable.addEventListener('scrollend', function(e) {
	$.moveHighlighter(e.currentPage);
});


/** 
 * move the leaderboard highlighter right->left or left->rights
 * @param {Number} index 	the index of the scrollable view's current page
 * */
$.moveHighlighter = function(index){
	
	animate.left = ((index||0) * 25) + "%";
	$.leaderboard.viewMover.animate(animate);
};

//move the leaderboard highlighter when the controller first loads
$.leaderboard.viewMover.applyProperties({
	left : ($.scrollable.currentPage * 25) + '%'
});

/** called when this window (view) is opened by the window manager */
$.opened = function(){

	$.leaderboard.refreshLeaderboard(true);
};
