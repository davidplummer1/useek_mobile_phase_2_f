/**
 * @fileOverview	Register user's informaiton
 * @name			registration.js
 */
//get from scanned info
var args = arguments[0];
var moment = require('moment');
//var push = require("basicPush");
$.useAndroidBack = true;

//Support mainWindow
//Alloy.Globals.mainWindow = Alloy.Globals.mainWindow || Alloy.createController('mainWindow');

//include back button on title bar
if (OS_IOS) {
	$.navBar.toggleBackButton(true);
} else {
	//$.useAndroidBack = true;
}

$.navBar.parentController = $;
$.navBar.setTitle("Register");


// DOB is date of bith value container.
var DOB = "";

/**
 * Done button set change birth value in label and color
 */
$.buttonDone.addEventListener('click', function(e) {
	if (DOB == "") {
		DOB = new Date();
		DOB = moment(DOB).format('L');
	}
	$.birth.text = DOB;
	$.birth.color = 'black';
	require("alloy/animation").fadeOut($.viewPicker,200,function(){ $.email.focus(); });
});

/*
 * this function will save date value in DOB, when user change picker.
 */
$.picker.addEventListener('change', function(e) {
	DOB = moment(e.value).format('L');
});

$.picker.minDate = new Date(1900, 0, 1);

//Create user's account
$.buttonCreate.addEventListener('click', register);

/** attempt to register an account
 * @param {object} e the click event from the registration button
 * */
function register(e) {

	//check if you're online
	if (!require("onlineChecker").isOnline()) {
		return;
	}

	hideKeyboard();

	//ensure the required fields aren't blank
	if (($.fName.value !== '') && ($.lName.value !== '') && ($.email.value !== '') && ($.email.value.search(' ') === -1)) {
	} else {
		alert('Please complete all fields.');
		return;
	}

	//ensure the password  password confirmation both match
	if ($.password.value != $.confirm.value) {
		alert('Passwords do not match.');
		$.confirm.focus();
		return;
	}

	//ensure the password field doesn't contain spaces
	var searchResult = $.password.value.search(' ');
	if (searchResult !== -1) {
		alert('Password cannot contain spaces.');
		return;
	}

	//if the
	if ((0 < $.password.value.length) && ($.confirm.value.length < 6)) {
		alert('Password must be at least 7 characters.');
		$.password.focus();
		return;
	}

	//validate the email address inputted
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (reg.test($.email.value) === false) {
		alert('Please Enter a valid email address.');
		return;
	}

	dialog.show();

};//end create button event

$.confirm.addEventListener('return', register);

/** hide the software keyboard
 */
function hideKeyboard() {
	$.fName.blur();
	$.lName.blur();
	$.email.blur();
	$.password.blur();
	$.confirm.blur();

}

//create a confirmation dialog for the user
var dialog = Ti.UI.createAlertDialog({
	buttonNames : ['Yes', 'No'],
	message : 'Would you like to create an account?',
	title : 'Create Account'
});

//event listener to handle registering the user
dialog.addEventListener('click', function(e) {
	if (e.index === 0) {

		Alloy.Globals.activityIndicator.show("Processing...");

		var client = Ti.Network.createHTTPClient();
		if (args.guestRegistration) {
			client.open("PUT", Alloy.CFG.useekWebService+"/users/" + Alloy.Models.instance("users").id);
		} else {
			client.open("POST", Alloy.CFG.useekWebService+"/users/");
		}
		client.setRequestHeader('Content-Type', "application/json");
		client.setRequestHeader("Authorization", 'Token token="' + Ti.App.Properties.getString("sessionToken") + '"');
		client.onload = function(e) {

			try {
				var response = JSON.parse(client.responseText);

				//if there are any errors returned, stop
				if (response.messages.error.length > 0) {
					Alloy.Globals.activityIndicator.hide();
					alert(response.messages.error[0]);
					return;
				}

				//parse the user object and open the main window
				var user = Alloy.Models.instance("users");
				user.clear();
				user.set(response.user);
				//TODO add logic to pull to sync that games & rewards progress in the user model & the leaderboard tab

				Ti.App.Properties.setString("userId", user.get("id"));
				Ti.App.Properties.setString("sessionToken", user.get("token") || "");

				user.toggleSessionRefresher(true);

			} catch(ex) {
				Alloy.Globals.activityIndicator.hide();
				alert("An error occurred while registering your account.");
				return;
			}
			Alloy.Globals.activityIndicator.hide();

			//if this is a guest user registration attempt, close the registration window
			if (args.guestRegistration) {
				Alloy.Globals.mainWindow.closeWindow({
					animate : true,
					force : true
				});
			} else {
				//open main window
				Alloy.Globals.mainWindow.openWindow({
					name : 'main',
					animate : true
				});
			}
		};

		/**
		 *
		 * @param {Object} e
		 */
		client.onerror = function(e) {
			Ti.API.debug(e.error);

			try {
				//display an error if it can be found
				var response = JSON.parse(client.responseText);
				if (response.messages.error.length > 0) {
					alert(response.messages.error[0]);
				}
			} catch(ex) {
			}
			Alloy.Globals.activityIndicator.hide();
		};
		client.timeout = 5000;

		//if this is a guest registration, exclude the device_token from the request payload
		if (args.guestRegistration) {
			client.send(JSON.stringify({
				"user" : {
					"email" : $.email.value,
					"first_name" : $.fName.value,
					"last_name" : $.lName.value,
					"birth_date" : $.birth.text,
					"password" : $.password.value
				}
			}));
		} else {
			client.send(JSON.stringify({
				"user" : {
					"email" : $.email.value,
					"first_name" : $.fName.value,
					"last_name" : $.lName.value,
					"birth_date" : $.birth.text,
					"password" : $.password.value
				},
				"device_token" : Alloy.CFG.deviceToken
			}));
		}

	} else {

	}

});

/** hide the software keyboard by blurring the login fields */
function hideKeyboard() {
	$.fName.blur();
	$.lName.blur();
	$.email.blur();
	$.password.blur();
	$.confirm.blur();
}

/** this method is called by the window manager when the view becomes visible */
$.opened = function(){
	
	//setup the keyboard toolbar & text field indexing
	if(OS_IOS){
		$.fName.keyboardToolbar = $.lName.keyboardToolbar = $.email.keyboardToolbar = $.password.keyboardToolbarColor = $.confirm.keyboardToolbar = $.keyboardToolbar;
		$.cancel.addEventListener("click",hideKeyboard);
	}	
		//add focus to the applicable text field based on the key board next button and the field current focused
		$.fName.addEventListener("return", focusLastNameField);
		$.lName.addEventListener("return", focusBirthdatePicker);
		$.email.addEventListener("return", focusPasswordField);
		$.password.addEventListener("return", focusPasswordConfirmationField);
	
};

/** focus the last name field */
function focusLastNameField(){ $.lName.focus(); }


/** focus the password field */
function focusBirthdatePicker(){
	hideKeyboard();
	
	require("alloy/animation").popIn($.viewPicker);

}  
/** focus the password field */
function focusPasswordField(){ $.password.focus(); }

/** focus the password confirmation field */
function focusPasswordConfirmationField(){ $.confirm.focus(); }

/** de-constructor */
$.closed = function(){
	
	//remove the keyboard toolbar
	if(OS_IOS){
		$.cancel.removeEventListener("click",hideKeyboard);
	}	
		//add focus to the applicable text field based on the key board next button and the field current focused
		$.fName.removeEventListener("return", focusLastNameField);
		$.lName.removeEventListener("return", focusBirthdatePicker);
		$.email.removeEventListener("return", focusPasswordField);
		$.password.removeEventListener("return", focusPasswordConfirmationField);
	
};

//picker will show up on screen.
$.birth.addEventListener('click', focusBirthdatePicker);

//picker will show up on screen.
$.birth.addEventListener('focus', focusBirthdatePicker);
