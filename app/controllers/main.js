var args = arguments[0] || {};

/* For some reason, out of the blue, the app has begun to crash when the scrollable view is
initially displayed empty on android devices. To rectify this, a blank view is initially loaded and then removed
when the useek data populates the scrollable view.
*/
if(OS_ANDROID){
	$.scrollable.addView(Ti.UI.createView({ height : "40dp", width : "40dp", backgroundColor : "transparent"}));
}
$.navBar.setTitleImage('/images/logo.png');
$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());
$.navBar.setSearchIcon(true);
var async = require("async");

var user = Alloy.Models.instance('users');
//Ti.API.info("users: " + JSON.stringify(user));


var featured_games = Alloy.Collections.instance('featured_games');
//Ti.API.info("featured_games: " + JSON.stringify(featured_games));


//it will contain all coverflow images
var images = [];

//levels container for passsing value to video
var levelsArr = [];

var imgViews = [];

var name = [];

//if screen size is 3.5inch.
if(OS_IOS && Alloy.Globals.pW == 320 && Alloy.Globals.pH == 480){
	$.viewMain.height = Alloy.Globals.platformHeight50Percent;
}




/** fetch the featured games
 * @param {function} asyncCallback the callback method for the async module * */
function getFeaturedGames(asyncCallback) {
	
	//if the user isn't connection to the internet, stop
	if(!Alloy.Globals.isOnline()){
		callback && callback(true,{ message : "Please check your internet connection and try again."});
		return;
	}
	//if the activity indicator isn't shown, display it
	if(!Alloy.Globals.activityIndicator.shown){
		Alloy.Globals.activityIndicator.show("Loading...");
	}
	
	featured_games.fetch({
		success : function(e) {
			try {

				featured_games.forEach(function(item){

					//need to pagination function for memory issue. ex) if index is 20 and event : scrollend? retrive more
					var view = Alloy.createController('components/imageView', {
						image : require("tools").isTablet() ? item.get("image_urls").large : item.get("image_urls").medium,
						name : item.get("name"),
						id : item.id
					}).getView();

					imgViews.push(view);
				
				});
				$.scrollable.views = imgViews;

				/*
				 * Passing seleted index and data to levels page.
				 * !!!! Bug : It fires when components/imageView controller image event is exist.
				 */
				if(OS_IOS){
				$.scrollable.addEventListener('click', function(e) {
					//Ti.API.info("clicked")
					Alloy.Globals.activityIndicator.show("Loading...");
					Alloy.Globals.mainWindow.openWindow({
						name : 'levels',
						animate : true,
						data : {
							id : featured_games.models[$.scrollable.currentPage].id,
							collectionName : "featured_games"
							
						}
					});

				});
				}
				
				asyncCallback && asyncCallback(null);

			} catch(ex) {
				asyncCallback && asyncCallback(true, ex);
			}

		},
		error : function(error) {
			asyncCallback && asyncCallback(true, error);
		}
	});

}

/** show the activity indicator when the window is opened */
$.opened = function(){
	if(!Alloy.Globals.activityIndicator.shown){
		Alloy.Globals.activityIndicator.show("Loading...");
	}
	getFeaturedGames($.asyncCallback);
	$.pullTab.toggleEventListeners(true);
	$.leaderboard.refreshLeaderboard();

};

$.focused = function(){
	Ti.API.info("focused");
	$.leaderboard.refreshLeaderboard();
	$.pullTab.toggleEventListeners(true);

};


$.asyncCallback = function(error, details) {
	
	//if the sequential content fetch  failed, stop fetching content and display an error
	if (error) {
		alert(details.message || "An unexpected error occurred. Please try again.");
		
	}
	//show the left menu if it's still hidden
	Alloy.Globals.mainWindow.revealLeftMenu();
	Alloy.Globals.activityIndicator.hide();
	//warn the user about network connectivity
	Alloy.Globals.warnTheUser();
};
	
/** garbage collection */
$.closed = function(){
	Ti.API.info("closed");
	$.pullTab.toggleEventListeners(false);
};
$.blurred = function(){
	Ti.API.info("blurred");
	$.pullTab.toggleEventListeners(false);
};
