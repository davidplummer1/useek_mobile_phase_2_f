var args = arguments[0] || {};

//Ti.API.info(JSON.stringify(args));

$.labelNum.text = args.count;
$.labelTitle.text = args.level.name;
$.imgPic.image = args.level_img;
$.levelLocked = false;

var levelCollection = Alloy.Collections.instance("level");

//check if this level is locked or not
try {
	$.levelLocked = (args.level && args.level.locked_by_levels && args.level.locked_by_levels.length > 0) ? true : false;
	if(!$.levelLocked && args.level.unlock_points){
		$.levelLocked = args.level.unlock_points > 0 ? true : false;
	}
} catch(ex) {
	Ti.API.debug("each_level.js");
	Ti.API.error(ex);
}

//make same digit number and change to string to get length for converting.
var earnable_points = require('tools').roundDecimal(args.level.video.earnable_points);
var earnable_rewards = require('tools').roundDecimal(args.level.video.earnable_rewards);

//if number is over 1000, it will convert K
$.labelPoints.text = earnable_points >= 1000 ? (earnable_points / 1000).toFixed(1) + "K" : earnable_points;
$.labelRewards.text = earnable_rewards >= 1000 ? (earnable_rewards / 1000).toFixed(1) + "K" : "$" + earnable_rewards;

if (args.level["completed?"]) {
	$.imgCheck.text = '2';
}

//display sponsors
for (var i in args.level.video.sponsors) {

	var each_sponsors = Alloy.createController('components/each_level_sponsors', {
		image : args.level.video.sponsors[i].logo_url
		//parent : $
	}).getView();

	$.viewCollapse.add(each_sponsors);

}

//on off swich for collapsibility.
var visible = false;

//expend or collapse view.
$.container.addEventListener('click', function(e) {
	var lockedByLevels = [];
	try{
		lockedByLevels = args.level.locked_by_levels && args.level.locked_by_levels.length > 0 ? args.level.locked_by_levels : [];
	}catch(ex){}
	
	if ($.levelLocked) {
		debugger;
		/* If the level can be unlocked by cashing in points and the user has enough,
		 give them the option to do so
		 */
		
		var message = "To unlock this level, you must:\n";
		for(var x in lockedByLevels){
			var lockedByLevel = levelCollection.where({ id : lockedByLevels[x] });
			if(lockedByLevel.length > 0){
				message += "Watch " + lockedByLevel[0].get("name") + "\n";
			}
		}
		message += "Pay " + args.level.unlock_points + " points.";
		
		
		if (args.level.unlock_points > 0 && Alloy.Models.instance("users").get("points") > args.level.unlock_points) {
			require("alertDialog").show({
				title : "This level is currently locked.",
				message : message,
				buttonNames : ["Spend " + args.level.unlock_points + " points", "Cancel"],
				cancel : 1
			}, function(ok) {
				unlockLevel();
			});
		} else if (args.level.unlock_points > 0 && Alloy.Models.instance("users").get("points") < args.level.unlock_points) {
			alert("Sorry, but it takes " + args.level.unlock_points + " point" + (args.level.unlock_points == 1 ? "" : "s") + " to unlock this level.");

		}else if(args.level.locked_by_levels.length > 0){
			alert("Sorry, but you must play "+args.levels.locked_by_levels+" in order to unlock this level.");
		}
		return;
	}

	//if the user clicked the play button, load the level details window
	if (e.source.id == "playButton") {
		Alloy.createController('hidden_win', {
			level : args.level,
			parent : args.parent,
			id : args.id,
			image : args.level_img,
			earnable_points : args.level.video.earnable_points,
			earnable_rewards : args.level.video.earnable_rewards
		}).getView().open();
	} else {
		if (visible === false) {
			$.viewCollapse.height = Ti.UI.SIZE;

			if (Alloy.Globals.pW >= 768 && Alloy.Globals.pH >= 1024) {
				$.viewLine.height = '200dp';
			} else {
				$.viewLine.height = '110dp';
			}
			$.getView().applyProperties({
				backgroundColor : Alloy.Globals.colors.lightGray
			});
			for (var i in $.viewCollapse.children ) {
				$.viewCollapse.children[i].height = Ti.UI.SIZE;
			}
			$.seperator.visible = false;
			$.seperatorBottom.visible = true;
			visible = true;
		} else {
			$.viewCollapse.height = 0;
			$.viewLine.height = 0;
			for (var i in $.viewCollapse.children ) {
				$.viewCollapse.children[i].height = 0;
			}
			$.seperator.visible = true;
			$.seperatorBottom.visible = false;
			$.getView().applyProperties({
				backgroundColor : 'white'
			});
			visible = false;
		}
	}

});

/** refresh the lock icon 
 * if the level is still locked, show a pad lock icon; else , show the play button icon
 * */
function refreshLock(){
	//if this level is locked, change the arrow icon into a padlock
	if ($.levelLocked) {
	
		$.playButton.applyProperties({
			text : "\uf023",
			color : "#f00",
			right : "5dp",
			font : {
				fontSize : '40dp',
				fontFamily : "FontAwesome"
			}
		});
	}else{
		$.playButton.applyProperties({
			text : "5",
			color : Alloy.Globals.colors.themeColor,
			right : 0,
			font : {
				fontSize : '40dp',
				fontFamily : "USEEK"
			}
		});
	}
}


refreshLock();


/** attempt to unlock the current level in the useek API */
function unlockLevel(){
	
	$.levelModel = $.levelModel || Alloy.createModel("level");
	$.levelModel.unlock({
		gameId : args.level.game_id,
		levelId : args.level.id,
		success : function(e){
			alert("unlocked");
			
		},
		error : function(model,raw){
			alert(raw);
			Ti.API.info(raw);
		}
	});
}


