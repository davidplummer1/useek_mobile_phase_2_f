var args = arguments[0] || {};

$.navBar.setTitle(args.name);
$.navBar.toggleBackButton(true);
$.useAndroidBack = true;

try {
	//if the protocol has been excluded, add the http protocol by default
	if ( typeof (args.url) == "string" && args.url.indexOf("www") == 0) {
		args.url = "http://" + args.url;
	}
} catch(ex) {
}
$.web.url = args.url; 