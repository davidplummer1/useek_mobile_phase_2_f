/*
 * display game images for android main view.
 */

var args = arguments[0] || {};
//Ti.API.info("imageView page: " + JSON.stringify(args));

$.image.image = args.image;
$.labelTitle.text = args.name;
/*
 * If we didn't add image event listener, it will not fire with main scrollable view
 * It looks like a bug.
 */
$.image.addEventListener('click', function(e) {
	
	if (OS_ANDROID) {
		Alloy.Globals.mainWindow.openWindow({
			name : 'levels',
			animate : true,
			data : {
				
				id : args.id,
				collectionName : "featured_games"
			}
		});
		
	}
});

/*
 $.parent = args.parent || {};
 //set the event listener(s) if they've been passed'
 for(var x in args.events){
 $.image.addEventListener(x, args.events[x]);
 }
 */