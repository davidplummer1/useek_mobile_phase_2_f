var args = arguments[0] || {};

$.imageGame.image = args.thumbnail_url;
$.labelTitle.text = args.title;

//need to change image
if (args.levels["completed?"] === true) {
	$.imageLevelMark.image = '/images/logo.png';
} else {
	$.imageLevelMark.image = '/images/sample.png';
}

//passing video id and level data to video.js
$.container.addEventListener('click', function(e) {
	Alloy.Globals.mainWindow.openWindow({
		name : 'video',
		animate : true,
		data : {
			id : args.levels.video.id,
			level : args.levels
		}
	});
});
