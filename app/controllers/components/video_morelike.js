var args = arguments[0] || {};

//Ti.API.info("more like: " + JSON.stringify(args));

$.labelTitle.text = args.name;
$.imgPic.image = args.thumbnail_url;

$.container.addEventListener('click', function(e) {

	Ti.App.fireEvent("refreshLevelSummary",{
		id : args.id,
		name : args.name,
		thumbnail_url : args.thumbnail_url,
		level : args.levels,
		earnable_points : args.earnable_points,
		earnable_rewards : args.earnable_rewards,
		sponsors : args.levels.video.sponsors,   
		type : 'morelike'
	});

});
