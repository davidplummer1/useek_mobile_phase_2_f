/**
 *
 * @fileOverview	creates a button with style properties and event handlers specified by the user
 * @name			dynamicButton.js
 */


//gets the properties for this button
var properties = arguments[0] && arguments[0].properties || {};
/** gets an array of event listeners if they exist 
 * 
 * events = {
 * 			{string}eventType : {function}eventHandler
 * 		}
 * 
 * */
var events = arguments[0] && arguments[0].events || {};

// gets the parent controller if it's received
$.parent = arguments[0] && arguments[0].parent || {};


// register the provided event listeners for this button
for(var x in events){
	$.container.children[0].addEventListener(x, events[x]);
}

// set the button's properties
$.container.children[0].applyProperties(properties);


/*.
$.navBar.setRightNavButton(Alloy.createController('components/dynamicButton', 
{
	properties :{
		//title:'Save',
		width:'30dp',
		height:'30dp',
		backgroundImage:'/images/save.png'
	},
	events :{
		"click": $.saveAlertSettings
	}
}).getView() ,trackFieldChange );
 */