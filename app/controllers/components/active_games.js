/*
 * This page called from main bottom scrollview which is active_games.
 * And it will display user's active games thumbnail views.
 *
 */
var args = arguments[0] || {};


var gameDetails = Alloy.Collections.instance(args.type).where({ id : args.gameId })[0].toJSON();
var levelDetails = {};

//if the level id has been passed, retrieve the level's details from the game
if(args.levelId){
	var levels = gameDetails.levels;
	for(var x in levels){
		if(args.levelId == levels[x].id){
			levelDetails = levels[x];
			break;
		}
	}
}


$.imageActive.image = args.levelId ? levelDetails.video.image_urls.medium : gameDetails.image_urls.medium;
$.labelTitle.text = args.levelId ? levelDetails.name : gameDetails.name;
/*
var earnable_points = require('tools').roundDecimal(args.levelId ? levelDetails.video.earnable_points : gameDetails.earnable_points);
var earnable_rewards = require('tools').roundDecimal(args.levelId ? levelDetails.video.earnable_rewards : gameDetails.earnable_rewards);
$.labelPoints.text = earnable_points >= 1000 ? (earnable_points/1000).toFixed(1) + "K" : earnable_points;
$.labelRewards.text = earnable_rewards >= 1000 ? (earnable_rewards/1000).toFixed(1) + "K" : earnable_rewards;
*/

//if user clicks all games, it takes video play page directly.
if (args.type === "games" || args.type === "new_releases" ) {
	
$.container.addEventListener('click', function(e) {
	Alloy.Globals.activityIndicator.show("");
	Alloy.createController('hidden_win', {
		id : levelDetails.id,
		level : levelDetails,
		image : levelDetails.video.image_urls.medium,
		earnable_points : levelDetails.video.earnable_points,
		earnable_rewards : levelDetails.video.earnable_rewards,
	}).getView().open(); 
});
	/*
	$.container.addEventListener('click', function(e) {
		Alloy.Globals.mainWindow.openWindow({
			name : 'video',
			animate : true,
			data : {
				id : args.levels.video.id,
				level : args.levels
			}
		});
	});
	*/
	
} else {



	//need to change image
	/*if (levelDetails["completed?"] === true) {
		$.imgMarkLevel1.text = '2';
		$.imgMarkLevel2.text = '2';
		$.imgMarkLevel3.text = '2';
	} else {
		$.imgMarkLevel1.text = '7';
		$.imgMarkLevel2.text = '7';
		$.imgMarkLevel3.text = '7';
	}*/

	$.container.addEventListener('click', function(e) {
		
		Alloy.Globals.activityIndicator.show("");
		Alloy.Globals.mainWindow.openWindow({
			name : 'levels',
			animate : true,
			data : {
				id : args.gameId,
				collectionName : args.type
			}
		});

	});
}


