var args = arguments[0] || {};

var user = Alloy.Models.instance('users');

var charities = Alloy.Collections.instance('charities');
var donations = Alloy.Collections.instance("givebacks");
Alloy.Globals.activityIndicator.show('Loading');

/** 
 * load the list of charities into the charitiesListView 
 * */
$.displayCharities = function() {
	
	Alloy.Globals.activityIndicator.show('Loading...');
	charities.fetch({
		success : function(e) {
			var data = [];
			var x = e;
	
			$.labelName.text = user.get("charity").name;
			charities.forEach(function(item){
			
			
				data.push({
					template : 'template',
					properties : {
						height : '60dp'
					},
					logo : {
						image : item.get("image_urls").medium
					},
					name : {
						text : item.get("name")
					},
					buttonSelect : {
						visible : isCharityUnselected(item.id)
					},
					id : item.id

				});
			
		});
			$.section.setItems(data);
			
			

			Alloy.Globals.activityIndicator.hide();

		},
		error : function(error) {
			Alloy.Globals.activityIndicator.hide();
		}
	});
};


/*
 * take charity detail page with data
 */
$.charityListView.addEventListener('itemclick', function(e) {

	var item = $.section.getItemAt(e.itemIndex);

	Alloy.Globals.mainWindow.openWindow({
		name : 'charity_detail',
		animate : true,
		data : { 
			charityId : item.id
		}
	});
});

/*
 * select user's charity
 */
function selection(e) {
	var item = $.section.getItemAt(e.itemIndex);

	var dialog = Ti.UI.createAlertDialog({
		cancel : 1,
		buttonNames : ['Yes', 'No'],
		message : 'Would you like to select as partner?',
		title : 'Charity'
	});
	dialog.addEventListener('click', function(e) {
		if (e.index === 0) {
			Alloy.Globals.activityIndicator.show("Loading");

			//trigger selectCharity event and passing charity id
			selectCharity(item.id);

			Alloy.Globals.activityIndicator.show("Loading");

		}
		if (e.index === 1) {
		}
	});
	dialog.show();

}

/*
 * Set the selected charity as the current user's GiveBack partner and returns the updated user object.
 */
function selectCharity(id) {
	var client = Ti.Network.createHTTPClient();
	client.open("POST", Alloy.CFG.useekWebService+"/charities/" + id + "/partner");
	client.setRequestHeader('Content-Type', "application/json");
	client.setRequestHeader("Authorization", 'Token token="' + Ti.App.Properties.getString("sessionToken") + '"');
	client.onload = function(e) {
		var json = JSON.parse(this.responseText);
		//Ti.API.info(JSON.stringify(json));
		//clean array and display updated data.
		user.set({ charity : charities.where({ id : id})[0].toJSON()});
		$.displayCharities();
		alert(json.user.charity.name + " selected successfully");

		Alloy.Globals.activityIndicator.hide();
	};
	client.onerror = function(e) {
		Alloy.Globals.activityIndicator.hide();
		Ti.API.debug(e.error);
		alert(e.error);
		
	};
	client.timeout = 5000;
	client.send();
}

/** event handler to toggle the charity list and the charity history list
 * @param {Object} e the click event for the button bar
 */
$.switchView = function(e) {
	Ti.API.info(e.source.id);
	$.toggleButtonBar(buttons.indexOf(e.source.id));
	$.charitySubTab.applyProperties({
		visible : (e.source.id == "viewCharities"),
		zIndex : (e.source.id == "viewCharities" ? 1 : 2)
	});
	$.historySubTab.applyProperties({
		visible : (e.source.id == "viewHistory"),
		zIndex : (e.source.id == "viewHistory" ? 1 : 2)
	});
	
	//choose which list view to populate (charity list or donation list)
	if(e.source.id == "viewHistory"){
		$.loadDonations();
	}else if(e.source.id == "viewCharities"){
		$.displayCharities();
	}
};
var buttons = ["viewCharities","viewHistory"];
var selectedButton = 0;
$.toggleButtonBar = function(index) {

	if (index==0) {
		$.viewHistory.applyProperties({
			backgroundColor : "#fff"
		});
		$.labelHistory.applyProperties({
			color : Alloy.Globals.colors.themeColor
		});

		$.viewCharities.applyProperties({
			backgroundColor : Alloy.Globals.colors.themeColor
		});
		$.labelCharities.applyProperties({
			color : "#fff"
		});
	} else {
		$.viewHistory.applyProperties({
			backgroundColor : Alloy.Globals.colors.themeColor
		});
		$.labelHistory.applyProperties({
			color : "#fff"
		});

		$.viewCharities.applyProperties({
			backgroundColor : "#fff"
		});
		$.labelCharities.applyProperties({
			color : Alloy.Globals.colors.themeColor
		});
	}

};

/**
 * @event click
 * Triggered when the user clicks on this view's button bar
 */
$.viewButtonBar.addEventListener("click", $.switchView);




/** fetch the charity history */
$.loadDonations = function() {
	donations.fetch({
		success : function(e) {

			var data = [];

			//parse the history data into list view elements
			donations.each(function(item) {
				data.push({
					template : 'historyTemplate',
					properties : {
						height : Ti.UI.SIZE
					},
					historyLogo : {
						image : item.get("charity").image_urls.medium
					},
					historyName : {
						text : item.get("charity").name
					},
					historyAmount : {
						text : "$" + require("tools").roundDecimal(item.get("amount"))
					}

				});
			});
			$.historyListSection.setItems(data);

			Alloy.Globals.activityIndicator.hide();
		},
		error : function(e, raw) {
			Alloy.Globals.activityIndicator.hide();
		}
	});
};

/** 
 * checks whether the user has selected the provided charity
 * 
 * @param {Number} charityId the id of the charity to be checked
 * @return {Boolean} true: the user hasn't selected this charity; false: the user has selected this charity
 */
function isCharityUnselected(charityId){
	
	if(user.get("charity") == null){
		return true;
	}
	return !(user.get("charity") && user.get("charity").id == charityId);
}
