/*
 * Display user's active coupons.
 */

var active_rewards = Alloy.Collections.instance('active_rewards');
//Ti.API.info("active_rewards: " + JSON.stringify(active_rewards));
var data = [];

//container for expiration number.
var expNum = 0;

active_rewards.fetch({
	success : function(success) {
		Alloy.Globals.activityIndicator.show("Loading");
		//Ti.API.info("active_rewards: " + JSON.stringify(active_rewards));
		$.labelRewards.text = "You have the following " + active_rewards.models[0].get('total_rewards') + " rewards available:";
		//active rewards collections and display on listview.
		for (var i in active_rewards.models[0].get('active_rewards')) {

			var active = active_rewards.models[0].get('active_rewards')[i];

			//if expiring is true, addition number and display on expiring soon number.
			if (active["expiring?"] == true) {
				expNum += i;

				//if there are no expiration soon data, number circle will not display.
				if (expNum == 0) {
					$.viewExpNum.visible = false;
					$.labelExpNum.text = "";
				} else {
					$.viewExpNum.visible = true;
					$.labelExpNum.text = expNum;
				}
			}

			data.push({
				template : 'template',
				properties : {
					height : '80dp'
				},
				pic : {
					image : active.offer.logo_url
				},
				desc : {
					text : active.offer.description
				},
				mile : {
					text : active.distance !== null ? (active.distance).toFixed(1) + " mi away" : ""
				},
				redeem : {
					title : 'Redeem'
				},
				expiration : {
					text : "Valid through " + active.offer.expiration
				},
				offer : active.offer,
				id : active.id,
				expMark : {
					image : active["expiring?"] == true ? '/images/circle-50.png' : null
				},
				labelExpMark : {
					text : active["expiring?"] == true ? "!" : ""
				}
			});
		}
		$.section.setItems(data);

		Alloy.Globals.activityIndicator.hide();

	},
	error : function(error) {
		Ti.API.info(error);
		Alloy.Globals.activityIndicator.hide();
	}
});

//value for loadmore function call.
var alphabetical = false;
var nearme = false;
var expiration = false;

/*
 * When user click AZ button, it will start to sort and display array.
 */
$.viewAZ.addEventListener('click', function(e) {
	$.viewAZ.backgroundColor = Alloy.Globals.colors.themeColor;
	$.labelAZ.color = Alloy.Globals.colors.white;
	$.viewNearme.backgroundColor = 'white';
	$.labelNearme.color = Alloy.Globals.colors.themeColor;
	$.viewExp.backgroundColor = 'white';
	$.labelExp.color = Alloy.Globals.colors.themeColor;
	$.viewGoogle.height = '0dp';
	$.listView.bottom = '0dp';
	Alloy.Globals.activityIndicator.show('Loading');

	alphabetical = true;
	nearme = false;
	expiration = false;
	page = 1;
	i = 20;
	//sort by name call
	getActiveAPI("alphabetical", "");
});

$.viewExp.addEventListener('click', function(e) {

	$.viewAZ.backgroundColor = 'white';
	$.labelAZ.color = Alloy.Globals.colors.themeColor;
	$.viewNearme.backgroundColor = 'white';
	$.labelNearme.color = Alloy.Globals.colors.themeColor;
	$.viewExp.backgroundColor = Alloy.Globals.colors.themeColor;
	$.labelExp.color = Alloy.Globals.colors.white;
	$.viewGoogle.height = '0dp';
	$.listView.bottom = '0dp';
	Alloy.Globals.activityIndicator.show('Loading');

	alphabetical = false;
	nearme = false;
	expiration = true;
	page = 1;
	//starbucks : 27
	i = 20;
	//sort by expiration
	getActiveAPI("expiration", "");
});

$.viewNearme.addEventListener('click', function(e) {
	$.viewAZ.backgroundColor = 'white';
	$.labelAZ.color = Alloy.Globals.colors.themeColor;
	$.viewNearme.backgroundColor = Alloy.Globals.colors.themeColor;
	$.labelNearme.color = Alloy.Globals.colors.white;
	$.viewExp.backgroundColor = 'white';
	$.labelExp.color = Alloy.Globals.colors.themeColor;
	$.viewGoogle.height = '30dp';
	$.listView.bottom = '30dp';
	Alloy.Globals.activityIndicator.show('Loading');

	alphabetical = false;
	nearme = true;
	expiration = false;
	page = 1;
	i = 20;

	getCurrentLocation();

});

var longitude;
var latitude;

/*
 * get device's current location latitude and longitude for displaying rewards near user's location.
 */
function getCurrentLocation() {
	Titanium.Geolocation.purpose = "For displaying rewards near by you using GPS";
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;

	Titanium.Geolocation.distanceFilter = 10;

	// GET CURRENT POSITION - THIS FIRES ONCE
	Titanium.Geolocation.getCurrentPosition(function(e) {
		if (e.error) {
			Alloy.Globals.activityIndicator.hide();
			alert('Sorry cannot get your current location');
			return;
		}
		longitude = e.coords.longitude;
		latitude = e.coords.latitude;
		//sort by name location
		getActiveAPI("nearme", "");
	});
}

//for pagination.
var page = 1;

/*
 * This function receives sort_by and location url and make a http call and display data on listview.
 */
function getActiveAPI(type, loadmoreValue) {

	Ti.API.info(type + ", " + loadmoreValue);
	Alloy.Globals.activityIndicator.show("Loading");
	var client = Ti.Network.createHTTPClient();

	var url;

	//if function call type is loadmore, it will make to page value + 1. It makes to call next page.
	if (loadmoreValue == "loadmore") {
		var loadmore = true;
		page = page + 1;
	} else {
		loadmore = false;
	}

	//reset listview marker for loadmore, because this page has three sorting type in one page, but marker event fires only one.
	if (type == "alphabetical") {

		url = Alloy.CFG.useekWebService + "/users/" + Ti.App.Properties.getString("userId") + "/active_rewards?sort_by=name&page=" + page;
	} else if (type == "nearme") {

		//url = Alloy.CFG.useekWebService+"/users/" + Ti.App.Properties.getString("userId") + "/active_rewards?sort_by=location=" + 48.211 /*latitude.toFixed(3)*/ + "," + -114.320 /*longitude.toFixed(3)*/ + "&page=" + page;
		url = Alloy.CFG.useekWebService + "/users/" + Ti.App.Properties.getString("userId") + "/active_rewards?location=" + latitude.toFixed(3) + "," + longitude.toFixed(3) + "&page=" + page;
		Ti.API.info(url)//test with these !! ==> 48.211,-114.320
	} else if (type == "expiration") {

		url = Alloy.CFG.useekWebService + "/users/" + Ti.App.Properties.getString("userId") + "/active_rewards?sort_by=expiration&page=" + page;
	} else {
		url = Alloy.CFG.useekWebService + "/users/" + Ti.App.Properties.getString("userId") + "/active_rewards?page=" + page;
	}

	client.open("GET", url);
	client.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml");
	client.setRequestHeader("Content-Type", "application/json");
	client.setRequestHeader("Authorization", 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"');
	client.onload = function(e) {
		//clean up and display alphabetical sorting on listview.
		data.length = 0;

		var active = JSON.parse(this.responseText);
		//Ti.API.info(JSON.stringify(active));
		for (var x in active.active_rewards) {

			//if expiring is true, addition number and display on expiring soon number.
			if (active.active_rewards[x]["expiring?"] == true) {
				expNum += x;

				//if there are no expiration soon data, number circle will not display.
				if (expNum == 0) {
					$.viewExpNum.visible = false;
					$.labelExpNum.text = "";
				} else {
					$.viewExpNum.visible = true;
					$.labelExpNum.text = expNum;
				}

			}

			data.push({
				template : 'template',
				properties : {
					height : '80dp'
				},
				pic : {
					image : active.active_rewards[x].offer.logo_url
				},
				desc : {
					text : active.active_rewards[x].offer.description
				},
				mile : {
					text : active.active_rewards[x].distance !== null ? (active.active_rewards[x].distance).toFixed(1) + " mi away" : ""
				},
				redeem : {
					title : 'Redeem'
				},
				expiration : {
					text : "Valid through " + active.active_rewards[x].offer.expiration
				},
				offer : active.active_rewards[x].offer,
				id : active.active_rewards[x].id,
				expMark : {
					image : active.active_rewards[x]["expiring?"] == true ? '/images/circle-50.png' : null
				},
				labelExpMark : {
					text : active.active_rewards[x]["expiring?"] == true ? "!" : ""
				}
			});

		}

		//if listview marker event fired, it will append items on list
		if (loadmore) {
			$.section.appendItems(data);
		} else {
			$.section.setItems(data);
		}

		Alloy.Globals.activityIndicator.hide();

		$.listView.setMarker({
			sectionIndex : 0,
			itemIndex : (i - 5)
		});

	};
	client.onerror = function(e) {
		Ti.API.debug(e.error);
		//Ti.API.info(client.status)
		Alloy.Globals.activityIndicator.hide();
	};
	client.timeout = 102000;
	client.send();
}

//load page number.
var i = 20;

// Set the initial item threshold
$.listView.setMarker({
	sectionIndex : 0,
	itemIndex : (i - 5)
});

// Load more data and set a new threshold when item threshold is reached
$.listView.addEventListener('marker', function(e) {

	i += 20;

	if (alphabetical) {
		getActiveAPI("alphabetical", "loadmore");
	} else if (nearme) {
		getActiveAPI("nearme", "loadmore");
	} else if (expiration) {
		getActiveAPI("expiration", "loadmore");
	} else {
		getActiveAPI("no_type", "loadmore");
	}

});

/*
 * open redeem popup window from components/wallet_popup.js file.
 */
function getRedeem(e) {
	var item = e.section.getItemAt(e.itemIndex);

	Alloy.Globals.mainWindow.openWindow({
		name : 'components/wallet_popup',
		data : {
			parent : $,
			items : item,
			index : e.itemIndex,
			marker : i,
			total_rewards : active_rewards.models[0].get('total_rewards')
		}
	});

}
