var args = arguments[0] || {};

var user = Alloy.Models.instance('users');

var redeemed_rewards = Alloy.Collections.instance('redeemed_rewards');
//Ti.API.info("redeemed rewards: " + JSON.stringify(redeemed_rewards));

//value for loadmore function call.
var alphabetical = false;
var most_recent = false;
/*
 * This function will execute sorting function and display sorted data on listview.
 */
$.viewAZ.addEventListener('click', function(e) {
	$.viewAZ.backgroundColor = Alloy.Globals.colors.themeColor;
	$.labelAZ.color = Alloy.Globals.colors.white;
	$.viewRecent.backgroundColor = 'white';
	$.labelRecent.color = Alloy.Globals.colors.themeColor;
	
	alphabetical = true;
	most_recent = false;
	page = 1;
	i = 20;
	getRedeemed('alphabetical','');
});

/*
 * Refresh and display order by most recent data.
 */
$.viewRecent.addEventListener('click', function(e){
	$.viewAZ.backgroundColor = 'white';
	$.labelAZ.color = Alloy.Globals.colors.themeColor;
	$.viewRecent.backgroundColor = Alloy.Globals.colors.themeColor;
	$.labelRecent.color = Alloy.Globals.colors.white;

	alphabetical = false;
	most_recent = true;
	page = 1;
	getRedeemed('most_recent','');
});

var data = [];

redeemed_rewards.fetch({
	success : function(e){
		
		$.labelRedeemed.text = "You have redeemed the following " + redeemed_rewards.models[0].get('total_rewards') + " rewards:"; 

		//Ti.API.info("response; " + JSON.stringify(redeemed_rewards));
		for (var i in redeemed_rewards.models[0].get('redeemed_rewards')) {
			var redeemed = redeemed_rewards.models[0].get('redeemed_rewards')[i];

			data.push({
				template : 'template',
				properties : {
					height : '80dp'//Ti.UI.SIZE
				},
				pic : {
					image : redeemed.offer.logo_url
				},
				desc : {
					text : redeemed.offer.description
				},
				expiration : {
					text : "Valid through " + redeemed.offer.expiration
				},
				//name : redeemed.redeemed_rewards[i].offer.name,  //client needs to add name obj in their api
				allRedeemed : redeemed.offer
			});
			
		}
		
		$.section.setItems(data);
	},error: function(err){
		Ti.API.info(err);
	}
	
});


var page = 1;

function getRedeemed(type, loadmoreValue) {

	Alloy.Globals.activityIndicator.show("Loading");
	
	var client = Ti.Network.createHTTPClient();
	
	var url;
	var loadmore = false;
    //if function call type is loadmore, it will make to page value + 1. It makes to call next page.
	if (loadmoreValue == "loadmore") {
		loadmore = true;
		page = page + 1;
	}else{
		loadmore = false;
	}

	//reset listview marker for loadmore, because this page has three sorting type in one page, but marker event fires only one.
	if (type == "alphabetical") {
				
		url = Alloy.CFG.useekWebService+"/users/" + Ti.App.Properties.getString("userId") + "/redeemed_rewards/?sort_by=name?page=" + page;
	} else {
		url = Alloy.CFG.useekWebService+"/users/" + Ti.App.Properties.getString("userId") + "/redeemed_rewards/?page=" + page;
	}

	client.open("GET", url);
	client.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml");
	client.setRequestHeader("Content-Type", "application/json");
	client.setRequestHeader("Authorization", 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"');
	client.ondatastream = function(e){
		client.payload = client.payload || "";
		try{
			Ti.API.info("acu");
			client.payload += e.source.responseText;
			
		}catch(ex){
			Ti.API.error(ex);	
		}
	};
	client.onload = function(e) {
		//clean up and display alphabetical sorting on listview.
		data.length = 0;
			var json = {};
		if(client.payload && client.payload.length > 0){
			try{
			json = JSON.parse(client.payload);
			}catch(ex){
				json = JSON.parse(this.responseText);
			}
		}else{
			json = JSON.parse(this.responseText);
		}
//Ti.API.info( "json: " + JSON.stringify(json));

		for (var i in json.redeemed_rewards) {

			var redeemed = json.redeemed_rewards[i];

			//if expiring is true, addition number and display on expiring soon number.
			if (redeemed["expiring?"] == true) {
				expNum += i;
			}

			data.push({
				template : 'template',
				properties : {
					height : '80dp'//Ti.UI.SIZE
				},
				pic : {
					image : redeemed.offer.logo_url
				},
				desc : {
					text : redeemed.offer.description
				},
				expiration : {
					text : "Valid through " + redeemed.offer.expiration
				},
				//name : redeemed.redeemed_rewards[i].offer.name,  //client needs to add name obj in their api
				allRedeemed : redeemed.offer
			});

		}

		//if listview marker event fired, it will append items on list
		if (loadmore) {
			$.section.appendItems(data);
			//$.labelRewards.text = "You have the following " +  $.section.items.length + " rewards available:";
		} else {
			$.section.setItems(data);
		}

		
		
		Alloy.Globals.activityIndicator.hide();

	};
	client.onerror = function(e) {
		Ti.API.debug(e.error);
	};
	client.timeout = 10000;
	client.send();

}


//load page number.
var i = 20;

// Set the initial item threshold
$.listView.setMarker({sectionIndex:0, itemIndex: (i - 1) });

// Load more data and set a new threshold when item threshold is reached
$.listView.addEventListener('marker', function(e){
  
    i += 20;
	$.listView.setMarker({sectionIndex:0, itemIndex: (i - 1)});
    if(alphabetical){
    	getRedeemed("alphabetical","loadmore");
    }else if(most_recent){
    	getRedeemed("most_recent","loadmore");
    }else{
    	getRedeemed("no_type","loadmore");
    }

});




//prev code will delete
/*
function displayRedeemedRewards(){
for (var i in redeemed_rewards.models) {

	var redeemed = redeemed_rewards.models[i];

	data.push({
		template : 'template',
		properties : {
			height : '55dp'//Ti.UI.SIZE
		},
		pic : {
			image : redeemed.get("offer").logo_url
		},
		desc : {
			text : redeemed.get("offer").description
		},
		expiration : {
			text : "Valid through " + redeemed.get("offer").expiration
		},
		//name : redeemed.get("offer").name, //client needs to add name obj in their api
		allRedeemed : redeemed.offer
	});

}
$.section.setItems(data);
Alloy.Globals.activityIndicator.hide(); 
}
displayRedeemedRewards();
*/


