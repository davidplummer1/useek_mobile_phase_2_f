/**
 * This controller loads several sets of levels and games for the user to choose from.
 * The user can access this section by using the pull tab at the bottom of the main.js controller.
 * 
 */


var args = arguments[0] || {};


var async = require("async");

/** data collection for the all games list */
var games = Alloy.Collections.instance('games');

var user = Alloy.Models.instance('users');

/** data collection for the active games list */
var active_games = Alloy.Collections.instance('active_games');

/** data collection for the recommended games list */
var recommendations = Alloy.Collections.instance('recommendation_games');

/** data collection for the new releases list */
var new_releases = Alloy.Collections.instance('new_releases');


/** 
 * fetch the all games list 
 * @param {Function} asyncCallback the callback method for the async module
 * */
function getGames(asyncCallback) {

	//if the user isn't connection to the internet, stop
	if (!Alloy.Globals.isOnline()) {
		callback && callback(true, {
			message : "Please check your internet connection and try again."
		});
		asyncCallback(true);
		return;
	}
	//if the activity indicator isn't shown, display it
	if (!Alloy.Globals.activityIndicator.shown) {
		Alloy.Globals.activityIndicator.show("Loading...");
	}

	games.fetch({
		success : function(e) {

			try {

				//populate the active games list
				games.each(function(item) {

					var allLevels = item.get("levels");

					for (var c in allLevels) {

						var allGameViews = Alloy.createController('components/active_games', {
							gameId : allLevels[c].game_id,
							levelId : allLevels[c].id,
							
							type : 'games'
						}).getView();

						$.scrollAllGames.add(allGameViews);

					}
				});
				
				asyncCallback && asyncCallback(null);
			} catch(ex) {
				Alloy.Globals.activityIndicator.hide();
				asyncCallback && asyncCallback(true, ex);
			}
		},
		error : function(err) {

			asyncCallback && asyncCallback(true, err);
		}
	});

}

/** 
 * fetch the recommended games 
 * @param {Function} asyncCallback the callback method for the async module
 * */
function getRecommendedGames(asyncCallback) {

	//if the user isn't connection to the internet, stop
	if (!Alloy.Globals.isOnline()) {
		callback && callback(true, {
			message : "Please check your internet connection and try again."
		});
		return;
	}
	//if the activity indicator isn't shown, display it
	if (!Alloy.Globals.activityIndicator.shown) {
		Alloy.Globals.activityIndicator.show("Loading...");
	}

	recommendations.fetch({
		success : function(e) {
			try {

				//populate the recommended game scroll view
				for (var a in recommendations.models) {

					var recommendGame = recommendations.models[a].toJSON();

					var recommendViews = Alloy.createController('components/active_games', {
						gameId : recommendGame.id,
					
						type : 'recommendation_games'
					}).getView();

					$.scrollRecommend.add(recommendViews);

				}
				asyncCallback && asyncCallback(null);

			} catch(ex) {
				Alloy.Globals.activityIndicator.hide();
				asyncCallback && asyncCallback(true, ex);
			}
		},
		error : function(err) {

			asyncCallback && asyncCallback(true, err);
		}
	});
}

/** 
 * fetch the active games 
 * @param {Function} asyncCallback the callback method for the async module
 * */
function setActiveGames(asyncCallback) {

	//if the user isn't connection to the internet, stop
	if (!Alloy.Globals.isOnline()) {
		callback && callback(true, {
			message : "Please check your internet connection and try again."
		});
		return;
	}
	//if the activity indicator isn't shown, display it
	if (!Alloy.Globals.activityIndicator.shown) {
		Alloy.Globals.activityIndicator.show("Loading...");
	}

	//display bottom active games
	try {

		for (var s in active_games.models) {

			// Remove empty element in collection for avoiding crash
			if (active_games.models[s].get("levels") == null) {
				active_games.remove(active_games.models[s]);
			}

			var actives = active_games.models[s];

			//Ti.API.info("active: " + JSON.stringify(actives))

			var activeViews = Alloy.createController('components/active_games', {
				gameId : actives.id,
				
				type : 'active_games'
			}).getView();

			$.scrollActive.add(activeViews);

		}
		refreshActiveGames();
		asyncCallback && asyncCallback(null);
	} catch(ex) {
		Alloy.Globals.activityIndicator.hide();
		asyncCallback && asyncCallback(true, ex);
	}

}

/** 
 * fetch the new releases
 * @param {Function} asyncCallback the callback method for the async module
 * */
function getNewReleases(asyncCallback) {

	//if the activity indicator isn't shown, display it
	if (!Alloy.Globals.activityIndicator.shown) {
		Alloy.Globals.activityIndicator.show("Loading...");
	}

	new_releases.fetch({
		success : function(e) {
	
			try {
				//display bottom all games
				new_releases.forEach(function(item) {

					var levels = item.get("levels");

					for (var e in levels) {

						var new_releaseViews = Alloy.createController('components/active_games', {
							gameId : levels[e].game_id,
							levelId : levels[e].id,
							
							type : 'new_releases'
						}).getView();

						$.scrollNewReleases.add(new_releaseViews);

					}
				});
				asyncCallback && asyncCallback(null);
			} catch(ex) {
				Alloy.Globals.activityIndicator.hide();
				asyncCallback && asyncCallback(true, ex);
			}
		},
		error : function(err) {
			asyncCallback && asyncCallback(true, err);
		}
	});

}

/** show/hide the active games list */
function refreshActiveGames() {

	//if there are no active games available, hide the active games list
	if (active_games.models.length > 0) {
		$.labelActive.setVisible(true);
		$.activeGamesContainer.applyProperties({
			height : Ti.UI.SIZE,
			visible : true
		});
	} else {
		$.activeGamesContainer.applyProperties({
			height : 0,
			visible : false
		});
	}
}

/** defines the current state of the pull tab (true: the pull tab is down; false: the pull tab is up) */
var pullTabRevealed = false;

/**
 * show/hide the pull tab
 * @param {Boolean} newState the new state of the pull tab (true: pull it up; false: drop it down)
 */
function togglePullTab(newState) {

	var t = Ti.UI.create2DMatrix();
	t = t.rotate(180 * newState);
	$.pullTabIcon.animate({
		transform : t,
		duration : 300
	});

	//if the pull tab should be revealed, animate it upward
	if (newState) {

		$.pullTab.animate({
			top : (parseInt(Alloy.Globals.navigationBarHeight) - 10 ) + "dp",
			duration : 300
		}, function() {
			
			//redraw the pull tab position for android (buggy)
			if (OS_ANDROID) {
				$.pullTab.setTop((parseInt(Alloy.Globals.navigationBarHeight) - 10 ) + "dp");
			}
			
			//if the pull tab used to be down, populate the pull tab with data
			if (newState != pullTabRevealed) {
				pullTabRevealed = true;
				$.loadPullTabData();
			}
			
			//hide the transparent overlay
			$.pullTabOverlay.applyProperties({
				visible : !pullTabRevealed
			});
		});

		
		$.scrollViewWrapper.animate({
			opacity : 1.0,
			duration : 300
		});
	} else {
		//drop the pull tab
		
		pullTabRevealed = false;

		$.pullTab.animate({
			top : "80%",
			duration : 300
		}, function() {
			
			//redraw the pull tab's position for android (buggy)
			if (OS_ANDROID) {
				$.pullTab.setTop("80%");
			}
			//empty the pull tab of content
			$.scrollActive.removeAllChildren();
			$.scrollRecommend.removeAllChildren();
			$.scrollNewReleases.removeAllChildren();
			$.scrollAllGames.removeAllChildren();
			$.pullTabOverlay.applyProperties({
				visible : !pullTabRevealed
			});
		});
		$.scrollViewWrapper.animate({
			opacity : 0.7,
			duration : 300
		});
	}
}

/** load the data which should populate the scroll views in the pull tab */
$.loadPullTabData = function() {
	//fetch the required content from the useek in a sequential fashion
	$.labelRecommend.setVisible(true);
	$.labelAllGames.setVisible(true);
	$.labelNewReleases.setVisible(true);
	async.parallel([setActiveGames, getRecommendedGames, getNewReleases, getGames], $.asyncCallback);
};

/**
 * The click event handler for the pull tab which toggles it upward/downward based on its current state
 */
function togglePullTabEventHandler(e) {
	togglePullTab(!pullTabRevealed);
}

/** drags the pull tab to the current position of the user's finger 
 * @param {Object} e the touchmove event
 * */
$.drag = function(e) {
	
	//convert the pull tab's x/y coordinates to global coordinates
	var point = $.pullTabOverlay.convertPointToView({
		x : e.x,
		y : e.y
	}, Alloy.Globals.mainWindow.getView());

	//set the pull tab's vertical position
	$.pullTab.setTop((parseInt(point.y - $.startPoint) + ( OS_IOS ? "dp" : "px")));
};

/** make the pull tab snap to the top of the screen or the bottom of the screen
 * based on how far it's been dragged vertically
 * @param {Object} e the touchcancel/touchend event
 */
$.snapBack = function(e) {
	
	if(!e || !e.x || !e.y){
		togglePullTab(pullTabRevealed);
		return;
	}
	var currentPoint = $.pullTabOverlay.convertPointToView({
		x : e.x,
		y : e.y
	}, Alloy.Globals.mainWindow.getView());
	Ti.API.info($.startPoint + ", " + currentPoint.y + ", ");
	/*if the pull tab has been pull up at least a third of the screen's height and it used
	to rest at the bottom of the screen, move it upward */
	if (currentPoint.y - $.startPoint < (Alloy.Globals.pH * .67) && !pullTabRevealed) {
		togglePullTab(true);
		
	/* if the pull tab has been pulled down more than 20% of the screen's height and
	it used to rest at the top of the screen, drop it downward */
	
	} else if (currentPoint.y - $.startPoint > (Alloy.Globals.pH * .20) && pullTabRevealed) {
		togglePullTab(false);
	} else {
		
		//move the pull tab back to it's current resting place without loading any data
		togglePullTab(pullTabRevealed);

	}
};

/** save the x/y coordinate for the touchstart event 
 * @param {Object} e touchstart event
 * */
$.setStartPoint = function(e) {
	$.startPoint = e.y;
};

/** the callback method for the async module 
 * @param {Boolean} error true: an error has occurred
 * @param {Object} details an object container details about the error (optional)
 */
$.asyncCallback = function(error, details) {

	//if the sequential content fetch  failed, stop fetching content and display an error
	if (error) {
		Alloy.Globals.activityIndicator.hide();
		alert(details.message || "An unexpected error occurred. Please try again.");

	}
	//show the left menu if it's still hidden

	Alloy.Globals.activityIndicator.hide();

};



/**
 * turn the event listener on/off for this controller
 * @param {Boolean} on true: enable the event listeners; false: disable the event listeners
 */
$.toggleEventListeners = function(on) {

	if (on) {
		$.pullTabOverlay.addEventListener("touchmove", $.drag);
		$.pullTabOverlay.addEventListener("touchstart", $.setStartPoint);
		$.pullTabOverlay.addEventListener("touchend", $.snapBack);
		$.pullTabOverlay.addEventListener("touchcancel", $.snapBack);

		$.pullTabHeader.addEventListener("touchmove", $.drag);
		$.pullTabHeader.addEventListener("touchstart", $.setStartPoint);
		$.pullTabHeader.addEventListener("touchend", $.snapBack);
		$.pullTabHeader.addEventListener("touchcancel", $.snapBack);

	} else {
		$.pullTabOverlay.removeEventListener("touchmove", $.drag);
		$.pullTabOverlay.removeEventListener("touchstart", $.setStartPoint);
		$.pullTabOverlay.removeEventListener("touchend", $.snapBack);
		$.pullTabOverlay.removeEventListener("touchcancel", $.snapBack);
	
		$.pullTabHeader.removeEventListener("touchmove", $.drag);
		$.pullTabHeader.removeEventListener("touchstart", $.setStartPoint);
		$.pullTabHeader.removeEventListener("touchend", $.snapBack);
		$.pullTabHeader.removeEventListener("touchcancel", $.snapBack);

	}

};
