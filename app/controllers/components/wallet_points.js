var args = arguments[0] || {};

//var user = Alloy.Models.instance('users');
var active_games = Alloy.Collections.instance('active_games');
//Ti.API.info("active_games: " + JSON.stringify(active_games));

//control alphabetical sort.
var alphabetical = false;

/*
 * This function makes to sort by sponsors name alphabetically and unalphabetically.
 */
function sorting(a, b) {
	if (alphabetical !== true) {

		if (a.name < b.name)
			return -1;
		if (a.name > b.name)
			return 1;
		return 0;

	} else {

		if (a.name > b.name)
			return -1;
		if (a.name < b.name)
			return 1;
		return 0;

	}
}

/*
 * This function will execute sorting function and display sorted data on listview.
 */
$.viewAZ.addEventListener('click', function(e) {
	$.viewAZ.backgroundColor = Alloy.Globals.colors.themeColor;
	$.labelAZ.color = Alloy.Globals.colors.white;
	$.viewRecent.backgroundColor = 'white';
	$.labelRecent.color = Alloy.Globals.colors.themeColor;

	if (alphabetical !== false) {
		data.sort(sorting);
		data.push();
		$.section.setItems(data);
		alphabetical = false;

	} else {
		data.sort(sorting);
		data.push();
		$.section.setItems(data);
		alphabetical = true;
	}

});

/*
 * Refresh and display order by most recent data.
 */
$.viewRecent.addEventListener('click', function(e) {
	$.viewAZ.backgroundColor = 'white';
	$.labelAZ.color = Alloy.Globals.colors.themeColor;
	$.viewRecent.backgroundColor = Alloy.Globals.colors.themeColor;
	$.labelRecent.color = Alloy.Globals.colors.white;

	//clear array and display data on listview
	data.length = 0;
	mostRecent();
});

var total_playedGame = 0;

var data = [];

/*
 * Fetch active games and display on listview .
 */
function mostRecent() {
	for (var s in active_games.models) {

		if (active_games.models[s] !== null) {

			var actives = active_games.models[s];
			//Ti.API.info(JSON.stringify(actives))
			for (var y in actives.get('levels')) {
				var levels = actives.get('levels')[y];

				var earnable_points = require('tools').roundDecimal(actives.get('earnable_points'));
				var earnable_rewards = require('tools').roundDecimal(actives.get('earnable_rewards'));
				var _earnable_rewards = earnable_rewards >= 1000 ? (earnable_rewards / 1000).toFixed(1) + "K" : earnable_rewards;

				data.push({
					template : 'template',
					properties : {
						height : '75dp'//Ti.UI.SIZE
					},
					pic : {
						image : levels.video.image_urls.medium
					},
					title : {
						text : levels.name
					},
					point : {
						text : earnable_points >= 1000 ? (earnable_points / 1000).toFixed(1) + "K" : earnable_points //actives.get('earnable_points')
					},
					reward : {
						text : "$" + _earnable_rewards//actives.get('earnable_rewards')
					},
					level : levels,
					id : levels.game_id,
					name : levels.name,
					type : 'wallet_point'
				});

				$.labelGames.text = "You've played the following " + data.length + " games.";

			}
		}
	}
	$.section.setItems(data);
}

//fire mostRecent function.
mostRecent();

//open video page when user click play again button.
function playVideo(e) {
	var item = $.section.getItemAt(e.itemIndex);
	$.hiddenWin = Alloy.createController('hidden_win', item);
	$.hiddenWin.getView().open();

	//Alloy.createController('video', item).getView().open();
}
