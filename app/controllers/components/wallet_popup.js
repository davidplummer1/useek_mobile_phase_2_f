/*
 * Wallet popup view, when user click redeem button.
 * This view displays barcode and detail informaiton.
 */
var args = arguments[0] || {};
//Ti.API.info("args: "+JSON.stringify(args))
$.container.addEventListener('androidback', function() {
	closeWin();
});

$.imageLogo.image = args.items.pic.image;
$.labelDesc.text = args.items.desc.text;
$.labelValue.text = "Value: $" + require("tools").roundDecimal(args.items.offer.worth);
$.labelBody.text = args.items.offer.body;
$.labelLegal.text = args.items.offer.legal;

$.labelMaxUses.text = args.items.offer.max_uses === 0 ? "This offer may be used unlimited times. " : "This offer may be used " + args.items.offer.max_uses + " times. ";
$.labelTimeLimit.text = args.items.offer.use_time_limit === 0 ? "There is no time limit. " : "This offer must be used within " + args.items.offer.use_time_limit + " minute of redemption. ";

/*
 * redeem coupon
 */
$.buttonRedeem.addEventListener('click', function(e) {
	Alloy.Globals.activityIndicator.show('Loading');

	// Let server knows to redeem coupon.
	var client = Ti.Network.createHTTPClient();
	//alert(Alloy.CFG.useekWebService + "/earnings/" + args.items.offer.id + "/redeem")
	client.open("POST", Alloy.CFG.useekWebService + "/earnings/" + args.items.id + "/redeem");
	client.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml");
	client.setRequestHeader("Content-Type", "application/json");
	client.setRequestHeader("Authorization", 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"');
	client.onload = function(e) {
		var json = JSON.parse(this.responseText);
		Ti.API.info("success: " + JSON.stringify(json));

		$.labelRemainingTitle.text = 'Max Uses:';
		$.labelRemaining.text = parseInt(args.items.offer.max_uses) == 0 ? "Unlimited" : "You have " + parseInt(args.items.offer.max_uses - json.reward.reward_use_count) + " uses remaining";
		$.buttonRedeem.visible = false;
		$.viewLimits.height = 0;
		$.labelValue.visible = false;
		$.labelBody.visible = false;
		$.labelLegal.visible = false;
		
		// $.viewTimeLimit.height = 0;
		$.labelExp.visible = true;
		$.viewExp.height = Ti.UI.FILL;

		//if barcode is exist, display it. 
		if (json.reward.offer.coupon_url) {
			$.viewRedeemed.height = Ti.UI.SIZE;
			$.imageBarcode.applyProperties({
				height : Ti.UI.SIZE,
				image : (json.reward.offer.coupon_url.medium ||json.reward.offer.coupon_url.high || json.reward.offer.coupon_url.thumbnail )
			});
		} else {
			$.imageBarcode.height = 0;
		}
		//if promocode is exist, display it.
		if ( json.reward.offer.promo_code !== undefined && json.reward.offer.promo_code !== null && json.reward.offer.promo_code !== "" ) {
			$.viewRedeemed.height = Ti.UI.SIZE;
			$.labelPromocode.height = Ti.UI.SIZE;
			$.labelPromoDetail.height = Ti.UI.SIZE;
			$.labelPromoDetail.text = json.reward.offer.promo_details;
			$.labelPromocode.text = json.reward.offer.promo_code;
		} else {
			$.labelPromocode.height = 0;
			$.labelPromoDetail.height = 0;
		}
		
		//if a redemption url is present, allow the user to navigate to the web page provided
		if ( [null,undefined,""].indexOf(json.reward.offer.redemption_url) == -1) {
			$.labelClick.addEventListener('click', function(e) {
				Alloy.Globals.mainWindow.openWindow({
					name : "components/sponsorWeb",
					animate : true,
					data : {
						url : json.reward.offer.redemption_url,
						name : json.reward.sponsor.name
					}
				});
			});
			$.labelClick.setVisible(true);
			$.viewRedeemed.setHeight(Ti.UI.SIZE);
		}

		//time calculating.
		if (args.items.offer.use_time_limit == 0) {
			//hide expire view, when use of time is unlimited.
			$.viewExp.height = 0;

		} else if (args.items.offer.use_time_limit >= 0) {

			//define minute and second.
			var minute = args.items.offer.use_time_limit - 1;
			var second = 59;

			$.labelMin.text = minute;
			$.labelSec.text = second;

			function timeout() {
				setTimeout(function() {
					// counting seconds.if second is 1,it will count again.
					if (second == 1) {

						if ($.labelMin.text == "0") {

							$.labelMin.text = "0";
							$.labelSec.text = "0";
							$.imageBarcode.image = "/images/expired_barcode.png";
							$.labelClick.height = 0;

						} else {

							$.labelMin.text = parseInt($.labelMin.text) - 1;
							second = 60;
							second--;
							$.labelSec.text = second;
						}

					} else {
						second--;
						$.labelSec.text = second;
					}

					timeout();
				}, 1000);
			}

			timeout();

		}

		//when user clicks redeem, it will remove selected coupon.
		if (args.items.offer.max_uses - json.reward.reward_use_count <= 0) {
			args.parent.section.deleteItemsAt(args.index, 1);
		}

		Alloy.Globals.activityIndicator.hide();
	};
	client.onerror = function(e) {
		Alloy.Globals.activityIndicator.hide();
		Ti.API.info(e.error);
		alert("It has problem to retrieve data. Please try again.");
	};
	client.timeout = 5000;
	client.send();

});

function closeWin() {
	require("alloy/animation")["fadeOut"]($.getView(),300,function(){
	Alloy.Globals.mainWindow.closeWindow();
	});
}

$.buttonClose.addEventListener('click', function(e) {
	closeWin();
});

$.opened = function(){
	require("alloy/animation").fadeIn($.getView(),300);
};

