
$.args = arguments[0] || {};


//if it's tablet size, make bigger size.
if (Alloy.Globals.pW >= 768 && Alloy.Globals.pH >= 1024) {
	$.viewPointsRewards.height = "50dp";
	$.viewPlay.height = "60dp";
	$.viewGoback.height = "60dp";
}
//open video and passing data.
$.viewPlay.addEventListener('click', function(e) {
	if (!Alloy.Globals.isOnline()) {
		return;
	}
	
	//create the hotspot sponsor array
	var sponsorArray = [];
	Ti.API.info("L = " + JSON.stringify($.args.level));
	for(var x in $.args.level.video.hotspots){
		for(var y in $.args.level.video.sponsors){
			if($.args.level.video.sponsors[y].id == $.args.level.video.hotspots[x].sponsor_id){
				sponsorArray.push($.args.level.video.sponsors[y]);
			}
		}
	}
	

	$.videoWin = Alloy.createController('video_win', {
		level : $.args.level,
		id : $.args.id,
		earnable_points : $.args.earnable_points || $.args.point.text,
		earable_rewards : $.args.earnable_rewards || $.args.reward.text,
		sponsors : sponsorArray,
		parent : $
	});
	$.videoWin.getView().open();

});

//close window and change orentation mode.
$.viewGoback.addEventListener('click', function(e) {
	closeWindow();
});
//close window when android back clicked.
$.getView().addEventListener('androidback', function() {
	closeWindow();
});

/** hide the activity indicator */
function hideActivityIndicator(){
	Alloy.Globals.activityIndicator.hide();
}
function closeWindow() {
	//$.getView().orientationModes = [Ti.UI.PORTRAIT];
	$.getView().close();
}

function turnOnWindowCloser() {
	Ti.App.addEventListener("refreshLevelSummary", refreshLevelSummary);
	hideActivityIndicator();
}

function turnOffWindowCloser() {
	
	Ti.API.info("Cleaning up " + typeof($.args && $.args.parent && $.args.parent.focused)); 
		if($.args && $.args.parent && $.args.parent.focused){
			try{
				$.args.parent.focused();
				Ti.API.info("Okely dokely do.");
			}catch(ex){
				Ti.API.info("Ding dang doodily darnit.");
				Ti.API.error(ex);
			}
		}
	Ti.App.removeEventListener("refreshLevelSummary", refreshLevelSummary);
}

/**
 *
 * refresh the level summary details with the most recent recommendation chosen by the user
 * @param {Object} [params]	the JSON object containing the recommended level chosen by the user
 * @cfg params.recommendationId the id the recommended video chosen
 * */
function refreshLevelSummary(params) {
	$.videoWin.videoCompletedController.getView().close();
	$.videoWin.getView().close();
	$.args = params;
	populateScreen();

}

/** populate the screen with the details for this level */

function populateScreen() {

	$.viewLogos.removeAllChildren();

	//Display background image.
	if ($.args.type == 'morelike' || $.args.type == "refreshLevelSummary") {
		$.viewBG.applyProperties({
			backgroundImage : $.args.thumbnail_url
		});
	} else {
		$.viewBG.applyProperties({
			backgroundImage : $.args.image || $.args.pic.image
		});
	}

	//Loop through selected level sponsors and display images.
	for (var i in $.args.level.video.sponsors) {
		var sponsors = $.args.level.video.sponsors[i];

		var hidden_intro_controller = Alloy.createController('components/hidden_logo', {
			"sponsors" : sponsors.logo_url
		}).getView();
		$.viewLogos.add(hidden_intro_controller);
	}

	var earnable_points = require('tools').roundDecimal(($.args.earnable_points || ($.args.point && $.args.point.text) || 0).toString());
    var earnable_rewards = require('tools').roundDecimal(($.args.earnable_rewards || ($.args.reward && $.args.reward.text) || 0).toString().replace("$",""));
	//Display point and rewards data from previsous page.
	$.labelPoints.applyProperties({
		text : earnable_points >= 1000 ? (earnable_points / 1000).toFixed(1) + "K" : earnable_points
	});

	//avoid to display double dollar sign.
	if ($.args.type == "wallet_point") {
		$.labelRewards.applyProperties({
			text : earnable_rewards >= 1000 ? (earnable_rewards / 1000).toFixed(1) + "K" : earnable_rewards
		});
	} else {
		var _earnable_rewards = earnable_rewards >= 1000 ? (earnable_rewards / 1000).toFixed(1) + "K" : earnable_rewards;
		$.labelRewards.applyProperties({
			text : "$" + _earnable_rewards
		});
	}

}



//populate the screen with the details for this level (runs when this window first loads)
populateScreen();

//if the user is coming directly from the levels.js
if ($.args.parent && $.args.parent.openWallet) {
	$.args.parent.child = $;
	// set the event listeners to turn the window closer on/off
	$.getView().addEventListener("open", turnOnWindowCloser);
	$.getView().addEventListener("close", turnOffWindowCloser);
}
/* else if the user is coming from the main.js controller, set the event listeners to close
 all child windows
 */
else {
	/** open the wallet window */
	$.openWallet = function() {

		//close all child windows starting with the top-most window in the stack
		(function getChild(controller) {
			if (controller.child) {
				getChild(controller.child);
			}
			controller.getView().close();
		})($.child);

		//close all windows the stack
		Alloy.Globals.mainWindow.openAndCloseAllWindows({
			name : "wallet"
		});
		$.getView().close();
	};

	if (!$.args.parent || !$.args.parent.openWallet) {
		/**
		 * @event openWallet
		 * Fired by video_completed.js when the user clicks on the redeem button
		 */
		Ti.App.addEventListener("openWallet", $.openWallet);
	}
	/**
	 * de-constructor for this window
	 */
	function cleanup() {
		
		try {
			// set the event listeners to turn the window closer on/off
			$.getView().removeEventListener("open",  hideActivityIndicator);
			
			$.getView().removeEventListener("close", turnOffWindowCloser);
			
			Ti.App.removeEventListener("openWallet", $.openWallet);
				
			$.getView().removeEventListener("open", turnOnWindowCloser);
		} catch(ex) {
		}
	}

	/**
	 * @event close
	 * Fired when this window closes
	 */
	$.getView().addEventListener("close", cleanup);
	$.getView().addEventListener("open",  hideActivityIndicator);

}

/** close the child window and display an error Message
 * 
 * @param {Boolean} stoppedOnPurpose	true: this level stopped peacefully; false: a bad internet connection has halted this level
 */
$.stopLevelOnAndroid = function(stoppedOnPurpose){

	$.child.getView().close();
	setTimeout(function(e){
		if(!stoppedOnPurpose){
			alert("We detect that you might have a weak internet signal or another error. Please try again.");
			Alloy.Globals.activityIndicator.hide();
		}
	},400);
};
