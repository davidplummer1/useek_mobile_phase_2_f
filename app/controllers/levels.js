var args = arguments[0] || {};



//use the feature game id to retrieve the game details from the featured_games collection
var gameDetails = Alloy.Collections.instance(args.collectionName).where({ id : args.id})[0].toJSON();

$.navBar.setTitle("Games");
$.useAndroidBack = true;
$.navBar.parentController = $;
$.navBar.setSearchIcon(true);
$.navBar.toggleBackButton(true);


//$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());
//Ti.API.info("level: "+JSON.stringify(args.level));
$.parent = args.parent || {};
$.imageFeature.image = require("tools").isTablet() ? gameDetails.image_urls.large : gameDetails.image_urls.medium;
$.labelTitle.text = gameDetails.name;

if (require("tools").isTablet()) {
	$.viewDescBox.right = "30dp";
	$.viewDescBox.top = "10dp";
	$.viewDescBox.bottom = "10dp";
	$.labelPoint.left = '40dp';
	$.labelDollar.left = '40dp';
}

//make same digit number and change to string to get length for converting.
var earnable_points = require('tools').roundDecimal(gameDetails.earnable_points);
var earnable_rewards = require('tools').roundDecimal(gameDetails.earnable_rewards); 

//if number is over 1000, it will convert K		
$.labelPoint.text = earnable_points >= 1000 ? (earnable_points/1000).toFixed(1) + "K" +" Points" : earnable_points + " Points";
$.labelDollar.text =  earnable_rewards >= 1000 ? (earnable_rewards/1000).toFixed(1) + "K" + " Rewards" : "$" + require("tools").roundDecimal(gameDetails.earnable_rewards) + " Rewards";

//total number of sponsor container.
var total_sponsor = 0;

for (var k in gameDetails.levels) {
	
	var level = gameDetails.levels[k];

	//sum of sponsors data for number of avaiable rewards
	total_sponsor += parseInt(level.video.sponsors.length);
	$.labelRewards.text = total_sponsor + " Available Rewards From:";

	//display all sponsors data
	for (var x in level.video.sponsors) {
		var sponsors = level.video.sponsors[x];

		

		var rewardsController = Alloy.createController('components/scroll_rewards', {
			rewards_img : sponsors.logo_url,
			url : sponsors.url,
			name : sponsors.name
		}).getView();

		$.scrollRewards.add(rewardsController);

	}

	var sponsorController = Alloy.createController('components/each_level', {
		level_img : level.video.image_urls.medium,
		title : level.video.title,
		level : level,
		id : level.video.id,
		count : parseInt(k) + 1,
		parent : $
	}).getView();

	$.viewLevelBox.add(sponsorController);

}


/** open the wallet window */
$.openWallet = function(){
	
	//close all child windows starting with the top-most window in the stack
	if($.child){
		(function getChild(controller){
			if(controller.child){
				getChild(controller.child);
			}
			controller.getView().close();
		})($.child);
	}
	
	//close all windows the stack
	Alloy.Globals.mainWindow.openAndCloseAllWindows({
		name : "wallet"
	});
};

/** 
 * @event openWallet
 * Fired by video_completed.js when the user clicks on the redeem button
 */

Ti.App.addEventListener("openWallet",$.openWallet);

/**
 * de-constructor for this view (called by window manager)
 */
$.closed = function(){
	Ti.App.removeEventListener("openWallet",$.openWallet);

};

/** called when this window (view) is given focus by the window manager */
$.focused = function(){	
	$.leaderboard.refreshLeaderboard();
};

/** called when the window is made visible after it's created */
$.opened = function(){
	Alloy.Globals.activityIndicator.hide();
	$.leaderboard.refreshLeaderboard();
};


