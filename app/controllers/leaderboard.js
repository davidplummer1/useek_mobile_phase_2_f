var args = arguments[0] || {};

var leaderboard = Alloy.Collections.instance('leaderboard');

var user = Alloy.Models.instance("users");

//if height is 3.5inch, leaderboard size is longer.
//ratio logic needed.
if (OS_IOS && Alloy.Globals.pH <= 480) {
	$.getView().applyProperties({
		width : Ti.UI.FILL,
		height : "17%",
		backgroundColor : Alloy.Globals.colors.leaderboard
	});
}

//it will not allow click event, when user is on wallet page.
$.allowOpenWindow = true;

/**
 * @event click
 * Triggered when the unredeemed leaderboard button is clicked
 * 
 */
$.viewUnredeemed.addEventListener('click', function(e) {
	if (user.get("anonymous")) {
		showRegistrationMessage();
	} else {
		openWallet("unredeemed");
	}
});

/**
 * @event click
 * Triggered when the redeemed leaderboard button is clicked
 * 
 */
$.viewRedeemed.addEventListener('click', function(e) {
	if (user.get("anonymous")) {
		showRegistrationMessage();
	} else {
		openWallet("redeemed");
	}
});

/**
 * @event click
 * Triggered when the anonymous leaderboard button is clicked
 * 
 */
$.viewPoints.addEventListener('click', function(e) {
	if (user.get("anonymous")) {
		showRegistrationMessage();
	} else {
		openWallet("points");
	}
});

/**
 * @event click
 * Triggered when the giveback leaderboard button is clicked
 * 
 */
$.viewPrizes.addEventListener('click', function(e) {
	if (user.get("anonymous")) {
		showRegistrationMessage();
	} else {
		openWallet("giveback");
	}
});

/*
 * open wallets depends on clicking.
 */
function openWallet(type) {
	if ($.allowOpenWindow) {
		Alloy.Globals.mainWindow.openWindow({
			name : 'wallet',
			animate : true,
			data : type
		});
	}
}

function showMover() {
	$.viewMover.height = '5dp';
}

/** update the labels for the leaderboard header elements */
$.updateLabels = function() {
	var active_rewards_total = require('tools').roundDecimal(leaderboard.models[0].get("active_rewards_total"));
	var redeemed_rewards_total = require('tools').roundDecimal(leaderboard.models[0].get("redeemed_rewards_total"));
	var points_total = require('tools').roundDecimal(leaderboard.models[0].get("points_total"));
	var give_back_total = require('tools').roundDecimal(leaderboard.models[0].get("give_back_total"));
	var expiring_rewards_total = require('tools').roundDecimal(leaderboard.models[0].get("expiring_rewards_total"));
	//if number is over 1000, it will convert K
	$.labelUnredeemed.text = "$ " + (active_rewards_total >= 1000 ? (active_rewards_total / 1000).toFixed(1) + "K" : (active_rewards_total == 0 ? "--" : active_rewards_total));
	$.labelRedeemed.text = "$ " + (redeemed_rewards_total >= 1000 ? (redeemed_rewards_total / 1000).toFixed(1) + "K" : (redeemed_rewards_total == 0 ? "--" : redeemed_rewards_total));
	$.labelPoints.text = points_total >= 1000 ? (points_total / 1000).toFixed(1) + "K" : (points_total == 0 ? "--" : points_total);

	$.labelPrizes.text = "$ " + ( give_back_total >= 1000 ? (give_back_total / 1000).toFixed(1) + "K" : (give_back_total == 0 ? "--" : give_back_total) );

	if (expiring_rewards_total !== 0) {
		$.labelExpSoon.text = expiring_rewards_total >= 1000 ? (expiring_rewards_total / 1000).toFixed(1) + "K" : expiring_rewards_total;
		$.circle.visible = true;
	} else {
		$.circle.visible = false;
	}
};

/** refresh the leaderboard */
$.refreshLeaderboard = function() {

	$.labelPrizesTitle.setText("Give Back");

	//Display leaderboard info
	leaderboard.fetch({
		success : function(e) {

			//Ti.API.info("leaderboard: " + JSON.stringify(leaderboard));
			try {

				$.updateLabels();
			} catch(ex) {
				Ti.API.error(ex);
				//alert("An error occurred while processing your request. Please try again.");
			}
		},
		error : function(err) {
			Ti.API.info(err);
			//alert("An error occurred while fetching the leaderboard data. Please try again.");

		}
	});

};

function showRegistrationMessage() {
	if (Alloy.Models.instance("users").get("anonymous")) {
		require("tools").showAlertDialog({
			message : "Would you like to register in order to view your rewards?",
			buttonNames : ["Yes", "No"],
			cancel : 1
		}, function() {

			Alloy.Globals.mainWindow.openWindow({
				name : "registration",
				animate : true,
				data : {
					guestRegistration : true
				}
			});

		});
	}
}
