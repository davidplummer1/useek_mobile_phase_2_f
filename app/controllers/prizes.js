/**
 * This controller displays the list of all of the prizes earned by this user.
 *
 *
 */

//set the title for the nav bar and the back button behavior
$.navBar.setTitle("Prizes");
$.useAndroidBack = true;
$.navBar.parentController = $;
$.navBar.setSearchIcon(true);

$.data = []; //list item data for the prize list

//$.navBar.toggleBackButton(true);
$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());

var args = arguments[0] || {};

var prizes = Alloy.Collections.instance("prizes");

$.opened = function() {
	
	Alloy.Globals.activityIndicator.show("Loading...");
	
	/**
	 * @event	itemclick
	 * Fired when the user clicks
	 */
	$.prizesList.addEventListener("itemclick", viewPrizeDetails);

	refreshList();
};

$.closed = function() {
	
	/**
	 * @event	itemclick
	 * Fired when the user clicks
	 */
	$.prizesList.removeEventListener("itemclick", viewPrizeDetails);
};

function viewPrizeDetails(event) {
	
}

/** refresh the prize list */
function refreshList(){
	if(!Alloy.Globals.isOnline()){
		Alloy.Globals.activityIndicator.hide();
		return;
	}
	prizes.fetch({
		data : { per_page : 100 },
		success : function(){
			try{
			populateList();
			}catch(ex){
				Ti.API.error(ex);
			}
			Alloy.Globals.activityIndicator.hide();
		},
		error : function(errorJSON,raw){
			Alloy.Globals.activityIndicator.hide();
			alert(raw.message);
		}
	});
}

/** fills the listview with list items composed of prize details */
function populateList(){
	$.data = [];
	
	//for each prize...
	prizes.each(function(item){
		var locked = !item.get("unlocked");
		$.data.push({
			sponsorImage : {
				image : (item.get("sponsor") && item.get("sponsor").logo_url) || null,
				visible : (item.get("sponsor") && item.get("sponsor").logo_url)?true:false
			},
			
			title : {
				text : item.get("description") || ""
			},
			 pointsNeeded: {
				text : locked ? ("points needed: " + (item.get("points_required") || "0") ): ""
			},
			lockStatus : {
				text : locked?"\uf023":"",
				font : { fontFamily : "FontAwesome", fontSize : "25sp" },
				color : locked?"#f00":"#888"
			},
			itemId : item.id
				
		});
	});
	
	//push the list items into the list section
	$.prizesSection.setItems($.data);
}
