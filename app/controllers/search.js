var args = arguments[0] || {};

$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());

//include back button on title bar
$.useAndroidBack = true;
$.navBar.parentController = $;
$.navBar.setSearch(true);
$.navBar.search.hintText = "Search by keyword (title, sponsor etc)";
$.navBar.search.focus();
//when user click background, search view will close
$.viewBG.addEventListener('click', function(e) {
	$.navBar.search.blur();
	$.navBar.search.visible = false;
	Alloy.Globals.mainWindow.closeWindow({
		name : 'search'
	});

});

//height for listview template
var height = "85dp";
var check_height = "28dp";

if(OS_IOS && Ti.Platform.osname == "iphone"){
	height = "85dp";
	check_height = "28dp";
}else if(OS_IOS && Ti.Platform.osname == "ipad"){
	height = "110dp";
	check_height = "45dp";
}else{
	height = "110dp";
	check_height = "32dp";
}


$.navBar.search.addEventListener('return', function(e) {
	$.viewContainer.visible = true;
	getResult(Alloy.CFG.useekWebService+"/search?query=" + $.navBar.search.value);
});

var data = [];

function getResult(url) {
	Alloy.Globals.activityIndicator.show("Loading..");

	var client = Ti.Network.createHTTPClient();
	client.open("GET", url);
	client.setRequestHeader('Content-Type', "application/json");
	client.setRequestHeader("Authorization", 'Token token="' + Ti.App.Properties.getString("sessionToken") + '"');
	client.onload = function(e) {

		//clean array and push new data
		data.length = 0;
		$.viewBG.zIndex=0;
		$.listView.visible = true;
		
		if(Alloy.Globals.pW > 480 && Alloy.Globals.pH > 800) {
			$.viewResult.height = '50dp';
		}else{
			$.viewResult.height = '30dp';
		}
		
		$.navBar.search.blur();

		var json = JSON.parse(this.responseText);
		Ti.API.info("success: " + JSON.stringify(json));

		if (json.games.length !== 0) {

			//display number of result
			$.labelResult.text = json.games[0].levels.length >= 2 ? json.games[0].levels.length + " Results for " + json.games[0].name : json.games[0].levels.length + " Result for " + json.games[0].name;

			for (var i in json.games[0].levels) {
				data.push({
					template : 'template',
					properties : {
						height : height,
						backgroundColor : Alloy.Globals.colors.lightGray
					},
					pic : {
						image : json.games[0].levels[i].video.thumbnail_url
					},
					title : {
						text : json.games[0].levels[i].name
					},
					check : {
						image : json.games[0].levels[i]["completed?"] == true ? '/images/complete_level-25.png' : '/images/level-25.png',
						top : check_height
					},
					id : json.games[0].levels[i].id,
					level : json.games[0].levels[i],
					point : {
						text : json.games[0].levels[i].video.earnable_points
					},
					reward : {
						text : "$" + json.games[0].levels[i].video.earnable_rewards
					},
					earnable_points : json.games[0].levels[i].video.earnable_points,
					earnable_rewards : json.games[0].levels[i].video.earnable_rewards

				});
			}
		} else if (json.levels.length !== 0 && json.games.length == 0) {
			//display number of result
			$.labelResult.text = json.levels.length >= 2 ? json.levels.length + " Results" : json.levels.length + " Result";

			for (var x in json.levels) {
				data.push({
					template : 'template',
					properties : {
						height : height,
						backgroundColor : Alloy.Globals.colors.lightGray
					},
					pic : {
						image : json.levels[x].video.thumbnail_url
					},
					title : {
						text : json.levels[x].name
					},
					check : {
						image : json.levels[x]["completed?"] == true ? '/images/complete_level-25.png' : '/images/level-25.png',
						top : check_height
					},
					id : json.levels[x].id,
					level : json.levels[x],
					point : {
						text : json.levels[x].video.earnable_points
					},
					reward : {
						text : "$" + json.levels[x].video.earnable_rewards
					},
					earnable_points : json.levels[x].video.earnable_points,
					earnable_rewards : json.levels[x].video.earnable_rewards

				});
			}

		} else if (json.rewards.length !== 0 && json.games.length == 0) {
			//display number of result
			$.labelResult.text = json.rewards.length >= 2 ? json.rewards.length + " Results" : json.rewards.length + " Result";

			for (var y in json.rewards) {
				data.push({
					template : 'template',
					properties : {
						height : height,
						backgroundColor : Alloy.Globals.colors.lightGray
					},
					pic : {
						image : json.rewards[y].video.thumbnail_url
					},
					title : {
						text : json.rewards[y].name
					},
					check : {
						image : json.rewards[y]["completed?"] == true ? '/images/complete_level-25.png' : '/images/level-25.png',
						top : check_height
					},
					id : json.rewards[y].id,
					level : json.rewards[y],
					point : {
						text : json.rewards[y].video.earnable_points
					},
					reward : {
						text : "$" + json.rewards[y].video.earnable_rewards
					},
					earnable_points : json.rewards[y].video.earnable_points,
					earnable_rewards : json.rewards[y].video.earnable_rewards

				});
			}

		} else {
			data.length = 0;
			$.listView.visible = false;
			$.viewResult.height = '0dp';
			alert("There is no result. Please try to search by game title, sponsor, level and offer descriptions");
		}

		$.section.setItems(data);

		Alloy.Globals.activityIndicator.hide();

	};
	client.onerror = function(e) {
		Ti.API.info(e.error);
		$.listView.visible = false;
		$.viewResult.height = 0;
		Alloy.Globals.activityIndicator.hide();
	};
	client.timeout = 7000;

	client.send();
}

/**
 * @event	itemclick
 * Triggered when the user clicks on a search result
 */
$.listView.addEventListener('itemclick', function(e) {
	
	//fetch the level object from the list item
	var item = $.section.getItemAt(e.itemIndex);

	//fetch the game data for this level
	var game = Alloy.Collections.instance("games").where({ id : item.level.game_id })[0].toJSON();

	
	//open the levels window
	Alloy.Globals.mainWindow.openAndCloseAllWindows({
			name : 'levels',
			animate : true,
			data : {
				id : game.id,
				level : game.levels,
				name : game.name,
				thumbnail_url : require("tools").getImageURL(game.image_urls),
				earnable_points : game.earnable_points,
				earnable_rewards : game.earnable_rewards
			}
		});
});
