var args = arguments[0] || {};

$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());
$.useAndroidBack = true;
$.navBar.parentController = $;
$.navBar.setTitle(args.title || "");


/** set the html after this view has been opened by the window manager */
$.opened = function() {
	var policies = Alloy.Models.instance("policies");
	Alloy.Globals.activityIndicator.show("Loading...");
	policies.fetch({
		success : function() {
			Ti.API.info(JSON.stringify(policies.toJSON()));
			$.web.applyProperties({
				html : policies.get(args.type)
			});
			Alloy.Globals.activityIndicator.hide();
		},
		error : function(e,raw) {
			
			Alloy.Globals.activityIndicator.hide();
			$.web.applyProperties({
				html : html
			});
		}
	});
}; 