var args = arguments[0] || {};
$.navBar.setTitle('Give Back');
$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());
$.useAndroidBack = false;
$.navBar.setSearchIcon(false);
$.navBar.setHistory(true);

var user = Alloy.Models.instance('users');

var charities = Alloy.Collections.instance('charities');

Alloy.Globals.activityIndicator.show('Loading');

$.displayCharities = function() {
	Alloy.Globals.activityIndicator.show('Loading...');

	charities.fetch({
		success : function(e) {
	
			var data = [];
			var x = e;

			if (charities.models.length > 0) {
				$.labelName.text = charities.models[0].get("name");
			}
			charities.forEach(function(charity) {

				data.push({
					template : 'template',
					properties : {
						height : '60dp'
					},
					logo : {
						image : charity.get("image_urls").medium
					},
					name : {
						text : charity.get("name")
					},
					id : charity.id

				});

			});
			$.section.setItems(data);

			Alloy.Globals.activityIndicator.hide();

		},
		error : function(error) {
			Alloy.Globals.activityIndicator.hide();
		}
	});
};

$.displayCharities();
/*
 * take charity detail page with data
 */
$.listView.addEventListener('itemclick', function(e) {

	var item = $.section.getItemAt(e.itemIndex);

	Alloy.Globals.mainWindow.openWindow({
		name : 'charity_detail',
		animate : true,
		data : {
			charityId : item.id
		}
	});
});

/*
 * select user's charity
 */
function selection(e) {
	var item = $.section.getItemAt(e.itemIndex);

	var dialog = Ti.UI.createAlertDialog({
		cancel : 1,
		buttonNames : ['Yes', 'No'],
		message : 'Would you like to select as partner?',
		title : 'Charity'
	});
	dialog.addEventListener('click', function(e) {
		if (e.index === 0) {
			Alloy.Globals.activityIndicator.show("Loading");

			//trigger selectCharity event and passing charity id
			selectCharity(item.id);

			Alloy.Globals.activityIndicator.show("Loading");

		}
		if (e.index === 1) {
		}
	});
	dialog.show();

}

/*
 * Set the selected charity as the current user's GiveBack partner and returns the updated user object.
 */
function selectCharity(id) {
	var client = Ti.Network.createHTTPClient();
	client.open("POST", Alloy.CFG.useekWebService + "/charities/" + id + "/partner");
	client.setRequestHeader('Content-Type', "application/json");
	client.setRequestHeader("Authorization", 'Token token="' + Ti.App.Properties.getString("sessionToken") + '"');
	client.onload = function(e) {
		var json = JSON.parse(this.responseText);
		//Ti.API.info(JSON.stringify(json));
		//clean array and display updated data.
		
		$.displayCharities();
		alert(json.user.charity.name + " selected successfully");

		Alloy.Globals.activityIndicator.hide();
	};
	client.onerror = function(e) {
		Ti.API.debug(e.error);
		Alloy.Globals.activityIndicator.hide();
	};
	client.timeout = 5000;
	client.send();
}
