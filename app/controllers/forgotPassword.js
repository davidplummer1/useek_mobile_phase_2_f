/**
 * @fileOverview	reset password
 * 					Reset user's password : It makes to send reset function via email to user 
 * @name			reserPassword.js
 */

//include back button on title bar
$.navBar.toggleBackButton(true);
$.navBar.setTitle("Reset Password");
$.useAndroidBack = true;

if(Alloy.Globals.pW >= 768 && Alloy.Globals.pH >= 1024){
	$.email.height = "60dp";
	$.buttonSubmit.height = "60dp";
}

//Send password request to user's email
$.buttonSubmit.addEventListener('click', function(e){
		
		
	//validate the email address inputted
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (reg.test($.email.value) === false) {
		alert('Please Enter a valid email address.');
		return;
	}
	
	Alloy.Models.instance("users").resetPassword($.email.value);

});

