var args = arguments[0] || {};
$.navBar.setTitle('Video List');
$.navBar.toggleBackButton(true);
$.useAndroidBack = true;

var games = Alloy.Collections.instance('games');

var data = [];

games.fetch({
	success : function(e) {
		//Ti.API.info(JSON.stringify(games));
		//Ti.API.info(JSON.stringify(JSON.parse(games.models[0].get('levels'))));
		games.each(function(item){
		

			var levels = JSON.parse(item.get('levels'));

			for (var x in levels) {
				
				data.push({
					template : 'template',
					properties : {
						height : Ti.UI.SIZE
					},
					pic : {
						image : levels[x].video.thumbnail_url
					},
					title : {
						text : levels[x].video.description
					},
					id : item.get('id'),//levels[x].video.id,
					level : levels[x]
				});

			}

		
		});
		$.section.setItems(data);

	},
	error : function(error) {
		alert('Please check internet connection');
	}
});

/*
 * passing video id and all level to video.js
 */
$.listView.addEventListener('itemclick', function(e) {
	var item = e.section.getItemAt(e.itemIndex);
	Alloy.Globals.activityIndicator.show("Loading");
	Alloy.createController('video', {
		id : item.id,
		level : item.level
	}).getView().open();

});
