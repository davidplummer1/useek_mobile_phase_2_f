var args = arguments[0] || {};
args.parent.child = $;
var Social = OS_IOS ? require('dk.napp.social') : {};

//Ti.API.info('video_completed: ' + JSON.stringify(args.data));
//Ti.API.info('video_completed parent: ' + JSON.stringify(args.parent));

//args.pointsEarned

$.labelTitle.text = "Level " + args.data.get('number') + " Completed!";

var earned_points = require('tools').roundDecimal(args.pointsEarned);
var earned_rewards = require('tools').roundDecimal(args.rewardsEarned);

$.labelPoints.text = earned_points >= 1000 ? (earned_points / 1000).toFixed(1) + "K" : earned_points;
var _earned_rewards = earned_rewards >= 1000 ? (earned_rewards / 1000).toFixed(1) + "K" : earned_rewards;
$.labelRewards.text = "$" + _earned_rewards;

var recommendations = Alloy.Collections.instance('recommendation_games');

// populate the recommendations list with the recommendations returned by the game recording process
var moreLikes = Alloy.Collections.instance("moreLikes");

//populate the recommended video section
//TODO refactor the components so they'll only receive the model id
moreLikes.forEach(function(item) {

	var recommendViews = Alloy.createController('components/video_morelike', {
		name : item.get("name"),
		id : item.id,
		thumbnail_url : item.get("video").image_urls.medium,
		levels : item.toJSON(),
		earnable_points : item.get("video").earnable_points,
		earnable_rewards : item.get("video").earnable_rewards,
		parent : $
	}).getView();

	$.scrollMoreLike.add(recommendViews);

});

$.buttonShare.addEventListener('click', function(e) {
	if (OS_IOS) {

		Ti.API.info("module is => " + Social);
		Ti.API.info("Facebook available: " + Social.isFacebookSupported());
		Ti.API.info("Twitter available: " + Social.isTwitterSupported());
		Ti.API.info("SinaWeibo available: " + Social.isSinaWeiboSupported());

		if (Social.isActivityViewSupported()) {//min iOS6 required
			Social.activityView({
				text : "", //Share like a king!
				image : "", //pin.png
				removeIcons : "print,copy,contact,camera"
			});
		} else {
		}
	} else if (OS_ANDROID) {
		var file = Titanium.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, 'viaInstaCheckIn.jpeg');
		var B = file.read();

		var shareIntent = Ti.Android.createIntent({
			action : Ti.Android.ACTION_SEND,
			type : "image/jpeg"
		});

		shareIntent.putExtra(Ti.Android.EXTRA_TITLE, "");
		//Share like a king! - TITLE
		shareIntent.putExtra(Ti.Android.EXTRA_TEXT, "");
		//Share like a king! - TEXT
		//shareIntent.putExtraUri(Ti.Android.EXTRA_STREAM, B.nativePath);
		Ti.Android.currentActivity.startActivity(Ti.Android.createIntentChooser(shareIntent, "Share image"));
	}
});

/** @event click
 * Triggered when the redeem button is pressed
 */
$.buttonRedeem.addEventListener('click', function(e) {

	if (Alloy.Models.instance("users").get("anonymous")) {
		require("tools").showAlertDialog({
			message : (args.messages && args.messages.info && args.messages.info[0]) || "Would you like to register in order to redeem your rewards?",
			buttonNames : ["Yes", "No"],
			cancel : 1
		}, function() {
			Alloy.Globals.activityIndicator.show("Loading...");
			setTimeout(function() {
				$.showRegistrationWindow();
			}, 2000);

		});
	} else {
		Ti.App.fireEvent("openWallet");
	}
});

/** event listener to replay the current level */
$.buttonAgain.addEventListener('click', function(e) {
	Ti.App.fireEvent("replayLevel", {
		childWindow : $.getView()
	});
});

$.imgExit.addEventListener('click', function(e) {
	//close current window
	$.win.close();
	//close parent window(video)
	args.parent.win.close();
	//need to close grand parent(hidden_win)
	args.parent.closeWin();
});

$.closeParents = function() {
	//close current window
	$.win.close();

	//close parent window(video)
	args.parent.win.close();
	return;
	//need to close grand parent(hidden_win)
	args.parent.closeWin();

	//open wallet page
	Alloy.Globals.mainWindow.openAndCloseAllWindows({
		name : 'wallet',
		type : 'unredeemed'
	});
};

/** display the registration modal for the guest user */
$.showRegistrationWindow = function() {

	Alloy.Globals.activityIndicator.show("Loading...");

	//close current window
	$.win.close();

	//close parent window(video)
	args.parent.win.close();

	//need to close grand parent(hidden_win)
	args.parent.closeWin();

	Alloy.Globals.mainWindow.openWindow({
		name : "registration",
		animate : true,
		data : {
			guestRegistration : true
		}
	});
	Alloy.Globals.activityIndicator.hide();

};

/** check if the user is a guest and encourage them to register if (s)he is */
function checkIfUserIsAGuest() {
	animateDetails();
	//if this is a guest user, encourage him/her to register
	if (Alloy.Models.instance("users").get("anonymous")) {
		require("tools").showAlertDialog({
			message : (args.messages && args.messages.info && args.messages.info[0]) || "Would you like to register in order to keep your rewards?",
			buttonNames : ["Yes", "No"],
			cancel : 1
		}, function() {
			Alloy.Globals.activityIndicator.show("Loading...");
			setTimeout(function() {
				$.showRegistrationWindow();
			}, 2000);

		});
	}
	//if the user isn't a guest, but there's still a message to be shown, display it
	else if (args.messages.info && args.messages.info[0] && typeof (args.messages.info[0]) == "string") {
		alert(args.messages.info[0]);
	}
}

/** animates the details of the game summary */
function animateDetails() {

	setTimeout(function() {
		require("alloy/animation").popIn($.viewTitle);
	}, 0);

	//if the user has earned any points for the level completed, animate the earned points
	if (parseInt(earned_points) > 0) {
		setTimeout(function() {
			require("alloy/animation").popIn($.viewPoints);

		}, 100);
	}

	//if the user has earned any monetary rewards, animate the rewards amount
	if (parseFloat(earned_rewards) > 0) {
		setTimeout(function() {
			require("alloy/animation").popIn($.viewRewards);

		}, 200);
	}
}


$.getView().addEventListener("open", checkIfUserIsAGuest);
