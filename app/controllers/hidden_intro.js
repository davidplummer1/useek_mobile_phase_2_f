var args = arguments[0] || {};

//Ti.API.info(JSON.stringify(args));

Alloy.Globals.mainWindow.toggleLandscapeOrientation(true);

for (var i in args.level.video.sponsors) {
	var sponsors = args.level.video.sponsors[i];

	var hidden_intro_controller = Alloy.createController('components/hidden_logo', {
		"sponsors" : sponsors.logo_url
	}).getView();
	$.viewLogos.add(hidden_intro_controller);
}

$.labelPoints.text = args.earnable_points + " Points";
$.labelRewards.text = "$" + args.earnable_rewards;

//open video and play game.
$.buttonPlay.addEventListener('click', function(e) {

	Alloy.Globals.mainWindow.openWindow({
		name : 'video',
		animate : true,
		data : {
			level : args.level,
			id : args.id,
			earnable_points : args.earnable_points,
			earable_rewards : args.earnable_rewards,
			sponsors : args.sponsors
		}
	});

});

//close window.
$.buttonGoback.addEventListener('click', function(e) {

	Alloy.Globals.mainWindow.toggleLandscapeOrientation(false);

	Alloy.Globals.mainWindow.closeWindow({
		name : 'hidden_intro'
	});
});

