var args = arguments[0] || {};

$.navBar.setLeftNavButton(Alloy.createController('menuButton').getView());
$.useAndroidBack = true;
$.navBar.parentController = $;
$.navBar.setTitle("How to Play");

var html = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html lang="en"> <head><meta name="viewport" content="width=device-width, initial-scale=1.0" http-equiv="Content-Type" content="text/html; charset=utf-8"/> </head><body><p class="p1"><strong>Disclaimer: </strong></p><p class="MsoNormal" style="margin: 0in 0in 10pt; color: #222222; font-family: Arial; font-size: x-small;"><span style="font-size: medium;"><span style="font-family: Calibri;">'+
'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></span></p></body></html>';

/** set the html after this view has been opened by the window manager */
$.opened = function(){

	$.web.applyProperties({
		html : html
	});
};