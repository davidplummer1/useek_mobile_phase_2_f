exports.definition = {
	config : {
		useCache : true,
		autoParse : true,
		adapter : {
			type : "restapi",
			collection_name : "new_releases",
			idAttribute : "id" //identifies the unique field for each model
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			url : function() {
				return Alloy.CFG.useekWebService+"/games/new_releases";
			},
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			url : function() {
				return Alloy.CFG.useekWebService+"/games/new_releases";
			},
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});


		return Collection;
	}
};
