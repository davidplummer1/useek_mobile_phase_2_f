exports.definition = {
	config: {
		useCache : false,
		autoParse : true,
		adapter: {
			type: "restapi",
			collection_name: "policies"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			
			/**
			 * @return {String} the url for this API resource 
			 */
			url : function() {
				return Alloy.CFG.useekWebService + "/"+this.config.adapter.collection_name;
			},
			
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			
			/**
			 * @return {String} the url for this API resource 
			 */
			url : function() {
				return Alloy.CFG.useekWebService + "/"+this.config.adapter.collection_name;
			},
			
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Collection;
	}
};