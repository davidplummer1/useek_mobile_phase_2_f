/**
 * This model will keep track of all 
 * 
 * 
 */

exports.definition = {
	config : {
		useCache : true, //if this property is set to true, the data will be cached
		autoParse : true, //if this property is set to true, the sync adapter will attempt to parse the payload into the collection as a series of models

		adapter : {
			type : "restapi",
			collection_name : "givebacks",
			idAttribute : "id"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			
			
			/**
			 * @return {String} the url for this API resource 
			 */
			url : function() {
				return Alloy.CFG.useekWebService + "/users/" + Alloy.Models.instance("users").get("id") + "/givebacks";
			},
			
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			
			
			/**
			 * @return {String} the url for this API resource 
			 */
			url : function() {
				return Alloy.CFG.useekWebService + "/users/" + Alloy.Models.instance("users").get("id") + "/givebacks";
			},
			
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Collection;
	}
};