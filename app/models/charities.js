exports.definition = {
	config : {
		useCache : false,
		autoParse : true,
		adapter : {
			type : "restapi",
			collection_name : "charities"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			
			/** returns the web service url for this model
			 * @return {String} the web service url for this resource
			 * */
			url : function() {
				return  Alloy.CFG.useekWebService+"/charities";

			},

			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			

			/** returns the web service url for this model
			 * @return {String} the web service url for this resource
			 * */
			url : function() {
				return  Alloy.CFG.useekWebService+"/charities";

			},

			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}

		});

		return Collection;
	}
};
