/** 
 * This model holds all of the recommended levels returned by the game recording API call.
 * 					
 * **Note:** This model is used instead of the recommendation_games collection because
 * 	the game recording API call returns an array of recommended levels instead of games.
 * 	These levels will be used to populate the recommendation list which is shown
 *  after a game is completed.
 *
 **/

exports.definition = {
	

	config : {
		useCache : false, //if this property is set to true, the data will be cached
		autoParse : true, //if this property is set to true, the sync adapter will attempt to parse the payload into the collection as a series of models

        adapter : {
			type : "restapi",
			collection_name : "recommendations"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			

			/** returns the web service url for this model
			 * @return {String} the web service url for this resource
			 * */
			url : function() {
				return  "Alloy.CFG.useekWebService+/users/"+ Ti.App.Properties.getString("userId") + "/" + this.config.adapter.collection_name;
			},

			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			
			/** returns the web service url for this model
			 * @return {String} the web service url for this resource
			 * */
			url : function() {
				return  Alloy.CFG.useekWebService+"/users/"+ Ti.App.Properties.getString("userId") + "/" + this.config.adapter.collection_name;

			},

			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			},
			
			

		});

		return Collection;
	}
};

