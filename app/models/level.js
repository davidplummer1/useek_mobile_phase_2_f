exports.definition = {

	/**
	 * @property {Object} config
	 * @property {Number} config.videoLoadingLatencyLimit the amount of milliseconds to wait for a video to load before
	 * stopping a level
	 * @property {Object} config.adapter
	 * @property {String} config.type	the type of sync adapter this resource is tied to
	 * @property {String} config.collection_name	the name of this collection
	 *
	 */
	config : {
		"columns" : {
			"id": "TEXT"
		},
		videoLoadingLatencyLimit : 30000,
		videoPlaybackLatencyLimit : 10000,
		adapter : {
			type : "restapi",
			collection_name : "level"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {

			/**
			 * @property {Object}
			 * @property {String} sponsorIcon
			 */
			sponsorIconBG : {
				hit : "/images/sponsorHitIcon.png",
				miss : "/images/sponsorMissIcon.png",
				active : "/images/sponsorActiveIcon.png"
			},
			/**
			 * @property {Number}
			 * the amount of milliseconds to wait in between each interval tick
			 */
			intervalUnit : 200,

			/**
			 * @property {Number}
			 * this is the amount of milliseconds the active hotspot's sponsor icon will be highlighted before it becomes active on the screen
			 */
			activeHotspotNoticeTime : 1000,

			/**
			 * @property {Boolean}
			 * checking whether or not the game is being played
			 * */
			levelIsPlaying : false,

			/**
			 * @property {Object}
			 * the interval used to track video latency and sponsor visibility
			 * */
			videoMonitorInterval : null,

			/**
			 * @property {Number}
			 * the total amount of consecutive milliseconds the video has taken to load
			 *
			 * */
			videoLoadingLatency : 0,

			/**
			 * @property {Number}
			 * the total amount of consecutive milliseconds the video has frozen
			 * @property {Number} playbackLatency.timestamp
			 * @property {Number} playbackLatency.latencyTotal
			 * */
			playbackLatency : {
				timestamp : 0,
				latency : 0
			},

			/**
			 * @property {Object}
			 * the coordinates for each successful sponsor click
			 * */
			clicks : [],

			pointsEarned : 0,

			rewardsEarned : 0,

			/**
			 * @property {Object}
			 * a JSON object of IDs for each hotspot with their click status (true: clicked; false: missed )
			 */
			hotspotsStatus : {},

			/** monitor the latency of the video as well as the progress sponsor visibility */
			monitorVideo : function() {

				if (this.videoMonitorInterval == null) {
					return;
				}
				this.monitorLoadingLatency() && this.monitorPlaybackLatency() && this.monitorSponsorActivity();
			},

			/** checks how long a video has been loading */
			monitorLoadingLatency : function() {
				
				//if app is running in the background, do nothing
				if(Alloy.Globals.appPaused){	Ti.API.info("monitorLoadingLatency");return;	}
				
				//if the video isn't playing...
				if (!this.levelIsPlaying) {

					//calculate the total amount of time the video has been loading
					this.videoLoadingLatency += this.intervalUnit;

					//if it's taken too long, stop the video
					if (this.videoLoadingLatency >= this.config.videoLoadingLatencyLimit) {
						this.cancelLevel(false);
					} else if (this.videoLoadingLatency > 600) {

						this.controller.toggleLoadingImage(true);
					} else {
						this.controller.toggleLoadingImage(false);
					}

					return false;
				}
				return true;

			},

			/** checks how long the video playback has stuttered */
			monitorPlaybackLatency : function() {
				//if the video is freezing, calculate the amount of time it's been frozen

				// if (this.playbackLatency.timestamp + this.intervalUnit > this.videoPlayer.currentPlaybackTime) {
				// this.playbackLatency.latency += (this.playbackLatency.timestamp + this.intervalUnit) - this.videoPlayer.currentPlaybackTime;
				//
				// } else {
				// this.playbackLatency.timestamp = this.videoPlayer.currentPlaybackTime;
				// //reset the latency since the video has started playing again
				// this.playbackLatency.latency = 0;
				// }
				//
				// //if the latency limit has been reached, stop the level
				// if (this.playbackLatency.latency >= this.config.videoPlaybackLatencyLimit) {
				//
				// this.cancelLevel(false);
				// return false;
				// } else if (this.playbackLatency.latency > 500) {
				// this.controller.toggleLoadingImage(true);
				// } else {
				// this.controller.toggleLoadingImage(false);
				// }

				return true;
			},

			/** highlights any sponsor that corresponds with the current timestamp of the video player */
			monitorSponsorActivity : function() {

				var hotspots = this.get("video").hotspots;

				//mark missed sponsors
				for (var x in hotspots) {

					//if this hotspot's duration has already been passed and the user didn't click it, mark it as missed

					if (["hit", "miss"].indexOf(this.hotspotsStatus[hotspots[x].id]) == -1 && (hotspots[x].end_time * 1000) < this.videoPlayer.currentPlaybackTime) {

						this.selectSponsorIcon({
							sponsorId : hotspots[x].sponsor_id,
							type : "miss"
						});
						this.hotspotsStatus[hotspots[x].id] = "miss";

					} else if (["active", "hit"].indexOf(this.hotspotsStatus[hotspots[x].id]) == -1) {

						//if this hotspot's start_time & end_time are in the range of the video player's current playback time, mark the hotspot as active

						if ((hotspots[x].start_time * 1000 - this.activeHotspotNoticeTime) <= this.videoPlayer.currentPlaybackTime && (hotspots[x].end_time * 1000) >= this.videoPlayer.currentPlaybackTime) {

							//mark the sponsor as active sponsor
							this.selectSponsorIcon({
								sponsorId : hotspots[x].sponsor_id,
								type : "active"
							});
							this.hotspotsStatus[hotspots[x].id] = "active";
						}
					}
				}

			},

			selectSponsorIcon : function(params) {
				//change backgroundColor for sponsors on top, when user click right hotspot.
				for (var k in this.controller.sponsorsArr) {
					//Ti.API.info(hotspots[i].sponsor_id + "," +  this.controller.sponsorsArr[k].id);

					if (params.sponsorId == this.controller.sponsorsArr[k].id) {

						//change the background color for this sponsor
						this.controller.scrollSponsors.children[k].applyProperties({
							backgroundImage : this.sponsorIconBG[params.type]

						});

						//if this isn't a missed sponsor, mark it
						if (params.type !== "miss") {
							require("alloy/animation").popIn(this.controller.scrollSponsors.children[k]);
							//if this is the 6th (or more) sponsor being marked as active, scroll to it
							if (params.type == "active" && parseInt(k) >= 6) {
								var sponsorLogoWidth = parseInt(this.controller.scrollSponsors.children[k].width);

								this.controller.scrollSponsors.scrollTo(((parseInt(k) - 5) * (sponsorLogoWidth + 20)) + ( OS_IOS ? "dp" : ""), 0);
							}
						}

						break;
					}
				}
			},

			/** reset the level stats
			 * @param {object} [parameters] an optional JSON object of parameters this level may use for this game
			 * */
			resetLevel : function(parameters) {

				//if any parameters have been passed to this object, store them in this model
				if (parameters) {
					this.controller = parameters.controller || this.controller || {};
					this.gameId = parameters.gameId || this.gameId || false;
					this.videoPlayer = this.controller.video || this.videoPlayer || {};
					this.viewCanvas = this.controller.viewCanvas || this.viewCanvas || {};

				}

				//mark all of the hotspots as unearned
				var h = this.get("video").hotspots;
				for (var x in h) {
					delete h[x].earned;
				}

				this.clicks = [];
				this.hotspotsStatus = {};
				this.playbackLatency = {
					timestamp : 0,
					latency : 0
				};
				this.videoLoadingLatency = 0;

				//earned point and rewards will calculate with these value
				this.pointsEarned = 0;
				this.rewardsEarned = 0;
				this.controller.labelRewards.setText("0");
				this.controller.labelPoints.setText("0");
			},

			/** toggle the video latency monitor
			 * This function will determine how long of a video lag warrants stopping the video
			 *
			 */
			toggleVideoMonitor : function(on) {

				if (this.levelIsPlaying && on) {

					return;
				}
				var that = this;
				try {
					clearInterval(that.videoMonitorInterval);
					that.videoMonitorInterval = null;

				} catch(ex) {
					that.videoMonitorInterval = null;
				}

				if (on) {
					that.videoMonitorInterval = setInterval(function() {
						that.monitorVideo();
					}, that.intervalUnit);
				}
			},

			/** start the level by playing the video */
			playLevel : function() {
				Ti.API.debug('In level playLevel...');

				var that = this;
				that.toggleVideoMonitor(true);

				//play video
				this.videoPlayer.url = this.get("video").hls_manifest;
				!this.videoPlayer.playing && this.videoPlayer.play();

			},

			/** stop the level and record the result if need be
			 * @param {Object} [options]
			 * @cfg options.success the success callback method for this process
			 * @cfg options.error the error callback method for this process
			 * */
			finishLevel : function(options) {
				Ti.API.debug('In level finishLevel...');
				this.toggleVideoMonitor(false);
				this.videoPlayer.stop();
				this.videoPlayer.url = "";

				//format the rewardEarned as a numerical value with two decimal places

				this.rewardsEarned = parseFloat(Math.round(this.rewardsEarned * 100) / 100);

				this.rewardsEarned = isNaN(this.rewardsEarned) ? "0" : this.rewardsEarned;

				//if the user clicked on at least one offer/prize, record the points
				// if (this.clicks.length > 0) {
				Alloy.Globals.activityIndicator.show("Processing...");
				this.recordPoints(Alloy.CFG.useekWebService + "/games/" + this.get("game_id") + "/levels/" + this.get("id") + "/videos/" + this.get("video_id") + "/record", options);
				// } else {
				// Ti.API.debug('No clicks to process!');
				// // this.controller.getView().close();
				// }

			},

			/** cancel the current level being played
			 * @param {Boolean} stoppedOnPurpose	true: this video was cancelled by the user; false: an error has likely occurred
			 */
			cancelLevel : function(stoppedOnPurpose) {
				Ti.API.info("cancelLevel");
	if(Alloy.Globals.appPaused){ return; }else{ Ti.API.info("app isn't paused"); }
				this.toggleVideoMonitor(false);
				if (!OS_ANDROID) {

					this.resetLevel();

					this.videoPlayer.stop();
					this.videoPlayer.url = "";

					Alloy.Globals.activityIndicator.hide();

					this.controller.getView().close();
					if (this.id == null) {
						return;
					} else {
						this.set({
							id : null
						});
					}
					if (!stoppedOnPurpose && !this.controller.levelComplete) {
						alert("We detect that you might have a weak internet signal. Please try again.");
					}
				} else {
					this.controller.closeForAndroid(stoppedOnPurpose);
				}

			},

			/** record the user's points on the useek server */
			recordPoints : function(link, options) {
				var client = Ti.Network.createHTTPClient();
				client.onload = function() {

					try {
						var payload = JSON.parse(client.responseText);
						var points = Alloy.Models.instance("users").get("points");
						var worth = Alloy.Models.instance("users").get("worth");
						//store the earned points in the
						Alloy.Models.instance("users").set({
							points : (points + (payload.points || 0)),
							worth : (worth + (payload.worth || 0))
						});
						options && options.success && options.success(payload);
					} catch(ex) {
						Alloy.Globals.activityIndicator.hide();
					}
				};
				client.onerror = function(e) {
					Ti.API.error(e);
					alert("An error occurred while trying to save your earned rewards. We apologize for the inconvenience");
					options && options.error && options.error();
				};

				client.open("POST", link);
				client.setRequestHeader("Content-Type", "application/json");
				client.setRequestHeader("Authorization", 'Token token="' + Ti.App.Properties.getString("sessionToken") + '"');
				client.send(JSON.stringify({
					clicks : this.clicks
				}));
			},

			/** determine if the user has clicked on a hotstop at the right time */
			evaluateClick : function(e) {

				if (!this.levelIsPlaying) {
					return;
				}
				//time data is two decimal number but video playback time is milisecond so touch time value needs to times 1000
				var touchTime = this.videoPlayer.currentPlaybackTime / 1000;

				var touchPoint = {
					"x" : (e.x / (Ti.Platform.displayCaps.logicalDensityFactor ? Ti.Platform.displayCaps.logicalDensityFactor : 1 )) / parseInt(e.source.rect.width),
					"y" : (e.y / (Ti.Platform.displayCaps.logicalDensityFactor ? Ti.Platform.displayCaps.logicalDensityFactor : 1 )) / parseInt(e.source.rect.height)
				};
				Ti.API.info('Width: ' + parseInt(e.source.rect.width) + ' OffsetX: ' + parseInt(e.source.rect.x) + ' Height: ' + parseInt(e.source.rect.height) + ' OffsetY: ' + parseInt(e.source.rect.y) + ' x: ' + e.x + ' y: ' + e.y + ' source: ' + e.source.id);
				Ti.API.info('touchPoint: ' + JSON.stringify(touchPoint));
				Ti.API.info('Display caps: ' + JSON.stringify(Ti.Platform.displayCaps));

				//save this click to send to the server
				this.clicks.push({
					"x_cord" : touchPoint.x.toFixed(3), //only 3 digits to the right of the decimal
					"y_cord" : touchPoint.y.toFixed(3), //only 3 digits to the right of the decimal
					video_time : touchTime.toFixed(3) //only 3 digits to the right of the decimal
				});

				var onOff = true;

				var hotspots = this.get("video").hotspots;
				for (var i in hotspots) {

					//if the user has already evaluated this hotspot during this level, skip it
					if (hotspots[i].earned) {
						continue;
					}

					//when touch point is correct also time is correct, get score
					if (touchTime >= hotspots[i].start_time && touchTime <= hotspots[i].end_time) {

						var coordinates = hotspots[i].coordinates;
						var polygon = [];
						for (var x in coordinates) {
							var json = {
								"x" : coordinates[x][0],
								"y" : coordinates[x][1]
							};
							polygon.push(json);
						}

						//if the user is in the range of the polygon, reward him/her
						if (this.isPointInPoly(polygon, touchPoint) == true) {

							//mark this hotspot as having been earned so that it won't be processed again during this level
							hotspots[i].earned = true;

							this.showSuccess("You earned " + hotspots[i].points + (this.pointsEarned > 0 ? " more" : "") + (hotspots[i].points == 1 ? " point!" : " points!"), {
								x : e.x,
								y : e.y
							});
							require("alloy/animation").popIn(this.controller.labelPoints);

							onOff = false;

							//earned points data for displaying completed page.

							//earned rewards data for displaying completed page
							var points = parseInt(hotspots[i].points);

							if (!isNaN(points) && points != null && points > 0) {

								this.pointsEarned += points;
								this.controller.labelPoints.setText(this.pointsEarned);
							}
							for (var index in this.get("video").offers) {
								if ((hotspots[i].offer_id == this.get("video").offers[index].id) && this.get("video").offers[index].worth != null && !isNaN(parseFloat(this.get("video").offers[index].worth))) {
									this.rewardsEarned += parseFloat(require("tools").roundDecimal(this.get("video").offers[index].worth));
									this.controller.labelRewards.setText("$ " + require("tools").roundDecimal(this.rewardsEarned));
									break;
								}
							}

							this.hotspotsStatus[hotspots[i].id] = "hit";

							//change backgroundColor for sponsors on top, when user click right hotspot.
							this.selectSponsorIcon({
								sponsorId : hotspots[i].sponsor_id,
								type : "hit"
							});

							break;
						}

					}

				}

				if (onOff) {

					//penalize the player
					var points = parseInt(this.get("video").miss_value);

					if (!isNaN(points) && points != null) {

						this.pointsEarned += points;
						this.controller.labelPoints.setText(this.pointsEarned);
					}
					require("alloy/animation").shake(this.controller.labelPoints);

					this.showFailure({
						x : e.x,
						y : e.y
					});

					//onOff = false;
				}

			},

			/**
			 * polygon detection function : when pass coordinates and touch point, it will return In or Out.
			 */
			isPointInPoly : function(poly, pt) {
				for (var c = false,
				    i = -1,
				    l = poly.length,
				    j = l - 1; ++i < l; j = i)
					((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y)) && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x) && ( c = !c);
				return c;

			},

			/**
			 * show a success message to the user
			 * @param {String} message	the message to be displayed
			 * @param {Object} coordinates	the x/y coordinate pair for the animation
			 * */
			showSuccess : function(message, coordinates) {
				var that = this;

				//create an animation for android users
				if (OS_ANDROID) {
					coordinates.x += "px";
					coordinates.y += "px";
					Ti.API.info("co = " + JSON.stringify(coordinates));
					this.controller.successImage.applyProperties({
						height : "10dp",
						width : "10dp",
						opacity : 0,
					});
					this.controller.successImageWrapper.applyProperties({
						center : coordinates
					});
					this.controller.successMessage.applyProperties({
						text : message
					});
					this.controller.successPopup.applyProperties({
						opacity : 1
					});
					require("alloy/animation").chainAnimate(this.controller.successImage, [Ti.UI.createAnimation({
						height : "10dp",
						width : "10dp",
						opacity : 0,
						duration : 10
					}), Ti.UI.createAnimation({
						height : "70dp",
						width : "70dp",
						duration : 250,
						opacity : 1
					})], function(e) {

					});

					require("alloy/animation").fadeIn(this.controller.successPopup, 100, function() {

						require("alloy/animation").fadeOut(that.controller.successPopup, 1100);
					});

				} else {
					//create an animation for iOS users
					//Ti.API.info(coordinates);
					this.controller.successImage.applyProperties({
						height : "10dp",
						width : "10dp",
						opacity : 0,
						center : OS_IOS ? coordinates : {
							x : "50%",
							y : "50%"
						}
					});
					this.controller.successMessage.applyProperties({
						text : message
					});
					this.controller.successPopup.applyProperties({
						opacity : 1
					});
					this.controller.successImage.animate({
						height : "70dp",
						width : "70dp",
						duration : 250,
						opacity : 1
					});

					this.controller.successPopup.animate({
						duration : 1250,
						opacity : 0
					});
				}
			},

			/**
			 * show a success message to the user
			 * @param {Object} coordinates	the x/y coordinate pair for the animation
			 * */
			showFailure : function(coordinates) {
				var that = this;

				//create an animation for android users
				if (OS_ANDROID) {
					coordinates.x += "px";
					coordinates.y += "px";
					Ti.API.info("co = " + JSON.stringify(coordinates));
					this.controller.failureImage.applyProperties({
						height : "10dp",
						width : "10dp",
						opacity : 0,
					});
					this.controller.failureImageWrapper.applyProperties({
						center : coordinates
					});
					this.controller.failureMessage.applyProperties({
						text : this.getRandomFailurMessage()
					});
					this.controller.failurePopup.applyProperties({
						opacity : 1
					});
					require("alloy/animation").chainAnimate(this.controller.failureImage, [Ti.UI.createAnimation({
						height : "10dp",
						width : "10dp",
						opacity : 0,
						duration : 10
					}), Ti.UI.createAnimation({
						height : "70dp",
						width : "70dp",
						duration : 250,
						opacity : 1
					})], function(e) {

					});

					require("alloy/animation").fadeIn(this.controller.failurePopup, 100, function() {

						require("alloy/animation").fadeOut(that.controller.failurePopup, 1100);
					});

				} else {
					//create an animation for iOS users
					//Ti.API.info(coordinates);
					this.controller.failureImage.applyProperties({
						height : "10dp",
						width : "10dp",
						opacity : 0,
						center : OS_IOS ? coordinates : {
							x : "50%",
							y : "50%"
						}
					});
					this.controller.failureMessage.applyProperties({
						text : that.getRandomFailureMessage()
					});
					this.controller.failurePopup.applyProperties({
						opacity : 1
					});
					this.controller.failureImage.animate({
						height : "70dp",
						width : "70dp",
						duration : 250,
						opacity : 1
					});

					this.controller.failurePopup.animate({
						duration : 1250,
						opacity : 0
					});
				}
			},

			/** change the failure message that is displayed when a user successfully clicks on a sponsor
			 * @return {String} a randomly selected failure message pulled from level.video.custom_messages
			 * */
			getRandomFailureMessage : function() {
				//get the random message array
				var messageArray = this.get("video").custom_messages || [];

				//grab a random message from the array or use the default message
				return messageArray.length > 0 ? messageArray[Math.floor(Math.random() * messageArray.length)] : "No dice!";

			},

			/** ensure the video isn't lagging more than 3 seconds */
			monitorVideoLatency : function() {

				//capture the video playback time for the first time
				if (!this.playbackTimestamp) {
					this.playbackTimestamp = this.videoPlayer.currentPlaybackTime;
				}

				Ti.API.info("Playback timestamp: " + this.playbackTimestamp + ", " + this.config.videoLatencyThreshold + ", " + (this.playbackTimestamp + this.config.videoLatencyThreshold) + " > " + this.videoPlayer.currentPlaybackTime);
				//if the video has lagged for more than 2 seconds, stop
				if ((this.playbackTimestamp + this.config.videoLatencyThreshold) > this.videoPlayer.currentPlaybackTime) {
					// this.cancelLevel();
				} else {

					//update the video playback time
					//Ti.API.info(this.playbackTimestamp + " - " + this.videoPlayer.currentPlaybackTime);
					this.playbackTimestamp = this.videoPlayer.currentPlaybackTime;
				}

			},
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			},
			
			/**
			 * attempt to unlock a level in the useek API
			 * @param {Object} params
			 *  @param {Object} params.success
			 *  @param {Object} params.error
			 */
			unlock : function(params) {
				var d = require("q").defer();
				var that = this;
				debugger;
				that.save({},{
					url : Alloy.CFG.useekWebService + "/games/"+params.gameId+"/levels/"+params.levelId+"/unlock",
				
					success : function(e){
						debugger;
						Ti.API.info(e);
						
						params && typeof(params.success) == "function" && params.success(e);
						d.resolve(e);
					},
					error : function(e,raw){
						Ti.API.error(e);
						params && typeof(params.error) == "function" && params.error(raw);

						d.reject(raw);
					}
				});
				return d.promise;
			}
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};

