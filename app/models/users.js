exports.definition = {
	config : {

		adapter : {
			type : "restapi",
			collection_name : "users"
		},
		useCache : false
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here

			url : function(e) {
				return Alloy.CFG.useekWebService+"/sessions";
			},

			/** returns the headers for this models
			 * This is preferable to using model.config.headers since headers can be dynamic (auth headers).
			 * @return {Object} JSON object of headers
			 */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			},

			/** refresh the access token and store it in Ti.App.Properties.getString("accessToken") */
			login : function(credentials, callbackMethods) {

				var that = this;
				credentials.device_token = Alloy.CFG.deviceToken;

				//get rid of the existing token information to avoid the update method (put) from being used
				that.clear();

				//get the token
				that.save(credentials, {
					"headers" : {
						"Accept" : "text/html,application/xhtml+xml,application/xml",
						"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
						"Content-Type" : "application/json"
					},
					success : function(e) {

						//capture the username and password to be used for refreshing the token later on
						Ti.App.Properties.setString("auth_key", credentials.auth_key);
						Ti.App.Properties.setString("password", credentials.password);

						//stop refreshing the session
						that.toggleSessionRefresher(false);

						//save the user session data
						that.set(e.get("user"));

						Alloy.Collections.instance("active_games").add(e.get("active_games") || [], {
							merge : false
						});

						/*
						 Alloy.Collections.instance("active_rewards").add(e.get("active_rewards") || [], {
						 merge : true
						 });

						 Alloy.Collections.instance('redeemed_rewards').add(e.get('redeemed_rewards') || [], {
						 merge : true
						 });
						 */

						Ti.App.Properties.setString("userId", e.get("id"));
						Ti.App.Properties.setString("sessionToken", e.get("user").token || "");

						that.toggleSessionRefresher(true);
						callbackMethods && callbackMethods.success && callbackMethods.success(e);
					},
					error : callbackMethods && callbackMethods.error
				});
			},

			/** log the user out of the system */
			logout : function() {
				this.clear();
				Ti.App.Properties.removeProperty("guestLogin");
				Ti.App.Properties.removeProperty("sessionToken");
				Ti.App.Properties.removeProperty("userId");
				this.toggleSessionRefresher(false);

				//if the user doesn't have remember enabled, clear the locally saved credentials
				if (!Ti.App.Properties.getBool("rememberSession", false)) {
					
					require("facebookModule").hardLogout();
					Ti.App.Properties.removeProperty('login_id');
					Ti.App.Properties.removeProperty('login_password');
				}else{
					
					require("facebookModule").softLogout();
				}
			},

			/** refresh the user session */
			refreshSession : function(opts) {
				
				var that = this;
				var options = {};

				//if an old user id already exists, use it
				if (!this.get("id") && Ti.App.Properties.getString("userId")) {
					this.id = Ti.App.Properties.getString("userId");
					this.set({
						id : Ti.App.Properties.getString("userId")
					});
				}
				//if a user id or session id cannot be found locally, return an error
				/*else if(!Ti.App.Properties.hasProperty("sessionToken") || (!this.get("id") && !Ti.App.Properties.hasProperty("userId"))){
				 opts && opts.error && opts.error();
				 return;
				 }*/
				options.success = function(e) {

					//parse the user data and redeemed_rewards out of the session data recevied
					var sessionJSON = e.toJSON();

					Alloy.Collections.instance("active_games").reset(sessionJSON["active_games"], {
						merge : true
					});

					Alloy.Collections.instance("active_rewards").add(sessionJSON["active_rewards"], {
						merge : true
					});

					Alloy.Collections.instance("redeemed_rewards").add(sessionJSON["redeemed_rewards"], {
						merge : true
					});

					that.clear();
					that.set(sessionJSON.user);
					//debugger;
					opts && opts.success && opts.success();

				};
				options.error = (opts && opts.error) ||

				/** try to login again to get a fresh session */
				function() {

					that.toggleSessionRefresher(false);

					//attempt to login again and refresh the session data
					that.login({
						auth_key : Ti.App.Properties.getString("auth_key"),
						password : Ti.App.Properties.getString("password")
					}, {
						success : function() {
							Ti.API.info("background login attempt successful.");
						},
						error : function(e) {
							Ti.API.error(e);
							alert("An error occurred while trying to refresh your access credentials. Please logout and login again.");
						}
					});
				};

				//set the headers
				//options.headers = (opts && opts.error) || this.config.headers;
				options.headers = {};
				options.headers["Authorization"] = 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"';

				options.url = Alloy.CFG.useekWebService+"/users/" + this.id;
				this.fetch(options);
			},

			/** turn the session refresher on/off */
			toggleSessionRefresher : function(on) {

				var that = this;

				if (on) {

					//set the session refresher to run every 59 minutes
					Alloy.Globals.sessionInterval = setInterval(function() {
						that.refreshSession();
					}, 3540000);
				} else {
					try {
						clearInterval(Alloy.Globals.sessionInterval);
					} catch(ex) {
						Alloy.Globals.sessionInterval = null;
					}
				}

			},

			/**	reset a user's password
			 * @param {String} email the email address of the account holder requesting a password reset
			 * */
			resetPassword : function(email) {
				var that = this;
				Alloy.Globals.activityIndicator.show("Loading...");

				//clear the user model
				that.clear();
				that.save({
					email : email,
					device_token : Alloy.CFG.deviceToken
				}, {
					url : Alloy.CFG.useekWebService + "/users/reset_password",
					headers : {
						"Accept" : "text/html,application/xhtml+xml,application/xml",
						"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
						"Content-Type" : "application/json"
					},

					success : function(responseModel, rawResponse) {

						//display an success message
						Alloy.Globals.activityIndicator.hide();

						try {
							if (responseModel.get("messages").info.length > 0) {
								alert("The password email has been sent.");
							} else if (responseModel.get("messages").error.length > 0) {
								alert(responseModel.get("messages").error[0]);
							}
						} catch(ex) {
							alert("This password reset process was unsuccessful. Please try again.");
						}

					},
					error : function(errorModel) {

						//display an error message
						Alloy.Globals.activityIndicator.hide();
						try {
							alert(errorModel.get("messages").error[0]);
						} catch(ex) {
							alert("This password reset process was unsuccessful. Please try again.");
						}
					}
				});
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};
