exports.definition = {
	config : {
		useCache : false, //if this property is set to true, the data will be cached
		autoParse : true, //if this property is set to true, the sync adapter will attempt to parse the payload into the collection as a series of models

		adapter : {
			type : "restapi",
			collection_name : "games"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			
			/** returns the web service url for this model
			 * @return {String} the web service url for this resource
			 * */
			url : function() {
				return Alloy.CFG.useekWebService+"/games/featured";
			},

			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			},
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			/** returns the web service url for this model
			 * @return {String} the web service url for this resource
			 * */
			url : function() {
				return Alloy.CFG.useekWebService+"/games/featured";
			},

			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			},
			/** take each of the levels in each of the game models in this collection and put them in the level collection */
			updateLevelCollection : function(){
				var levelCollection = Alloy.Collections.instance("level");
				this.each(function(item){
					levelCollection.add(item.get("levels")||[],{ merge : true });
				});
			}
		});

		return Collection;
	}
};
