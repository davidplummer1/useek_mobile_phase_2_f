/**
 *	The model/collection definition for the games API resource
 * 
 * 
 * 
 *  
 */

exports.definition = {
	
	/**
	 * @property {Object} config							the configuration settings for this model/collection
	 * 
	 * @property {String} config.useCache 					determines whether or not this model/collection will be cached
	 * by the sync adapter	
	 * @property {String} config.autoParse 					determines whether or not the content retrieved via fetch will
	 * 	automatically be parsed into the model/collection
	 * @property {String} config.url 						the optional url  for this API resource
	 * @property {Object} config.adapter					the configurations pertaining to the applicable sync adapter
	 * @property {String} config.adapter.type				the name of the sync adapter to point to
	 * @property {String} config.adapter.collection_name 	the name of this model/collection
	 * @property {String} config.adapter.idAttribute		the name of the attribute in the model which will be considered the unique id of the model
	 */
	config : {
		useCache : false, //if this property is set to true, the data will be cached
		autoParse : true, //if this property is set to true, the sync adapter will attempt to parse the payload into the collection as a series of models

		
		adapter : {
			type : "restapi",
			collection_name : "games",
			idAttribute : "id"
		}
	},
	
	/** 
	 * extends the game model
	 *
	 *  @param {Object} Model the game model to be extended
	 */
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			
			/**
			 * @return {String} the url for this API resource 
			 */
			url : function() {
				return Alloy.CFG.useekWebService+"/games/";
			},
			
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Model;
	},
	
	/** 
	 * extends the game collection
	 *
	 *  @param {Object} Collection	the games collection to be extended
	 */
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			
			/**
			 * @return {String} the url for this API resource 
			 */
			url : function() {
				return Alloy.CFG.useekWebService+"/games/";
			},
			
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			},
			
			/** take each of the levels in each of the game models in this collection and put them in the level collection */
			updateLevelCollection : function(){
				var levelCollection = Alloy.Collections.instance("level");
				this.each(function(item){
					levelCollection.add(item.get("levels")||[],{ merge : true });
				});
			}
			
		});
		return Collection;
	}
};



