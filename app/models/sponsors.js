exports.definition = {
	config: {
		"columns" : {
			"id": "TEXT"
		},
		adapter: {
			type: "restapi",
			collection_name: "sponsors",
			idAttribute : "id" //identifies the unique field for each model
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};