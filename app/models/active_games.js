exports.definition = {
	config: {
		autoParse : true,
		adapter: {
			type: "restapi",
			collection_name: "active_games",
			idAttribute : "id" //identifies the unique field for each model
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			
			/** take each of the levels in each of the game models in this collection and put them in the level collection */
			updateLevelCollection : function(){
				var levelCollection = Alloy.Collections.instance("level");
				this.each(function(item){
					levelCollection.add(item.get("levels")||[],{ merge : true });
				});
			}
		});

		return Collection;
	}
};