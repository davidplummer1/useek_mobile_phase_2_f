exports.definition = {
	config : {

		adapter : {
			type : "restapi",
			collection_name : "active_rewards"
		},

	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			url : function() {
				return Alloy.CFG.useekWebService + "/users/" + Ti.App.Properties.getString("userId") + "/" + this.config.adapter.collection_name;
			},
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			url : function() {
				
				return Alloy.CFG.useekWebService + "/users/" + Ti.App.Properties.getString("userId") + "/" + this.config.adapter.collection_name;
			},
			/** returns dynamic http request headers for this model */
			headers : function() {
				return {
					"Accept" : "text/html,application/xhtml+xml,application/xml",
					"Authorization" : 'Token token="' + Titanium.App.Properties.getString('sessionToken') + '"',
					"Content-Type" : "application/json"
				};
			},
			fetchActiveRewards : function(opts) {

				var params = opts || {};
				params.headers = (opts && opts.header) || this.config.headers;
				this.fetch(opts);

			}
		});

		return Collection;
	}
};
