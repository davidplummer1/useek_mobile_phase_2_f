// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

var newrelic = require('ti.newrelic'); newrelic.start("AA2c82e06153486c404a9f37bbc99a4990968ef20e");


	Alloy.Globals.activityIndicator = Alloy.Globals.activityIndicator || require('activityIndicator');

if (!OS_IOS) {
	var pH = parseInt(Ti.Platform.displayCaps.platformHeight);
	var pW = parseInt(Ti.Platform.displayCaps.platformWidth);
	var landscapeOrientations = [Ti.UI.LANDSCAPE_RIGHT,Ti.UI.LANDSCAPE_LEFT];
	Alloy.Globals.platformHeight = Alloy.Globals.platformHeight || (landscapeOrientations.indexOf(Ti.Gesture.orientation) > -1 ? pW+"px" : pH+"px");
	Alloy.Globals.platformWidth = Alloy.Globals.platformWidth || (landscapeOrientations.indexOf(Ti.Gesture.orientation) > -1 ? pH+"px" : pW+"px");
	Alloy.Globals.pH = Alloy.Globals.pH || pH;
	Alloy.Globals.pW = Alloy.Globals.pW || pW;
	
} else {
	Alloy.Globals.pH = Alloy.Globals.pH || parseInt(Ti.Platform.displayCaps.platformHeight);
	Alloy.Globals.pW = Alloy.Globals.pW || parseInt(Ti.Platform.displayCaps.platformWidth);
	Alloy.Globals.platformWidth = Alloy.Globals.platformWidth || Ti.Platform.displayCaps.platformWidth;
	Alloy.Globals.platformHeight = Alloy.Globals.platformHeight || Ti.Platform.displayCaps.platformHeight;

}

Alloy.Globals.deviceUnitOfMeasure = Alloy.Globals.deviceUnitOfMeasure || ( OS_IOS ? "dp" : "px");
Alloy.Globals.videoDimension = {
	height : (Alloy.Globals.pH > Alloy.Globals.pW ? (Alloy.Globals.pW / 16 * 9) : (Alloy.Globals.pW / 16 * 9)) + Alloy.Globals.deviceUnitOfMeasure,
	width : (Alloy.Globals.pH > Alloy.Globals.pW ? Alloy.Globals.pW : Alloy.Globals.pH) + Alloy.Globals.deviceUnitOfMeasure
};

Alloy.Globals.quarterPlatformHeight = Alloy.Globals.quarterPlatformHeight || (Math.ceil(Alloy.Globals.pH / 4) + Alloy.Globals.deviceUnitOfMeasure);
Alloy.Globals.halfPlatformHeight = Alloy.Globals.halfPlatformHeight || (Math.ceil(Alloy.Globals.pH / 2) + Alloy.Globals.deviceUnitOfMeasure);

//main scrollable image size
if (OS_IOS && Ti.Platform.osname == "iphone") {
	Alloy.Globals.platformHeight60Percent = Alloy.Globals.platformHeight60Percent || ((Alloy.Globals.pH * .45) + Alloy.Globals.deviceUnitOfMeasure);
	Alloy.Globals.platformHeight50Percent = Alloy.Globals.platformHeight50Percent || ((Alloy.Globals.pH * .53) + Alloy.Globals.deviceUnitOfMeasure);
} else if (OS_IOS && Ti.Platform.osname == "ipad") {
	Alloy.Globals.platformHeight60Percent = Alloy.Globals.platformHeight60Percent || ((Alloy.Globals.pH * .49) + Alloy.Globals.deviceUnitOfMeasure);
} else {
	Alloy.Globals.platformHeight60Percent = Alloy.Globals.platformHeight60Percent || ((Alloy.Globals.pH * .42) + Alloy.Globals.deviceUnitOfMeasure);
}

if (OS_IOS) {
	Alloy.Globals.statusBar = "20dp";
} else {
	Alloy.Globals.statusBar = 0;
}

Alloy.Globals.navigationBarHeight = Alloy.Globals.navigationBarHeight || "45dp";
/** color key for demographic question answers */
Alloy.Globals.colors = Alloy.Globals.colors || {
	themeColor : '#2EB8B8',
	bgPrimary : '#d8d8d8',
	bgSecondary : '#ebeef2',
	titleColor : '#7db7e6',
	leaderboard : '#005C7A',
	bgColor : "#1b2f54", //#323336
	indigo : "#1c263e",
	trueBlue : "#105098",
	decoAqua : "#e4eaea",
	darkGrey : "#444",
	darkGray : "#444",
	gray : "#888",
	grey : "#888",
	lightGray : "#ddd",
	lightGrey : "#ddd",
	white : '#fff',
	black : '#000',
	selectedMenuOptionBG : "#2EB8B8",
	unselectedMenuOptionBG : "#323336",
	selectedMenuOptionText : "#5883b6",
	unselectedMenuOptionText : "white",
	disabledText : "#b3b3b3",
	selectedLeftMenuText : "#6a93ca",
	selectedLeftMenuOption : "#1c3054",
	dollar : '#006600',
	star : 'orange',
	giveback : 'purple',
	limitColor : '#e89c99',
	gameplaySide : '#323336',
	androidHintText : '#787878',
	iosHintText : '#C0C0C0'
};

Alloy.Globals.getCurrentWindow = Alloy.Globals.getCurrentWindow ||
function() {
	return Alloy.Globals.mainWindow.windowManager.getCurrentWindow();
};
Alloy.Globals.events = Alloy.Globals.events || {};

Ti.UI.setBackgroundColor(Alloy.Globals.colors.themeColor);

if (OS_IOS) {
	Alloy.Globals.gameDimensions = Alloy.Globals.gameDimensions || {
		video : {
			height : (parseInt(Ti.Platform.displayCaps.platformWidth) * 9 / 16) + "dp",
			width : Ti.Platform.displayCaps.platformWidth
		},
		panel : {
			height : (parseInt(Ti.Platform.displayCaps.platformHeight) * .7) + "dp",
			width : Ti.UI.FILL
		}
	};
} else {

	if (Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight) {
		Alloy.Globals.gameDimensions = Alloy.Globals.gameDimensions || {
			video : {
				height : (parseInt(Ti.Platform.displayCaps.platformHeight) * 9 / 16) + "px",
				width : Ti.Platform.displayCaps.platformHeight + "px"
			},
			panel : {
				height : (parseInt(Ti.Platform.displayCaps.platformHeight) * .7) + "px",
				width : Ti.UI.FILL
			}
		};
	} else {
		Alloy.Globals.gameDimensions = Alloy.Globals.gameDimensions || {
			video : {
				height : (parseInt(Ti.Platform.displayCaps.platformWidth) * 9 / 16) + "px",
				width : Ti.Platform.displayCaps.platformWidth + "px"
			},
			panel : {
				height : (parseInt(Ti.Platform.displayCaps.platformHeight) * .7) + "px",
				width : Ti.UI.FILL
			}
		};
	}
}

/*
 * handling multi resolution for font size.
 * general device size / mini tablet size / tablet size
 */
if (Alloy.Globals.pW <= 480 && Alloy.Globals.pH <= 800) {
	Alloy.Globals.fontSizes = Alloy.Globals.fontSizes || {
		x_sm : {
			fontSize : '11dp',
			fontFamily : 'Roboto-Regular'
		},
		sm : {
			fontSize : '12dp',
			fontFamily : 'Roboto-Regular'
		},
		med : {
			fontSize : '14dp',
			fontFamily : 'Roboto-Regular'
		},
		med_l : {
			fontSize : '16dp',
			fontFamily : 'Roboto-Regular'
		},
		l : {
			fontSize : '20dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Regular'
		},
		xl : {
			fontSize : '24dp',
			fontFamily : 'Roboto-Regular'
		},
		x_sm_bold : {
			fontSize : '10dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		sm_bold : {
			fontSize : '12dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		med_bold : {
			fontSize : '14dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		med_l_bold : {
			fontSize : '16dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		l_bold : {
			fontSize : '20dp',
			fontFamily : 'Roboto-Bold'
		},
		xl_bold : {
			fontSize : '24dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		}
	};

} else if ((Alloy.Globals.pW > 480 && Alloy.Globals.pW < 768) && (Alloy.Globals.pH > 800 && Alloy.Globals.pH < 1024)) {
	Alloy.Globals.fontSizes = Alloy.Globals.fontSizes || {
		x_sm : {
			fontSize : '14dp',
			fontFamily : 'Roboto-Regular'
		},
		sm : {
			fontSize : '16dp',
			fontFamily : 'Roboto-Regular'
		},
		med : {
			fontSize : '18dp',
			fontFamily : 'Roboto-Regular'
		},
		med_l : {
			fontSize : '22dp',
			fontFamily : 'Roboto-Regular'
		},
		l : {
			fontSize : '26dp',
			fontFamily : 'Roboto-Regular'
		},
		xl : {
			fontSize : '28dp',
			fontFamily : 'Roboto-Regular'
		},
		x_sm_bold : {
			fontSize : '14dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		sm_bold : {
			fontSize : '16dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		med_bold : {
			fontSize : '18dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		med_l_bold : {
			fontSize : '22dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		l_bold : {
			fontSize : '26dp',
			fontFamily : 'Roboto-Bold'
		},
		xl_bold : {
			fontSize : '28dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		}
	};
} else if (Alloy.Globals.pW >= 768 && Alloy.Globals.pH >= 1024) {
	if (OS_IOS) {
		Alloy.Globals.fontSizes = Alloy.Globals.fontSizes || {
			x_sm : {
				fontSize : '17dp',
				fontFamily : 'Roboto-Regular'
			},
			sm : {
				fontSize : '20dp',
				fontFamily : 'Roboto-Regular'
			},
			med : {
				fontSize : '24dp',
				fontFamily : 'Roboto-Regular'
			},
			med_l : {
				fontSize : '28dp',
				fontFamily : 'Roboto-Regular'
			},
			l : {
				fontSize : '32dp',
				fontFamily : 'Roboto-Regular'
			},
			xl : {
				fontSize : '36dp',
				fontFamily : 'Roboto-Regular'
			},
			x_sm_bold : {
				fontSize : '16dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			},
			sm_bold : {
				fontSize : '18dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			},
			med_bold : {
				fontSize : '22dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			},
			med_l_bold : {
				fontSize : '28dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			},
			l_bold : {
				fontSize : '32dp',
				fontFamily : 'Roboto-Bold'
			},
			xl_bold : {
				fontSize : '36dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			}
		};
	} else {
		Alloy.Globals.fontSizes = Alloy.Globals.fontSizes || {
			x_sm : {
				fontSize : '12dp',
				fontFamily : 'Roboto-Regular'
			},
			sm : {
				fontSize : '14dp',
				fontFamily : 'Roboto-Regular'
			},
			med : {
				fontSize : '16dp',
				fontFamily : 'Roboto-Regular'
			},
			med_l : {
				fontSize : '18dp',
				fontFamily : 'Roboto-Regular'
			},
			l : {
				fontSize : '20dp',
				fontFamily : 'Roboto-Regular'
			},
			xl : {
				fontSize : '22dp',
				fontFamily : 'Roboto-Regular'
			},
			x_sm_bold : {
				fontSize : '12dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			},
			sm_bold : {
				fontSize : '14dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			},
			med_bold : {
				fontSize : '16dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			},
			med_l_bold : {
				fontSize : '18dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			},
			l_bold : {
				fontSize : '20dp',
				fontFamily : 'Roboto-Bold'
			},
			xl_bold : {
				fontSize : '22dp',
				fontWeight : 'bold',
				fontFamily : 'Roboto-Bold'
			}
		};
	}
} else {
	Alloy.Globals.fontSizes = Alloy.Globals.fontSizes || {
		x_sm : {
			fontSize : '11dp',
			fontFamily : 'Roboto-Regular'
		},
		sm : {
			fontSize : '12dp',
			fontFamily : 'Roboto-Regular'
		},
		med : {
			fontSize : '14dp',
			fontFamily : 'Roboto-Regular'
		},
		med_l : {
			fontSize : '16dp',
			fontFamily : 'Roboto-Regular'
		},
		l : {
			fontSize : '20dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Regular'
		},
		xl : {
			fontSize : '24dp',
			fontFamily : 'Roboto-Regular'
		},
		x_sm_bold : {
			fontSize : '10dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		sm_bold : {
			fontSize : '12dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		med_bold : {
			fontSize : '14dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		med_l_bold : {
			fontSize : '16dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		},
		l_bold : {
			fontSize : '20dp',
			fontFamily : 'Roboto-Bold'
		},
		xl_bold : {
			fontSize : '24dp',
			fontWeight : 'bold',
			fontFamily : 'Roboto-Bold'
		}
	};
}
//alert(Alloy.Globals.pW +", "+Alloy.Globals.pH )

